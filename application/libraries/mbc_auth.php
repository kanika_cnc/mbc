<?php if (!defined("BASEPATH")) exit("No direct script access allowed");

/**
 * 
 * Class to check if the loggin user is accessing the privilaged pages only
 * @author fintotal
 * @todo : to move the url to some config file
 *
 */
class Mbc_Auth
{
	public function process_redirection($logged_in_user)
	{
		$logged_in =(object) $logged_in_user;
		switch($logged_in->user_type_id)
		{
			case UserType::MBC_USER :
			case UserType::MBC_ADMIN:
				redirect('/mbc/dashboard/', 'refresh');
				break;
			case UserType::CANDIDATE:
				redirect('/candidates/home/', 'refresh');
				break;
			case UserType::BANK:
				redirect('/banks/home/', 'refresh');
				break;
			default:
				redirect('home', 'refresh');
		}
	}
}