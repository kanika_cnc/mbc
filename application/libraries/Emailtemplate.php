<?php if (!defined("BASEPATH")) exit("No direct script access allowed");

/**
 * 
 * Class to send the Email for this website
 * @todo: This class would call the templates created for each email and then create the messae to send.
 * The templates are placed in view/email
 * @author sunil
 *
 */
class Emailtemplate
{
	const FORGOTPASSWORD = 'forgot_password';
	const CREATEPASSWORD = 'create_password';
	const CHANGEPASSWORD = 'change_password';
	/**
	 * 
	 * Enter description here ...
	 * @param $email_type : const list of the class
	 * @param $from_user : from name. If nothings is set then it would be default setting for each email
	 * @param $to_user: List of user to which email should be sent
	 * @param $subject_params : Any user data to be sent in subject
	 * @param $message_params : Any user data to be sent in mail body
	 */
	public function sendEmail($email_type,$from_user="",$to_user=array(),$subject_params = array(),$message_params = array())
	{
		switch ($email_type)
		{
			case Emailtemplate::FORGOTPASSWORD :
				$subject = "MBC Account Password";
				$message = "Password : 0";
				break;
			case Emailtemplate::CREATEPASSWORD :
				$subject = "MBC Account Password";
				$message = "Password : 0";
				break;	
			case Emailtemplate::CHANGEPASSWORD :
				$subject = "MBC Account Password Changed";
				$message = "New Password : 0";
				break;	
		}
		if(!empty($message_params))
		{
			foreach($message_params as $key =>$value)
			$message = str_replace("$key", $value, $message);
		}
		if(!empty($to_user))
		{
			foreach($to_user as $email)
			{
				mail($email,$subject,$message);
			}
		}		
	}
}