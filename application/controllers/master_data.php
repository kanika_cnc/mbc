<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Master_Data extends MBC_Controller {

	public function banks($format,$keyword){
		$this->load->model('bl/Master_Data_Bl','master');
		$data = $this->master->get_banks("banks",$keyword,false);
		
		if(strtolower($format) =='json')
		{
			$format_data =array();
			foreach ($data as $key=>$value)
			{
				$format_data["options"] = array(array("id"=>$key,"name" =>$value));
			} 
			
			echo json_encode($format_data);
		}
		else if($format =='html')
		{
			echo form_dropdown('hddl_auto_banks', $data, 'id="hddl_auto_banks"  class=""') ;
			/*foreach ($data as $key=>$value)
			{
				echo "<li id='".$key."'>". $value ."</li>";
			} */
		}
		else
		{
			
		}
	}
	public function branches($bank_id,$format){
		$this->load->model('bl/Master_Data_Bl','master');
		$data = $this->master->get_branches($bank_id,false);
		if(strtolower($format) =='json')
		{
			$format_data =array();
			foreach ($data as $key=>$value)
			{
				$format_data["options"] = array(array("id"=>$key,"name" =>$value));
			} 
			
			echo json_encode($format_data);
		}
		else if($format =='html')
		{
			echo form_dropdown('hddl_auto_banks_branches', $data, 'id="hddl_auto_banks_branches"  class=""') ;
			/*foreach ($data as $key=>$value)
			{
				echo "<li id='".$key."'>". $value ."</li>";
			} */
		}
		else
		{
			
		}
	}
		public function locations($format,$keyword){
		$this->load->model('bl/locations_bl','master');
		$data = $this->master->get_locations($keyword,false);
		if(strtolower($format) =='json')
		{
			$format_data =array();
			foreach ($data as $key=>$value)
			{
				$format_data["options"] = array(array("id"=>$value->lm_id,"name" =>$value->city . " , ".$value->states ));
			} 
			echo json_encode($format_data);
		}
	}
 	public function get_qualifications()
 	{
 		$data = array();
 		$data = $this->get_items('mbc_qualification_master', 'qm_id');
 		print_r($data);
 	}
}