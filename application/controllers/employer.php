<?php
if(empty($_POST['isAjax']))
{
$page = "Update Your Details";
include "header.php";


 ?>
        
        <div class="right-box">
        
        	<div class="box" id="experience">
              <img src="images/icon-3.jpg" width="40" alt="" align="left" />
           	  <h1 class="title">Experience</h1>
              <div style="clear:both;"></div>
              <br />
                
 <table width="610" border="0" cellspacing="0" cellpadding="5">
  <tr>
    <td width="120">
    <label>Current Bank</label>
    </td>
    <td width="490">
    <select name="select" id="bank" class="large">
      <option value="0">--Select--</option>
	  <?php 
	  $sql = "SELECT * FROM `organisations` WHERE 1";
	  $result = mysql_query($sql);
	  while($row = mysql_fetch_array($result))
	  {
	   ?>
	   <option value="<?php echo $row['org_id'] ?>"><?php echo $row['org_name'] ?></option>
	   <?php } ?>
    </select>
    </td>
  </tr>
  <tr>
    <td>
    <label>Branch</label>
    </td>
    <td>
    <select name="select" id="branches" class="large">
      <option value="0">--Select Bank First--</option>
    </select>
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>
      <input type="checkbox" name="isworking" value="1" id="isworking" /> <label style="margin:0px; float:none" for="isworking">I currently work here</label>
    </td>
  </tr>
  <tr>
    <td><label>Since</label></td>
    <td>
    <select name="since_month" id="since_month" class="small">
      <option value="00">MM</option>
	  <option value="01">Jan</option>
	  <option value="02">Feb</option>
	  <option value="03">Mar</option>
	  <option value="04">Apr</option>
	  <option value="05">May</option>
	  <option value="06">June</option>
	  <option value="07">July</option>
	  <option value="08">Aug</option>
	  <option value="09">Sept</option>
	  <option value="10">Oct</option>
	  <option value="11">Nov</option>
	  <option value="12">Dec</option>
    </select>
    <input name="since_year" id="since_year" type="text" value="" class="input small" placeholder="YYYY" />
    <span id="till">Till</span>
    <select name="till_month" id="till_month" class="small">
      <option value="00">MM</option>
	  <option value="01">Jan</option>
	  <option value="02">Feb</option>
	  <option value="03">Mar</option>
	  <option value="04">Apr</option>
	  <option value="05">May</option>
	  <option value="06">June</option>
	  <option value="07">July</option>
	  <option value="08">Aug</option>
	  <option value="09">Sept</option>
	  <option value="10">Oct</option>
	  <option value="11">Nov</option>
	  <option value="12">Dec</option>
    </select>
    <input name="till_year" id="till_year" type="text" value="" class="input small" placeholder="YYYY" />
    </td>
  </tr>
  <tr>
    <td><label>Designation</label></td>
    <td>
    <input name="role" id="role" type="text" value="" placeholder="e.g. Manager, officer etc." class="input large" />
    </td>
  </tr>
  <tr>
    <td><label>Internal Band</label></td>
    <td>
    <input name="band" id="band" type="text" value="" placeholder="e.g. Scale 4, Grade 3 etc." class="input large" />
    </td>
  </tr>
  <tr>
    <td><label>Current CTC</label></td>
    <td>
    <select id="ctc_lacs" class="small-select">
        <?php for ($i=1; $i<=99;$i++) { ?>
				<option value="<?php echo $i ?>"> <?php echo $i ?> Lacs </option> 
		<?php } ?>
    </select> 
                    
    <select id="ctc_thousand" class="small-select">
        <?php for ($i=1; $i<=99;$i++) { ?>
				<option value="<?php echo $i ?>"> <?php echo $i ?> Thousands </option> 
		<?php } ?>
    </select>    
	<div style="clear:both;"></div>
    </td>
  </tr>
  <tr>
    <td><label>Role</label></td>
    <td>
	<div>
    <input type="text" value="" id="departments" class="large input selectinput">
	<input type="hidden" value="" id="departments_ids">
	<div id="list_depts">
	
	<?php $sql = "SELECT * FROM `departments` WHERE `parent_id` = 0 LIMIT 0,13";
	$result = mysql_query($sql);
	$row = mysql_fetch_array($result);
	while($row = mysql_fetch_array($result)) { ?> 
	<div class="parent"><input type="checkbox" name="<?php echo $row['dept_name'] ?>" value="<?php echo $row['dept_id'] ?>" level='1' class="depts"><?php echo $row['dept_name'] ?></div>
	<?php } ?>
	
	
	<?php $sql = "SELECT * FROM `departments` WHERE `parent_id` = 0 LIMIT 13,50000";
	$result = mysql_query($sql);
	while($row = mysql_fetch_array($result)) { ?> 
	<div class="parent"><input type="checkbox" name="<?php echo $row['dept_name'] ?>" value="<?php echo $row['dept_id'] ?>" level='1' class="depts"><?php echo $row['dept_name'] ?>
	<?php } ?>
	</div>
	</div>
	</div>
    </td>
  </tr>
  <tr>
    <td colspan="2" align="center">
       <input type="submit" value="Next" id="current_submit" class="submit">
    </td>
    </tr>
</table>
  
                   <?php /*?> <div class="addmore">
           	        <a href="#"><img src="images/Add-icon.png" align="left" alt=" " /> Add Previous Experience</a>
                    </div><!--end addmore-->
                    <div style="clear:both;"></div><?php */?>
                
                    
                    
                
                
          </div><!--end box-->
		  
<!---------------------------------------------------------------Box Qualification------------------------------------------------------------------------------------------------>
            <div class="box" id="qualification">
            	<img src="images/icon-2.jpg" width="40" alt="" align="left" />
           	  <h1 class="title">Qualification</h1>
              <div style="clear:both;"></div>
                <br />
                
 <table width="480" border="0" cellspacing="0" cellpadding="5">
  <tr>
    <td width="120">
    <label>
    Post Graduation
    <p class="note">(you can select multiple)</p>
    </label>
    </td>
    <td width="290">
    <select name="select" class="large" id="pg">
      <?php $sql = "SELECT * FROM `education` WHERE `edu_type` = 'PG' ";
	  	$result = mysql_query($sql);
		while($row = mysql_fetch_array($result))
		{
	   ?>
	   	<option value="<?php echo $row['edu_id'] ?>"><?php echo $row['edu_name'] ?></option>
	   <?php } ?>
    </select>
    </td>
  </tr>
  <tr>
    <td>
    <label>
     Graduation
    <p class="note">(you can select multiple)</p>
    </label>
    </td>
    <td>
    <select name="select" class="large" id="grad">
      <?php $sql = "SELECT * FROM `education` WHERE `edu_type` = 'GR' ";
	  	$result = mysql_query($sql);
		while($row = mysql_fetch_array($result))
		{
	   ?>
	   	<option value="<?php echo $row['edu_id'] ?>"><?php echo $row['edu_name'] ?></option>
	   <?php } ?>
    </select>
    </td>
  </tr>
  <tr>
    <td><label>Certification</label></td>
    <td>
    <input type="text" name="cerifications" id="cerifications" placeholder = "CCNA, NPA etc." class="input large" />
 
    </td>
  </tr>

  <tr>
    <td colspan="2" align="center">
       <input type="button" id="submit_ql" value="Next" class="submit">
    </td>
    </tr>
</table>
                
          </div><!--end box-->  
<!---------------------------------------------------------------Box Personal------------------------------------------------------------------------------------------------>
		  
		  
     <div class="box" id="personal">
            	<img src="images/icon-1.jpg" width="40" alt="" align="left" />
           	  <h1 class="title">Personal</h1>
              <div style="clear:both;"></div>
                <br />
                
 <table width="480" border="0" cellspacing="0" cellpadding="5">
  <tr>
    <td width="120">
    <label>
    Date of Birth
    </label>
    </td>
    <td width="290">
    <select class="small-select date" id="dob-date">
                    	<option value="00">00</option>
						<option value="01">01</option>
						<option value="02">02</option>
						<option value="03">03</option>
						<option value="04">04</option>
						<option value="05">05</option>
						<option value="06">06</option>
						<option value="07">07</option>
						<option value="08">08</option>
						<option value="09">09</option>
						<?php for ($i=10;$i<32;$i++) { ?>
							<option value="<?php echo $i ?>"><?php echo $i ?></option>
						<?php } ?>
              </select> 
                    <select class="small-select month" id="dob-month">
                    	  <option value="00">MM</option>
						  <option value="01">Jan</option>
						  <option value="02">Feb</option>
						  <option value="03">Mar</option>
						  <option value="04">Apr</option>
						  <option value="05">May</option>
						  <option value="06">June</option>
						  <option value="07">July</option>
						  <option value="08">Aug</option>
						  <option value="09">Sept</option>
						  <option value="10">Oct</option>
						  <option value="11">Nov</option>
						  <option value="12">Dec</option>
                    </select>   
                    <input type="text" class="input small" id="dob-year" placeholder="YYYY" />
				  <div style="clear:both;"></div> 
    </td>
  </tr>
  <tr>
    <td>
    <label>
    Gender
    </label>
    </td>
    <td>
	<select class="small-select" id="gender">
	<option value="M">Male</option>
       <option value="F">Female</option>
    </select>  
    </td>
  </tr>
  <tr>
    <td><label>Marital Status</label></td>
    <td>
    <select class="small-select" id="marital-status">
	<option value="S">Single</option>
       <option value="M">Married</option>
    </select>  
    </td>
  </tr>
  <tr>
  	<td>
    <label>Attach Resume</label>
    </td>
  	<td>
    <input type="file" name="resume" id="resume" />
    </td>
  </tr>
  <tr>
  	<td>
    <label>Upload Photo</label>
    </td>
  	<td><input type="file" name="photo" id="photo" /></td>
  </tr>

  <tr>
    <td colspan="2" align="center">
       <input type="button" id="submit_pr" value="Next" class="submit">
    </td>
    </tr>
</table>     
          </div><!--end box-->

<!---------------------------------------------------------------Box Job Preference----------------------------------------------------------------------------------------------->

<div class="box" id="job-preference">
            	<img src="images/icon-4.jpg" width="40" alt="" align="left" />
           	  <h1 class="title">Job Preference</h1>
              <div style="clear:both;"></div>
                <br />
                
 <table width="480" border="0" cellspacing="0" cellpadding="5">
  <tr>
    <td width="120">
    <label>
    Location
    </label>
    </td>
    <td width="290">
    <input type="text" id="pref_loc" class="large input selectinput">
	<div id="loc_list">
      <?php 
	  $sql = "SELECT DISTINCT `branch` FROM `org_branches` WHERE 1"; 
	  $result = mysql_query($sql);
	  while($row = mysql_fetch_array($result))
	  {
	  ?>
	  	<input type="checkbox" class="loc_cb" value="<?php echo $row['branch'] ?>" ><?php echo $row['branch'] ?><br />
	  <?php } ?>
    </div>
      <div style="clear:both;"></div> 
    </td>
  </tr>
  <tr>
    <td>
    <label>
    Banks
    </label>
    </td>
    <td><input type="text" name="banks" id="banks" value="" class="large input selectinput" />
	<input type="hidden" name="bank_ids" id="banks_ids" value="" />
	<div id="banks_list">
      <?php 
	  $sql = "SELECT * FROM `banks` WHERE 1"; 
	  $result = mysql_query($sql);
	  while($row = mysql_fetch_array($result))
	  {
	  ?>
	  	<input type="checkbox" class="banks_cb" value="<?php echo $row['bank_id'] ?>" name="<?php echo $row['bank_name'] ?>"><?php echo $row['bank_name'] ?><br />
	  <?php } ?>
    </div></td>
  </tr>
  <tr>
    <td colspan="2" align="center">
       <input type="button" id="submit_jp" value="Next" class="submit">
    </td>
    </tr>
</table>
                
          </div><!--end box-->    

          </div><!--end right-box-->
        
        <div style="clear:both;"></div>
    </div><!--end main-->
   
            
            

<?php include "footer.php"; ?>   
<script type="text/javascript">
	$(document).ready(function(){
		$("#experience").show();
		$("#submit_jp").click(function(){
			var banks_ids = $("#banks_ids").val();
			var pref_loc = $("#pref_loc").val();
			$.ajax({
				url:'employer.php',
				type:'post',
				data:{isAjax:1,action:'submit_jp',banks_ids:banks_ids,pref_loc:pref_loc}			
			}).done(function(html){
				$("#job-preference").html(html);
			});
		});
		$("#banks").focus(function(){
			$("#banks_list").show();
		});
		$("#pref_loc").focus(function(){
			$("#loc_list").show();
		});
		$(document).on('click','.banks_cb',function(){
			
		 if($(this).prop('checked') == true)
		{
			var curr = $("#banks").val();
			if(curr!="")
			var newval = curr+", "+$(this).attr('name');
			else
			var newval = $(this).attr('name');
			$("#banks").val(newval);
			var curr_ids = $("#banks_ids").val();
			if(curr_ids!="")
			var newval_ids = curr_ids+", "+$(this).val();
			else
			var newval_ids = $(this).val();
			$("#banks_ids").val(newval_ids);
		}
		else
		{
			var val = $("#banks").val();
			var name = $(this).attr('name');
			val = val.replace(name+", ", "");
			val = val.replace(name, "");
			$("#banks").val(val);
			//////////////////////////////
			val = $("#banks_ids").val();
			name = $(this).val();
			val = val.replace(name+", ", "");
			val = val.replace(name, "");
			$("#banks_ids").val(val);
			
		}
			
		});
		$(document).on('click','.loc_cb',function(){
			
		 if($(this).prop('checked') == true)
		{
			var curr = $("#pref_loc").val();
			if(curr!="")
			var newval = curr+", "+$(this).val();
			else
			var newval = $(this).val();
			$("#pref_loc").val(newval);
		}
		else
		{
			var val = $("#pref_loc").val();
			var name = $(this).val();
			val = val.replace(name+", ", "");
			val = val.replace(name, "");
			$("#pref_loc").val(val);
			
		}
			
		});
		$("#submit_pr").click(function(){
			var dob_date = $("#dob-date").val();
			var dob_month = $("#dob-month").val();
			var dob_year = $("#dob-year").val();
			var gender = $("#gender").val();
			var marital = $("#marital-status").val();
			var resume = $("#resume").val();
			var photo = $("#photo").val();
			if(resume != "")
			{
				$.ajaxFileUpload({
					url:'doajaxfileupload.php',
					secureuri:false,
					fileElementId:'resume',
					dataType: 'json',
					success: function(data){
						if(typeof(data.error) != 'undefined')
						{
							if(data.error != '')
							{
								alert(data.error);
								return false;
							}
							else
							{
							 var resume1=  data.msg;
							}
						}
						return false;
					},
					error: function (data, status, e){
						alert(e);
						return false;
						}
					});
			  }
			  if(photo != "")
			{
				$.ajaxFileUpload({
					url:'doajaxfileupload1.php',
					secureuri:false,
					fileElementId:'photo',
					dataType: 'json',
					success: function(data){
						if(typeof(data.error) != 'undefined')
						{
							if(data.error != '')
							{
								alert(data.error);
								return false;
							}
							else
							{
							 var photo1=  data.msg;
							}
						}
						return false;
					},
					error: function (data, status, e){
						alert(e);
						return false;
						}
					});
			  }
			  $.ajax({
				url:'employer.php',
				type:'post',
				data:{isAjax:1,action:'submit_pr',dob_date:dob_date,dob_month:dob_month,dob_year:dob_year,gender:gender,marital:marital}			
			}).done(function(html){
			
				$("#personal").html(html);
				$("#job-preference").show();
				var content = $("body");
    			content.delay(5000).scrollTop(content[0].scrollHeight);
				
			});
			 
		});
		$("#submit_ql").click(function(){
			var pg = $("#pg").val();
			var gr = $("#grad").val();
			var certi = $("#cerifications").val();
			$.ajax({
				url:'employer.php',
				type:'post',
				data:{isAjax:1,action:'submit_ql',pg:pg,gr:gr,certi:certi}			
			}).done(function(html){
				$("#qualification").html(html);
				$("#personal").show();
				var content = $("body");
    			content.delay(5000).scrollTop(content[0].scrollHeight);	
			});
			
		});
		$("#current_submit").click(function(){
			var branch = $("#branches").val();
			var since_month = $("#since_month").val();
			var since_year = $("#since_year").val();
			var isworking = 0;
			var till_month = $("#till_month").val();
			var till_year = $("#till_year").val();
			if($("#isworking").is(":checked"))
			{
				till_month = 0;
				till_year = 0
				isworking = 1;
			}
			var role = $("#role").val();
			var band = $("#band").val();
			var ctc_lacs = $("#ctc_lacs").val();
			var ctc_thousand = $("#ctc_thousand").val();
			var departments_ids = $("#departments_ids").val();
				$.ajax({
				url:'employer.php',
				type:'post',
				data:{isAjax:1,action:'submit_ce',branch:branch,since_month:since_month,since_year:since_year,isworking:isworking,till_month:till_month,till_year:till_year,role:role,band:band,ctc_lacs:ctc_lacs,ctc_thousand:ctc_thousand,departments_ids:departments_ids}			
			}).done(function(html){
			
				$("#experience").html(html);
				$("#qualification").show();
				var content = $("body");
    			content.delay(5000).scrollTop(content[0].scrollHeight);
				
			});
		
		});
		$('#list_depts, #departments, #banks_list, #loc_list, #banks, #pref_loc').hover(function(){ 
				mouse_is_inside=true; 
			}, function(){ 
				mouse_is_inside=false; 
		});
		
		$("body").mouseup(function(){ 
			if(! mouse_is_inside) 
			{
				$('#list_depts').hide();
				$('#banks_list').hide();
				$('#loc_list').hide();
			}
		});
		$(document).on('click','.depts',function(){
			
		 if($(this).prop('checked') == true)
		{
			var parent_id = $(this).val();
			var parent = $(this).parent();
			var level = $(this).attr('level');
			var curr = $("#departments").val();
			if(curr!="")
			var newval = curr+", "+$(this).attr('name');
			else
			var newval = $(this).attr('name');
			$("#departments").val(newval);
			var curr_ids = $("#departments_ids").val();
			if(curr_ids!="")
			var newval_ids = curr_ids+", "+$(this).val();
			else
			var newval_ids = $(this).val();
			$("#departments_ids").val(newval_ids);
			$.ajax({
				url:'employer.php',
				type:'post',
				data:{isAjax:1,action:'get_depts',parent_id:parent_id,level:level}			
			}).done(function(html){
				parent.append(html);
			});
		}
		else
		{
			var val = $("#departments").val();
			var name = $(this).attr('name');
			val = val.replace(name+", ", "");
			val = val.replace(name, "");
			$("#departments").val(val);
			//////////////////////////////
			val = $("#departments_ids").val();
			name = $(this).val();
			val = val.replace(name+", ", "");
			val = val.replace(name, "");
			$("#departments_ids").val(val);
			//$(this).parent().children('div').remove();
		}
			
		});
		$("#departments").focus(function(){
			$("#list_depts").show();
		});
		$("#isworking").click(function(){
			if($(this).prop('checked') == true)
			{
				$("#till").hide();
				$("#till_month").hide();
				$("#till_year").hide();
			}
			else
			{
				$("#till").show();
				$("#till_month").show();
				$("#till_year").show();
			}
		});
		$("#bank").change(function(){
			$("#branches").prop('disabled', true);
			var bank = $(this).val();
			$.ajax({
				url:'employer.php',
				type:'post',
				data:{isAjax:1,action:'get_branch',bank_id:bank}			
			}).done(function(html){
				$("#branches").html(html);
				$("#branches").prop('disabled', false);
			});
		});
		$(document).on("click",'.addexp',function(){
			$(this).html('<input name="" type="text" class="exptxt input vsmall" /> <input name="save" type="button" value="Save" class="expsubmit"/>');
			$(this).removeClass('addexp');
		});
		$(document).on("click",'.expsubmit',function(){
		var t = $(this);
			var expe = t.prev(".exptxt").val();
			var map_id = t.parent().attr('id');
			$.ajax({ 
				url:'employer.php',
				type:'post',
				data:{isAjax:1,action:'add_dept_exp',expe:expe,map_id:map_id}			
			}).done(function(html){
				t.parent().addClass('addexp').html(expe+" years");
			});
			
		});
	});
</script>

<?php } // End of if!Ajajx 
elseif($_POST['isAjax']==1)
{
session_start();
include "../config.php";
	if($_POST['action']=='add_dept_exp')
	{
		$expe = mysql_real_escape_string($_POST['expe']);
		$map_id = mysql_real_escape_string($_POST['map_id']);
		$sql = "UPDATE `emp_curr_depts` SET `experience` = '$expe' WHERE `map_id` = '$map_id'";
		mysql_query($sql) or die($sql.mysql_error());
	}
	elseif($_POST['action']=='get_branch')
	{
		$org_id = mysql_real_escape_string(stripslashes($_POST['bank_id']));
		echo $sql = "SELECT * FROM `org_branches` WHERE `org_id` = '$org_id'";
		$result = mysql_query($sql);
		while($row = mysql_fetch_array($result))
		{
?>
			<option value="<?php echo $row['branch_id'] ?>"><?php echo $row['branch'] ?></option>
<?php
		}
	}// End Get Branches
	elseif($_POST['action'] == 'get_depts')
	{
		
		$parent_id = mysql_real_escape_string(stripslashes($_POST['parent_id']));
		$level = mysql_real_escape_string(stripslashes($_POST['level']));
		$level ++;
		$l = $level;
		if($l>2)
		$dashes = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
		if($l>4)
		$dashes = "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
		while($l>=0)
		{
			$dashes .= "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;";
			$l--;
		}
		$sql = "SELECT * FROM `departments` WHERE `parent_id` = '$parent_id'";
		$result = mysql_query($sql);
		while($row = mysql_fetch_array($result)) 
		{ 
?> 
			<div><?php echo $dashes ?><input type="checkbox" name="<?php echo $row['dept_name'] ?>" value="<?php echo $row['dept_id'] ?>" level='<?php echo $level ?>' class="depts"><?php echo $row['dept_name'] ?></div>
<?php 	
		} 
	}///End Get Departments
	elseif($_POST['action'] == 'submit_ce')
	{
		if($_REQUEST['a']!='edit')
		{
		$branch = mysql_real_escape_string(stripslashes($_POST['branch']));
		$since_month = mysql_real_escape_string(stripslashes($_POST['since_month']));
		$since_year = mysql_real_escape_string(stripslashes($_POST['since_year']));
		$joining_dt = $since_year."-".$since_month.-"01";
		$isworking = mysql_real_escape_string(stripslashes($_POST['isworking']));
		$till_month = mysql_real_escape_string(stripslashes($_POST['till_month']));
		$till_year = mysql_real_escape_string(stripslashes($_POST['till_year']));
		if($isworking == 0)
		$last_dt = $till_year."-".$till_month.-"01";
		else $last_dt = "";
		$role = mysql_real_escape_string(stripslashes($_POST['role']));
		$band = mysql_real_escape_string(stripslashes($_POST['band']));
		$ctc_lacs = mysql_real_escape_string(stripslashes($_POST['ctc_lacs']));
		$ctc_thousand = mysql_real_escape_string(stripslashes($_POST['ctc_thousand']));
		$ctc = $ctc_lacs * 100000 + $ctc_thousand * 1000;
		$emp_id = $_SESSION['mbc_user_id'];
		$departments_ids = mysql_real_escape_string(stripslashes($_POST['departments_ids']));
		$sql = "UPDATE `employees` SET `emp_curr_band`= '$band' ,`emp_curr_branch_id`='$branch',`emp_curr_org_joining_date`='$joining_dt',`emp_curr_org_last_date`='$last_dt',`emp_curr_deignation`='$role',`emp_curr_ctc`='$ctc' WHERE `emp_id` = '$emp_id'";
		mysql_query($sql) or die(mysql_error().$sql);
		$sql = "DELETE FROM `emp_curr_depts` WHERE `emp_id`='$emp_id'";
		mysql_query($sql) or die(mysql_error().$sql);
		$depts = explode(",",$departments_ids);
		foreach($depts as $d)
		{
			if(!empty($d))
			{
				$sql = "INSERT INTO `emp_curr_depts` (`emp_id`,`dept_id`) VALUES ('$emp_id', '$d')";
				mysql_query($sql) or die(mysql_error().$sql);
			}
		}
		}
		$sql = "SELECT * FROM `employees` WHERE `emp_id`='$emp_id'";
		$result = mysql_query($sql);
		$row = mysql_fetch_array($result);
		?>
		
              <img src="images/icon-3.jpg" width="40" alt="" align="left" />
           	  <h1 class="title">Experience </h1>
              <div style="clear:both;"></div>
              <br />
                
 <table width="440" border="0" cellspacing="0" cellpadding="5" class="table">
  <tr>
    <td width="120">
      <label class="headingline">Current Bank:</label>
    </td>
    <td width="283"><span class="profile-data"><?php $sql = "SELECT b.*, br.* FROM `organisations` AS b, `org_branches` AS br WHERE br.`branch_id`='$row[emp_curr_branch_id]' AND br.`org_id` = b.`org_id`";
	$result = mysql_query($sql); $bank_det = mysql_fetch_array($result); echo $bank_det['org_name'] ?></span></td>
    </tr>
  <tr>
    <td>
      <label class="headingline">Location of Bank:</label>
    </td>
    <td><span class="profile-data"><?php echo $bank_det['branch']; ?></span></td>
    </tr>
  <tr>
    <td><label class="headingline">Since:</label></td>
    <td><span class="profile-data"><?php echo date("d M, Y",strtotime($row['emp_curr_org_joining_date'])); if($row['emp_curr_org_last_date'] != "0000-00-00") echo " &nbsp;&nbsp;&nbsp;&nbsp; Till: ".date("d M, Y",strtotime($row['emp_curr_org_last_date'])); else echo "Till now"; ?></span></td>
    </tr>
  <tr>
    <td valign="top"><label class="headingline">Role:</label></td>
    <td>
    <table width="100%" border="0" cellspacing="0" cellpadding="5" class="minitable">
	<?php $sql = "SELECT ecd.*,d.* FROM `emp_curr_depts` AS ecd, `departments` AS d WHERE ecd.`emp_id` = '$emp_id' AND ecd.`dept_id` = d.`dept_id`";
	$result = mysql_query($sql);
	while($depts = mysql_fetch_array($result)) { ?>
          <tr>
            <td width="56%"><?php echo $depts['dept_name']; ?></td>
            <td width="44%" class="addexp" id="<?php echo $depts['map_id'] ?>"><?php if($depts['experience'] > 0) echo $depts['experience']." years"; else { ?> <a href="javascript:;" >Add Exp</a> <?php } ?></td>
          </tr>
	<?php } ?>
          
        </table>
    </td>
    </tr>
  <tr>
    <td><label class="headingline">Internal Band:</label></td>
    <td><span class="profile-data"><?php echo $row['emp_curr_band']; ?> </span></td>
    </tr>
  <tr>
    <td><label class="headingline">Current CTC:</label></td>
    <td><span class="profile-data"><?php $ctc =  $row['emp_curr_ctc'];
		if(strlen($ctc) == 7)
		{
			echo substr($ctc,0,2)." Lacs ";
			echo substr($ctc,2,2)." Thousands";
			
		}
		else
		{
			echo substr($ctc,0,1)." Lacs ";
			echo substr($ctc,1,2)." Thousands";
		}
	 ?> </span></td>
    </tr>
  <tr>
    <td><label class="headingline">Designation:</label></td>
    <td><span class="profile-data"><?php echo $row['emp_curr_deignation']; ?> </span></td>
    </tr>
</table>
  
                   
                    <div style="clear:both;"></div>
                
                    
                    
                
                
<?php		
	}// End submit_ce
	elseif($_POST['action'] == 'submit_ql')
	{
		$pg = mysql_real_escape_string(stripslashes($_POST['pg']));
		$gr = mysql_real_escape_string(stripslashes($_POST['gr']));
		$certi = mysql_real_escape_string(stripslashes($_POST['certi']));
		$emp_id = $_SESSION['mbc_user_id'];
		$sql = "UPDATE `employees` SET `emp_cirtification`='$certi' WHERE `emp_id` = '$emp_id'";
		mysql_query($sql) or die(mysql_error().$sql);
		$sql = "DELETE FROM `emp_edu` WHERE `emp_id`='$emp_id'";
		mysql_query($sql) or die(mysql_error().$sql);
		$sql = "INSERT INTO `emp_edu`(`emp_id`, `edu_id`) VALUES ('$emp_id','$pg')";
		mysql_query($sql) or die(mysql_error().$sql);
		$sql = "INSERT INTO `emp_edu`(`emp_id`, `edu_id`) VALUES ('$emp_id','$gr')";
		mysql_query($sql) or die(mysql_error().$sql);
		$sql = "SELECT ee.*,e.* FROM `emp_edu` AS ee, `education` AS e WHERE ee.`emp_id` = '$emp_id' AND e.`edu_id` = ee.`edu_id` AND e.`edu_type` = 'GR'";
		$result = mysql_query($sql);
		$gr = mysql_fetch_array($result);
		$sql = "SELECT ee.*,e.* FROM `emp_edu` AS ee, `education` AS e WHERE ee.`emp_id` = '$emp_id' AND e.`edu_id` = ee.`edu_id` AND e.`edu_type` = 'PG'";
		$result = mysql_query($sql);
		$pg = mysql_fetch_array($result);
		$sql = "SELECT * FROM `employees` WHERE `emp_id`='$emp_id'";
		$result = mysql_query($sql);
		$row = mysql_fetch_array($result);
		?>
		
            	<img src="images/icon-2.jpg" width="40" alt="" align="left" />
           	  <h1 class="title">Qualification </h1>
              <div style="clear:both;"></div>
                <br />
                
 <table width="480" border="0" cellspacing="0" cellpadding="5" class="table">
  <tr>
    <td width="120">
    <label class="headingline">
    Post Graduation:
    </label>
    </td>
    <td width="290"><span class="profile-data"><?php echo $pg['edu_name'] ?> </span></td>
  </tr>
  <tr>
    <td>
    <label class="headingline">
    Graduation;
    </label>
    </td>
    <td><span class="profile-data"><?php echo $gr['edu_name'] ?>  </span></td>
  </tr>
  <tr>
    <td><label class="headingline">Certification:</label></td>
    <td><span class="profile-data"><?php echo $row['emp_cirtification'] ?></span></td>
  </tr>
</table>

                    

		<?php
	}//End submit_ql
	elseif($_POST['action'] == 'submit_pr')
	{
		$dob_date = mysql_real_escape_string(stripslashes($_POST['dob_date']));
		$dob_month = mysql_real_escape_string(stripslashes($_POST['dob_month']));
		$dob_year = mysql_real_escape_string(stripslashes($_POST['dob_year']));
		$dob = $dob_year."-".$dob_month."-".$dob_date;
		$gender = mysql_real_escape_string(stripslashes($_POST['gender']));
		$marital = mysql_real_escape_string(stripslashes($_POST['marital']));
		$emp_id = $_SESSION['mbc_user_id'];
		$sql = "UPDATE `employees` SET `emp_dob`='$dob',`emp_gender`='$gender',`emp_marital_status`='$marital' WHERE `emp_id` = '$emp_id'";
		mysql_query($sql) or die(mysql_error().$sql);
		$sql = "SELECT * FROM `employees` WHERE `emp_id`='$emp_id'";
		$result = mysql_query($sql);
		$row = mysql_fetch_array($result);
		?>
			<img src="images/icon-1.jpg" width="40" alt="" align="left" />
           	  <h1 class="title">Personal </h1>
              <div style="clear:both;"></div>
                <br />
                
 <table width="480" border="0" cellspacing="0" cellpadding="5" class="table">
  <tr>
    <td width="120">
    <label class="headingline">
    Date of Birth:
    </label>
    </td>
    <td width="290">
    <span class="profile-data"><?php echo date("d M, Y",strtotime($row['emp_dob'])); ?></span>
    </td>
  </tr>
  <tr>
    <td>
    <label class="headingline">
    Gender:
    </label>
    </td>
    <td>
	<span class="profile-data"><?php  if($row['emp_gender'] == "M") echo "Male"; else echo "Female"; ?></span>
    </td>
  </tr>
  <tr>
    <td><label class="headingline">Marital Status:</label></td>
    <td>
    <span class="profile-data"><?php if($row['emp_marital_status'] == "M") echo "Married"; else echo "Single"; ?></span>
    </td>
  </tr>
  <tr>
  	<td>
    <label class="headingline">Attach Resume:</label>
    </td>
  	<td>
    <span class="profile-data">
    <a href="uploads/<?php echo $row['emp_cv_link'] ?>"><img src="images/ms-word-icon.png" width="24" alt="" /><?php echo $row['emp_cv_link'] ?></a>
    </span>
    </td>
  </tr>
  <tr>
  	<td valign="top">
    <label class="headingline">Upload Photo:</label>
    </td>
  	<td><img src="uploads/<?php echo $row['emp_profile_pic'] ?>" width="56" /></td>
  </tr>
</table>
		<?php
	}//End submit_pr
	elseif($_POST['action'] == 'submit_jp')
	{
		$bank_ids = mysql_real_escape_string(stripslashes($_POST['banks_ids']));
		$pref_loc = mysql_real_escape_string(stripslashes($_POST['pref_loc']));
		$emp_id = $_SESSION['mbc_user_id'];
		$sql = "UPDATE `employees` SET `emp_expected_location` = '$pref_loc' WHERE `emp_id`='$emp_id'";
		mysql_query($sql) or die(mysql_query().$sql);
		$sql = "DELETE FROM `emp_prferences` WHERE `emp_id` = '$emp_id'";
		mysql_query($sql) or die(mysql_query().$sql);
		$bank_id = explode(",",$bank_ids);
		foreach($bank_id as $bi)
		{
			if(!empty($bi))
			{
				$sql = "INSERT INTO `emp_prferences`( `emp_id`, `bank_id`) VALUES ('$emp_id','$bi')";
				mysql_query($sql);
			}
		}
		$sql = "SELECT ep.*, b.* FROM `emp_prferences` AS ep, `banks` AS b WHERE ep.`emp_id` = '$emp_id' AND ep.`bank_id`=b.`bank_id`";
		$result = mysql_query($sql)or die(mysql_error().$sql);
		while($row = mysql_fetch_array($result))
		{
			$banks .= $row['bank_name'].", ";
		}
		$sql = "SELECT * FROM `employees` WHERE `emp_id`='$emp_id'";
		$result = mysql_query($sql) or die(mysql_query().$sql);
		$row = mysql_fetch_array($result);
?>
<script> window.location='profile.php'; </script>
		<?php /*?><img src="images/icon-4.jpg" width="40" alt="" align="left" />
           	  <h1 class="title">Job Preference <a href="employer.html#job-preference"><img src="images/edit-icon.png" width="16" alt="Edit"/></a></h1>
              <div style="clear:both;"></div>
                <br />
                
 <table width="480" border="0" cellspacing="0" cellpadding="5" class="table">
  <tr>
    <td width="120">
    <label class="headingline">
      Location: </label>
    </td>
    <td width="290">
    <span class="profile-data"><?php echo $row['emp_expected_location'] ?></span>
      <div style="clear:both;"></div> 
    </td>
  </tr>
  <tr>
    <td>
    <label class="headingline">
      Banks: </label>
    </td>
    <td>
    <span class="profile-data"><?php echo $banks ?></span>
    </td>
  </tr>
</table><?php */?>
<?php 
	}
}

?>
