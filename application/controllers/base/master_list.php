<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Model :List_bl
 * Enter description here ...
 * @author sunil
 * 
 */
class Master_List extends MBC_Controller {

	public function banks($format,$keyword,$ctrl_name='hddl_auto_banks'){
		$this->load->model('bl/core/List_Bl','master');
		$data = $this->master->get_banks("banks",$keyword,false);
		
		if(strtolower($format) =='json')
		{
			$format_data =array();
			foreach ($data as $key=>$value)
			{
				$format_data["options"] = array(array("id"=>$key,"name" =>$value));
			} 
			
			echo json_encode($format_data);
		}
		else if($format =='html')
		{
			echo form_dropdown($ctrl_name, $data, 'id="'.$ctrl_name.'"  class=""') ;
			/*foreach ($data as $key=>$value)
			{
				echo "<li id='".$key."'>". $value ."</li>";
			} */
		}
		else
		{
			
		}
	}
	/**
	 * 
	 * Function to return branches for a given bank
	 * @param int $bank_id : bank id
	 * @param $format : json,html
	 */
	public function branches($bank_id,$format,$location_title=false){
		$this->load->model('bl/core/list_bl','list_model');
		$data = $this->list_model->get_branches($bank_id,$location_title,false);
		if(strtolower($format) =='json')
		{
			$format_data =array();
			foreach ($data as $key=>$value)
			{
				$format_data[] = array("id"=>$key,"name" =>$value);
			} 
			echo json_encode($format_data);
		}
		else if($format =='html')
		{
			echo form_dropdown('hddl_auto_banks_branches', $data, 'id="hddl_auto_banks_branches"  class=""') ;
		}
	}
	public function locations($format,$keyword){
		$this->load->model('bl/core/locations_bl','master');
		$data = $this->master->get_locations($keyword,false);
		if(strtolower($format) =='json')
		{
			$format_data =array();
			foreach ($data as $key=>$value)
			{
				$format_data["options"][] = array("id"=>$value->lm_id,"name" =>$value->city . " , ".$value->states );
			} 
			echo json_encode($format_data);
		}
	}
}