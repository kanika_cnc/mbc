<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Model : candidate_bl.php
 * Model : list_bl.php  
 * Model : 
 * Class accesbile to valid logged in mbc users 
 * for  upload candidate 
 * @author fintotal
 *   
 */  
class Candidate_upload extends MBC_Controller {
    private $i = 1;
	private $command = ''; 
	private $page =array(); 
	public function __construct ()
    { 
    	parent::__construct();
    }
	public function index($uri='',$errors='',$messages='')
	{
		$this->command = $this->input->post('submit');
		$this->load_models();
		$this->load_libraries();
		$this->load_form_params($uri);
    	$this->load_masters();	
    	$this->load_form_data($uri,$this->command,$errors,$messages);
		$this->page['page_name'] = "upload";
    	$this->load_view();
	}
	private function load_models()
	{
		$this->load->model('bl/candidate/candidate_bl','obj_cand_manager');
	}
	public function candidate($uri='',$error='',$messages='')
	{
		$this->index($uri,$error,$messages);
	}
	private function load_libraries()
	{
		$this->load->library('form_validation');
	}
	private function load_form_params($uri='')
	{
		$this->page["params"] = array(
            'id' => 'candidate_upload',
            'url' => uri_string(),
			'uri' => '',		
        	'logged_in_user_id' => $this->get_loggedin_user_id()
        	);
        
	}
	private function load_masters()
	{
		$this->load->model('bl/core/list_bl','objMaster');
		$this->load->model('bl/core/departments_bl','objDepartments');
		$this->load->model('bl/core/banks_bl','objBanks');
		
		$this->page["masters"]["bank"] = $this->objMaster->get_banks_drop_down_list('',true);
		$this->page["masters"]["departments"] = $this->objDepartments->get_departments(); 
		$this->page["masters"]["hbanks"] = $this->objBanks->get_banks_hierarchical_list();
		$this->page["masters"]["exp_years"] = $this->objMaster->get_numbers_dropdown(1,30,array(array('0','Years',0),array('31','31+',9999)));
		$this->page["masters"]["exp_months"] =  $this->objMaster->get_numbers_dropdown(1,12,array(array('0','Months',0)));
		$this->page["masters"]["days"] = $this->objMaster->get_numbers_dropdown(1,31,array(array("",'Day',0)));
		$this->page["masters"]["dob_years"] = $this->objMaster->get_numbers_dropdown(1960,date("Y"),array(array("","Year",0)));
		$this->page["masters"]["dob_months"] =  $this->objMaster->get_numbers_dropdown(1,12,array(array('','Months',0)));
		$this->page["masters"]["ctc_lakhs"] =  $this->objMaster->get_numbers_dropdown(0,50,array(array('0','Lacs',0),array('51','51+',9999)));
		$this->page["masters"]["ctc_thousands"] = $this->objMaster->get_numbers_dropdown(0,99,array(array('','Thousand',0)),5);
		$this->page["masters"]["designations"] = $this->objMaster->get_designation('',true);
		$this->page["masters"]["age"] = $this->objMaster->get_numbers_dropdown(15,60,array(array('0','Years',0),array('61','61+',9999)));
		
	}
	private function save_form()
    {
    	$candidate = $this->post_data_to_object();
    	$is_new_can = (is_null($candidate->user_id) || $candidate->user_id==0)?true:false; 
    	if($is_new_can)
        {
			$candidate->user_password = $this->auth_acl->get_random_password();
			$candidate->user_id = null;
		}
		$user_id=  $this->obj_cand_manager->save($candidate,$this->get_loggedin_user_id(),"");
		return $user_id;
    }
	private function load_form_data($uri,$mode="New",$errors,$messages)
	{
			if($mode == "Save" || $mode == "Verify And Save")
			{
				
				if (!$this->validate_form())
				{
					$this->load_form_data($uri,'Invalid',$errors,$messages);
				}
				else
				{
					try {
							
						$user_id = $this->save_form();
						//if verify the save is successfull then redirect the user to
						switch($this->command)
				    	{
				    		case 'Verify And Save':
				    			$this->session->set_flashdata('message', 'Candidate information uploaded successfully.');
				    			redirect(base_url()."mbc/candidate_upload/candidate");
				    		default:
				    			$this->session->set_flashdata('message', 'Candidate information uploaded successfully.');
				    			redirect(base_url()."mbc/candidate_upload/candidate/".$user_id);		
				    	}
						
					}
					catch(Exception $ex)
					{
						$candidate = $this->object_to_invalid_post_data($this->post_data_to_object());
						$this->page["data"] = $candidate;
						$errors = $ex->getMessage();
					}
				}
			}
			elseif($mode == 'Invalid')
			{
				
				$candidate = $this->object_to_invalid_post_data($this->post_data_to_object());
				$this->page["data"] = $candidate;
			}
			else
			{
				if(empty($uri))
				{
				$candidate = new Candidate();
				}
				else
				{
				$candidate = $this->obj_cand_manager->get_by_can_user_id($uri,-1);
    			$candidate = $this->object_to_post_data($candidate);
				}
				$this->page["data"] = $candidate;
			}
		$this->page['errors'] = $errors;
		$this->page['messages'] = $messages;
		$this->i++;
		//debug($this->page);
	}
	private function validate_form()
    {
    	$is_valid = false;
    	switch($this->command)
    	{
    		case 'Save':
    			$is_valid = $this->form_validation->run('mbc_candidate_upload_save');
    			break;
    		case 'Verify And Save':
    			$is_valid = $this->form_validation->run('admin_candidate_upload_verify_save');
    			break;
    	}
    	return $is_valid;
    }
	private function post_data_to_object()
    {
    	$candidate = new Candidate();
    	$form_data = $this->input->post();
    	$candidate = objectToObject(array_to_object($form_data),"Candidate");
		
		$candidate->user_id = $form_data["user_id"];
		if($this->command == 'Verify And Save')
		{
			$candidate->can_verified_by = $this->get_loggedin_user_id();
			$candidate->can_verified_date = date("Y-m-d H:i:s");
		}
		$candidate->can_user_id = $form_data["can_user_id"];    	
    	$candidate->can_total_experience = get_months($form_data["ddl_exp_years"],$form_data["ddl_exp_months"]);
    	$candidate->user_departments =  $this->format_department($form_data,$candidate->user_id);
    	$candidate->can_ctc = $form_data["can_ctc_lks"] .".". $form_data["can_ctc_thousands"];
    	if($form_data["dob_month"] || $form_data["dob_day"] || $form_data["dob_year"])
    	{
    		$candidate->can_dob = $form_data["dob_year"] ."-" . $form_data["dob_month"] ."-". $form_data["dob_day"];
    	}
		$candidate->can_resume_file = $form_data["f1Resume_h"];
		$candidate->can_photo = $form_data["f1Photos_h"];
		$candidate->can_location_name = $form_data["txtCurrLocation"];
		//debug($candidate);
    	return $candidate; 
    }
	private function format_department($form_data,$user_id)
    {
    	$user_departments = array();
    	if(!empty($form_data["ud_department_id"]))
    	{
    	$dep_arr= (array)$form_data["ud_department_id"];	
    	$dep_is_curr= (array)$form_data["ud_is_current"];
    	$dep_rel_exp= (array)$form_data["ud_relevant_experince"];
    	$dep_is_int= (array)$form_data["ud_is_interested"];
		$dep_desc = (array)$form_data["ud_dm_description"];
    	$parent_dep = "";
    	$index =0;
    	foreach($dep_arr as $departments)
  		{
  			$dep =  explode('/',$departments);
  			for ($i = 0; $i < count($dep); $i++) {
	  			if($i == count($dep)-1)
	  			{
	  				$user_department = new User_Departments();
					$user_department->ud_department_id = $dep[$i];
					$user_department->ud_user_id = $user_id;
					$user_department->ud_relevant_experince = $dep_rel_exp[$index];
					$user_department->ud_is_current = $dep_is_curr[$index];
					$user_department->ud_is_interested = $dep_is_int[$index];
					$user_department->dm_path = $dep_desc[$index];
					$user_departments[] = (array)$user_department;
	  			}
	  			else
	  			{
	  				$parent_dep .= $dep[$i] .",";
	  			}
  			}
			$index ++;
  		
  		}
  		$parent_dep = explode(',',$parent_dep);
  		$uni_par = array_unique($parent_dep);
    	foreach($uni_par as $par)
  		{
  			if($par !="")
  			{
	  			$user_department = new User_Departments();
				$user_department->ud_department_id = $par;
				$user_department->ud_user_id = $user_id;
				$user_department->ud_relevant_experince = 0;
				$user_department->ud_is_current = false;
				$user_department->ud_is_interested = false;
				$user_departments[] = (array)$user_department;
  			}
  		}
    	}
    	
    	return $user_departments;
    }
	private function load_view()
    {
    	$this->masterpage->setMasterPage('mbc/master');
		$this->masterpage->setPageTitle('Candidate Upload:');
		$this->masterpage->addContentPage('mbc/edit_candidate', 'content',$this->page);
        $this->masterpage->show();
    }
	private function object_to_post_data($candidate)
    {
    	$str="";
    	if(!empty($candidate->can_prev_experince))
    	{
	    	foreach($candidate->can_prev_experince as $prev)
	    	{
	    		$str .= $prev->ce_bank_id . ',';
	    	}
    	}
    	$candidate->can_prev_experince = $str;
    	
    	if(!empty($candidate->can_curr_experince))
    	{
    		$curarr = (array)$candidate->can_curr_experince;
	    	if(!empty($curarr))
	    	{
	    		foreach($curarr as $curr)
	    		{	
	    			$candidate->can_curr_experince = $curr->ce_bank_id;
	    		}
	    	}
    	}
    	$strcurrdep =  "";
    	$strPrevdep ="";
		$prev_dept_details = array();		
		if(isset($candidate->user_departments))
		{
	    	foreach($candidate->user_departments as $user_department)
	    	{
	    		//$prev_dept_details[$user_department->ud_department_id]['dm_name'] = $user_department->dm_name;
				$prev_dept_details[$user_department->ud_department_id]['dm_description'] = $user_department->dm_path;
				$prev_dept_details[$user_department->ud_department_id]['ud_relevant_experince'] = $user_department->ud_relevant_experince;
				$prev_dept_details[$user_department->ud_department_id]['ud_is_current'] = $user_department->ud_is_current;
				$prev_dept_details[$user_department->ud_department_id]['ud_is_interested'] = $user_department->ud_is_interested;
	    		if($user_department->ud_is_current)
	    		{
	    			$strcurrdep .= $user_department->ud_department_id . ",";
					
	    		}
	    		else
	    		{
	    			$strPrevdep .= $user_department->ud_department_id . ",";

	    		}
	    		
	    	}
		}
    	$candidate =  (array)$candidate;
    	$candidate["user_prev_departments"] = $strPrevdep;
		$candidate["prev_dept_details"] = $prev_dept_details;
    	$candidate["str_user_departments"] = $strcurrdep;
		return $candidate;
    }
	 public function upload_files()
    {
		 $dir = './uploads/';;
		if($_REQUEST['type'] == 'resume')
		{
			$dir = './uploads/resume';
			if (!file_exists($dir) and !is_dir($dir)) {
				 mkdir($dir);         
			} 
		}
		elseif($_REQUEST['type'] == 'photo')
		{
			$dir = './uploads/profile_photo';
			if (!file_exists($dir) and !is_dir($dir)) {
				 mkdir($dir);         
			} 
		}
		$targetFolder = $dir;

		$verifyToken = md5('unique_salt' . $_POST['timestamp']);

		if (!empty($_FILES) && $_POST['token'] == $verifyToken) 
		{
			$tempFile = $_FILES['Filedata']['tmp_name'];
			$targetPath = $targetFolder;
			$filename = strtotime(date('Y-m-d H:i:s')). $_FILES['Filedata']['name'];
			$targetFile = rtrim($targetPath,'/') . '/' .$filename;
			move_uploaded_file($tempFile,$targetFile);
			echo $filename;
		}
	}
    	
	public function required_resume()
	{
		if(empty($_FILES['flResume']['name']))
		{
			$this->form_validation->set_message('required_resume', 'Please Upload Resume');
			return false;
		}
		else return true;
	}
	private function object_to_invalid_post_data($candidate)
    {
		//debug($candidate);
    	$str="";
    	$strcurrdep =  "";
    	$strPrevdep ="";
		$resume_file = "";
		$photo_file = "";
		$prev_dept_details = array();
   	
		if(isset($candidate->user_departments))
		{
	    	foreach($candidate->user_departments as $user_department)
	    	{
	    		
	    			$user_department = array_to_object($user_department);
	    			//$prev_dept_details[$user_department->ud_department_id]['dm_name'] = $user_department->dm_name;
					$prev_dept_details[$user_department->ud_department_id]['dm_description'] = $user_department->dm_path;
					$prev_dept_details[$user_department->ud_department_id]['ud_relevant_experince'] = $user_department->ud_relevant_experince;
					$prev_dept_details[$user_department->ud_department_id]['ud_is_current'] = $user_department->ud_is_current;
					$prev_dept_details[$user_department->ud_department_id]['ud_is_interested'] = $user_department->ud_is_interested;
	    		if($user_department->ud_is_current)
	    		{
	    			$strcurrdep .= $user_department->ud_department_id . ",";
	    		}
	    		else
	    		{
	    			$strPrevdep .= $user_department->ud_department_id . ",";
					
					
	    		}
	    		
	    	}
			$resume_file = $candidate->f1Resume_h;
			$photo_file = $candidate->f1Photos_h;
			$candidate =  (array)$candidate;
    		$candidate["user_prev_departments"] = $strPrevdep;
			$candidate["prev_dept_details"] = $prev_dept_details;
    		$candidate["str_user_departments"] = $strcurrdep;
			$candidate["can_resume_file"] = $resume_file;
			$candidate["can_photo"] = $photo_file;
			
		}
    	
		
    	
		//debug($candidate);
    	return $candidate;
    }
    public function required_total_experience()
	{
		$years = $this->input->post('ddl_exp_years');
		$months = $this->input->post('ddl_exp_months');
		if($years <= 0 && $months <= 0)
		{
			$this->form_validation->set_message('required_total_experience', 'Total Experince field is required.');
			return false;
		}
		return TRUE;
	}
	
	public function required_current_ctc()
	{
		$can_ctc_lks = $this->input->post('can_ctc_lks');
		$can_ctc_thousands = $this->input->post('can_ctc_thousands');
		if($can_ctc_lks <= 0 && $can_ctc_thousands <= 0)
		{
			$this->form_validation->set_message('required_current_ctc', 'Current CTC field is required.');
			return false;
		}
		return TRUE;
	}
	public function required_age()
	{
		$can_age = $this->input->post('can_age');
		$can_day = $this->input->post('dob_day');
		$can_month = $this->input->post('dob_month');
		$can_year = $this->input->post('dob_year');
		$hasctc = ($can_age <= 0 ) ? false : true;
		$has_dob =  false;
		if($can_day != "" && $can_month != "" && $can_year != "")
		{
			$has_dob = true;	
		}
		if(!$hasctc && !$has_dob)
		{
			$this->form_validation->set_message('required_age', 'Age or Date of Birth field is required.');
			return false;
		}
		return TRUE;
	}
	public function invalid_date()
	{
		$can_day = $this->input->post('dob_day');
		$can_month = $this->input->post('dob_month');
		$can_year = $this->input->post('dob_year');
		$has_dob =  false;
		if($can_day == "" && $can_month == "" && $can_year == "")
		{
			$has_dob = true;	
		}
		if($can_day != "" && $can_month != "" && $can_year != "")
		{
			$has_dob = true;	
		}
		if(!$has_dob)
		{
			$this->form_validation->set_message('invalid_date', 'Invalid date of birth.');
			return false;
		}
		else
		{
			$can_age = $this->input->post('can_age');
			$has_dob =  false;
			if(trim($can_day) != "" && trim($can_month) != "" && trim($can_year) != "")
			{
				$has_dob = true;	
			}
			if($can_age  > 0 && $has_dob)
			{
				$tmp_dob_ = $can_year ."-" . $can_month ."-". $can_day;
				$tmp_dob = new DateTime($tmp_dob_);
				if($can_age != get_age($tmp_dob))
				{
					$this->form_validation->set_message('invalid_date', 'Age and Date of Birth does not match.');
					return false;
				}			
			}
		}
		return TRUE;
	}
}