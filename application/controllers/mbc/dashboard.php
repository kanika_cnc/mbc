<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 * Class accesbile to valid logged in mbc users 
 * for  dashboard
 * @author CNC
 *
 */
class Dashboard extends MBC_Controller {
	
	private $content = '';
	
	public function __construct () 
    {
    	parent::__construct();
    }	
   //page level access if user can access this page or not
	public function index()
	{ 
		$this->content["page_name"] = "dashboard";
		$this->coming_soon('17 Dec 2013');
		/*$this->masterpage->setMasterPage ('mbc/master');
		$this->masterpage->setPageTitle('Welcome to Admin Panel Dashboard ');
        $this->masterpage->addContentPage ('mbc/dashboard', 'content' );
        $this->masterpage->show();*/
	}
	
	
	public function verify()
	{
		$this->content["page_name"] = "verify";
		$this->coming_soon('','next');
	}
	public function recruitment()
	{
		$this->content["page_name"] = "recrutment_assistance";
		$this->coming_soon('','next');
	}
	public function coming_soon($rel_date='',$rel_build='current')
	{
		$this->masterpage->setMasterPage ('mbc/master');
		$this->masterpage->setPageTitle('Coming Soon ');
		$this->content["rel_date"] = $rel_date;
		$this->content["rel_build"] = $rel_build;
        $this->masterpage->addContentPage ('mbc/coming_soon', 'content',$this->content);
        $this->masterpage->show();
	}
	
}

