<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Model : mbc_user_bl.php
 * Class accesbile to valid logged in mbc users 
 * for User Management
 * @author CNC
 *    
 */ 
class Retrievals extends MBC_Controller {

	private $page ='';
	public $content;
	public function __construct () 
    {
    	parent::__construct();
    	
    }
	public function index()
	{
		//$this->coming_soon('3-Dec-2013');
		//$this->mbc_user();
		$this->load_models();
		$this->set_data();
		$this->page['page_name'] = "retrievals";
		if($this->input->post('search') == 'submit')
		$this->load->view('mbc/retrievals_table',$this->page);
		elseif($this->input->post('candidates') == 'submit')
		$this->load->view('mbc/retrievals_candidate_table',$this->page);
		else
		$this->load_view();
	}
	private function load_view()
    {
    	$this->masterpage->setMasterPage('mbc/master');
		$this->masterpage->setPageTitle('Candidate Upload:');
		$this->masterpage->addContentPage('mbc/retrievals', 'content',$this->page);
        $this->masterpage->show();
    }
	private function load_models()
	{
		$this->load->model('bl/candidate/candidate_bl','obj_cand_manager');
		$this->load->model('bl/core/list_bl','objMaster');
		$this->load->model('bl/core/departments_bl','objDepartments');
		$this->load->model('bl/core/banks_bl','objBanks');
	}
	private function set_data()
	{
		
		$this->page["data"]["banks"] = "";
		$this->page["data"]["designations"] = "";
		$this->page["data"]["departments"] = "";
		$this->page["data"]["candidates"] = array();
		if($this->input->post('search'))
		{
			
			$this->page["data"]["banks"] = $this->input->post('banks');
			$this->page["data"]["designations"] = $this->input->post('designations');
			$this->page["data"]["departments"] = $this->input->post('departments');
			
		}
		if($this->input->post('candidates'))
		{
			$this->page["data"]["banks"] = $this->input->post('banks');
			$this->page["data"]["designations"] = $this->input->post('designations');
			$this->page["data"]["departments"] = $this->input->post('departments');
			$this->page["data"]["candidates"] = $this->obj_cand_manager->search_candidate($this->input->post('departments'),$this->input->post('banks'),$this->input->post('designations'));
		}
		$this->page["masters"]["bank"] = $this->objMaster->get_banks_drop_down_list('',true);
		$data_formatted = array();
		$options = $this->objDepartments->get_departments_with_users($this->page["data"]["banks"],$this->page["data"]["designations"]);
		//debug($options);
		$this->page["masters"]["departments"] = $options;
		foreach($options as $index => $row)
    	{
    	  	$reformat_row = array();
    	  	$row = (array)$row;	
          	$reformat_row["title"] = $row["title"];
          	$reformat_row["key"] = $row["key"];  
          	$reformat_row["parent_key"] = $row["parent_key"];
		  	$reformat_row["count"] = $row["count"];
		  	if($this->input->post('designations') != "" || $this->input->post('banks') != "")
		  	{
		  		if($row["count"] != '(0)')
          		$data_formatted[] = $reformat_row;
		  	}
		  else
		  	$data_formatted[] = $reformat_row;
    }
		
		$this->page["masters"]["departments_tree"] = buildTree((array)$data_formatted,0,'parent_key','key',false,0);
		$this->page["masters"]["designations"] = $this->objMaster->get_designation('',true);
		
	}
	
}


