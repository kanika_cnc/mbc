<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Model : candidate_bl.php
 * Model : list_bl.php
 * Model : 
 * Class accesbile to valid logged in mbc users 
 * for  upload candidate
 * @author fintotal
 * 
 */  
class Candidate_upload_old extends MBC_Controller {
 
	private $is_edit_mode= false;
	private $command = '';
	private $page ='';
	
	public function __construct ()  
    {
    	parent::__construct();
    }
 
	public function index($uri='',$error='',$messages='')
	{
		
		$this->load_models();
		$this->load_libraries();
		$this->load_form($uri,$error,$messages);
	}
	
	public function candidate($uri='',$error='',$messages='')
	{
		$this->index($uri,$error,$messages);
	}
	
	private function load_models()
	{
		$this->load->model('bl/candidate/candidate_bl','obj_cand_manager');
	}
	private function load_libraries()
	{
		$this->load->library('form_validation');
	}
    private function load_form($uri='',$error='',$messages='')
    {
    	$this->command = $this->input->post('submit');
    	$this->is_edit_mode = !$this->command ? false :true;
    	if(!$this->is_edit_mode)
    	{
    		if($uri =='')
    		{
	   			$this->render_form($uri,'new',$error,$messages);
    		}
    		else 
    		{
    			$this->render_form($uri,'edit',$error,$messages);
    		}
    	}
    	else 
    	{
    		if (!$this->validate_form())
    		{
    			$this->render_form($uri,'invalid',$error,$messages);
    		}
    		else
    		{
    			try {	
					$user_id = $this->save_form();
					//$this->render_form($user_id,'edit',$error,'Saved successfully');
					redirect(base_url()."mbc/candidate_upload/index/".$user_id);
    			}
    			catch(Exception $ex)
    			{
    				$this->render_form($uri,'invalid',$ex->getMessage(),$messages);
    			}
    		}
    	}
    }
    private function validate_form()
    {
    	$is_valid = false;
    	switch($this->command)
    	{
    		case 'Save':
    			$is_valid = $this->form_validation->run('mbc_candidate_upload_save');
    			break;
    		case 'Verify And Save':
    			$is_valid = $this->form_validation->run('admin_candidate_upload_verify_save');
    			break;
    	}
    	return $is_valid;
    }
    
	private function render_form($uri='',$mode='new',$error='',$messages='')
	{
    	$this->load_page_params($uri);
    	$this->load_masters();	
    	$this->load_form_data($uri,$mode,$error,$messages);
    	$this->load_view($uri);
	}
	private function load_page_params($uri='')
	{
		$this->page["params"] = array(
            'id' => 'candidate_upload',
            'url' => uri_string(),
			'uri' => '',		
        	'logged_in_user_id' => $this->get_loggedin_user_id()
        	);
        
	}
	private function load_masters()
	{
		$this->load->model('bl/core/list_bl','objMaster');
		$this->load->model('bl/core/departments_bl','objDepartments');
		$this->load->model('bl/core/banks_bl','objBanks');
		
		$this->page["masters"]["bank"] = $this->objMaster->get_banks_drop_down_list('',true);
		$this->page["masters"]["departments"] = $this->objDepartments->get_departments(); 
		$this->page["masters"]["hbanks"] = $this->objBanks->get_banks_hierarchical_list();
		$this->page["masters"]["exp_years"] = $this->objMaster->get_numbers_dropdown(1,30,array(array('0','Years',0),array('31','31+',9999)));
		$this->page["masters"]["exp_months"] =  $this->objMaster->get_numbers_dropdown(1,12,array(array('0','Months',0)));
		$this->page["masters"]["days"] = $this->objMaster->get_numbers_dropdown(1,31,array(array("",'Day',0)));
		$this->page["masters"]["dob_years"] = $this->objMaster->get_numbers_dropdown(1960,date("Y"),array(array("","Year",0)));
		$this->page["masters"]["dob_months"] =  $this->objMaster->get_numbers_dropdown(1,12,array(array('','Months',0)));
		$this->page["masters"]["ctc_lakhs"] =  $this->objMaster->get_numbers_dropdown(0,50,array(array('0','Lacs',0),array('51','51+',9999)));
		$this->page["masters"]["ctc_thousands"] = $this->objMaster->get_numbers_dropdown(0,99,array(array('','Thousand',0)),5);
		$this->page["masters"]["designations"] = $this->objMaster->get_designation('',true);
		$this->page["masters"]["age"] = $this->objMaster->get_numbers_dropdown(15,60,array(array('0','Years',0),array('61','61+',9999)));
		
	}
    private function load_form_data($uri,$mode,$error='',$messages='')
    {
    	$candidate = new Candidate();
    	switch($mode)
    	{

    				$this->page["data"] = $this->object_to_post_data($candidate);	
    			break;
    		case 'edit':
    				$candidate = $this->obj_cand_manager->get_by_can_user_id($uri,-1);
    				$candidate = $this->object_to_post_data($candidate);
    				//debug($candidate);
    				$this->page["data"] = $candidate;
    			break;
    		case 'invalid':
    				$candidate = $this->post_data_to_object();
    				//debug($candidate);
    				$this->page["data"] = $this->object_to_invalid_post_data($candidate); //$this->object_to_post_data($candidate);
    			break;
    		default:
    			$this->page["data"] = $candidate;
    	}
    	$this->page["errors"] = $error;
    	$this->page["messages"] = $messages;
    }
	private function load_view()
    {
    	$this->masterpage->setMasterPage('mbc/master');
		$this->masterpage->setPageTitle('Candidate Upload:');
		$this->masterpage->addContentPage('mbc/edit_candidate', 'content',$this->page);
        $this->masterpage->show();
    }
    
    private function post_data_to_object()
    {
    	$candidate = new Candidate();
    	$form_data = $this->input->post();
    	$candidate = objectToObject(array_to_object($form_data),"Candidate");
    	
		$candidate->user_id = $form_data["user_id"];
		if($this->command == 'Verify And Save')
		{
			$candidate->can_verified_by = $this->get_loggedin_user_id();
			$candidate->can_verified_date = date("Y-m-d H:i:s");
		}
		$candidate->can_user_id = $form_data["can_user_id"];    	
    	$candidate->can_total_experience = get_months($form_data["ddl_exp_years"],$form_data["ddl_exp_months"]);
    	$candidate->user_departments =  $this->format_department($form_data,$candidate->user_id);
    	$candidate->can_ctc = $form_data["can_ctc_lks"] .".". $form_data["can_ctc_thousands"];
    	if($form_data["dob_month"])
    	{
    		$candidate->can_dob = $form_data["dob_month"] ."/" . $form_data["dob_day"] ."/". $form_data["dob_year"];
    	}
    	
    	$Candidate_Qualifications = array();
    	foreach($form_data["can_grad_qualifications"] as $value=>$key) 
    	{
    		$Candidate_Qualification = new Candidate_Qualifications();
    		$Candidate_Qualification->cq_user_id = $candidate->user_id;
    		$Candidate_Qualification->cq_qualification_master_id = $key;
    		$Candidate_Qualification->cq_qualification_type_id = Qulification_TYPE::GRADUATION;
    		$Candidate_Qualifications[] =(array) $Candidate_Qualification;
    	} 
    	if(isset($form_data["can_post_grd_qualifications"]))
    	{
	    	foreach($form_data["can_post_grd_qualifications"] as $value=>$key) 
	    	{
	    		$Candidate_Qualification = new Candidate_Qualifications();
	    		$Candidate_Qualification->cq_user_id = $candidate->user_id;
	    		$Candidate_Qualification->cq_qualification_master_id = $key;
	    		$Candidate_Qualification->cq_qualification_type_id = Qulification_TYPE::POSTGRADUATION;
	    		$Candidate_Qualifications[] =(array) $Candidate_Qualification;
	    	}
    	} 
    	$candidate->can_qualifications = (array) $Candidate_Qualifications;
    		
    	/*$Candidate_Departments = array();
		if(isset($form_data["ud_department_id"]))
		{
			foreach($form_data["ud_department_id"] as $value=>$key)
			{
				$Candidate_Department = new User_Departments();
				$Candidate_Department->ud_department_id = $key;
				$Candidate_Department->ud_relevant_experince = $form_data["ud_relevant_experince"][$value];
				$Candidate_Department->ud_is_current = $form_data["ud_is_current"][$value];
				$Candidate_Department->ud_is_interested = $form_data["ud_is_interested"][$value];	
				$Candidate_Departments[] =(array) $Candidate_Department;
			}
		}
		//$candidate->user_departments = (array)$Candidate_Departments;
    	//debug($candidate);*/
    	return $candidate; 
    
    }
	
	private function format_department($form_data,$user_id)
    {
    	$user_departments = array();
    	
    	if(!empty($form_data["ud_department_id"]))
    	{
    	$dep_arr= (array)$form_data["ud_department_id"];	
    	$dep_is_curr= (array)$form_data["ud_is_current"];
    	$dep_rel_exp= (array)$form_data["ud_relevant_experince"];
    	$dep_is_int= (array)$form_data["ud_is_interested"];
    	$parent_dep = "";
    	
    	$index =0;
    	foreach($dep_arr as $departments)
  		{
  			$dep =  explode('/',$departments);
  			for ($i = 0; $i < count($dep); $i++) {
	  			if($i == count($dep)-1)
	  			{
	  				$user_department = new User_Departments();
					$user_department->ud_department_id = $dep[$i];
					$user_department->ud_user_id = $user_id;
					$user_department->ud_relevant_experince = $dep_rel_exp[$index];
					$user_department->ud_is_current = $dep_is_curr[$index];
					$user_department->ud_is_interested = $dep_is_int[$index];
					$user_departments[] = (array)$user_department;
	  			}
	  			else
	  			{
	  				$parent_dep .= $dep[$i] .",";
	  			}
  			}
			$index ++;
  		
  		}
  		//remove duplicate departments
  		$parent_dep = explode(',',$parent_dep);
  		$uni_par = array_unique($parent_dep);
    	foreach($uni_par as $par)
  		{
  			if($par !="")
  			{
	  			$user_department = new User_Departments();
				$user_department->ud_department_id = $par;
				$user_department->ud_user_id = $user_id;
				$user_department->ud_relevant_experince = 0;
				$user_department->ud_is_current = false;
				$user_department->ud_is_interested = false;
				$user_departments[] = (array)$user_department;
  			}
  		}
    	}
    	
    	return $user_departments;
    }
	private function object_to_invalid_post_data($candidate)
    {
    	$str="";
    	$strcurrdep =  "";
    	$strPrevdep ="";
		$prev_dept_details = array();
		if(isset($candidate->user_departments))
		{
	    	foreach($candidate->user_departments as $user_department)
	    	{
	    		
	    		$user_department = array_to_object($user_department);
	    		 
	    		if($user_department->ud_is_current)
	    		{
	    			$strcurrdep .= $user_department->ud_department_id . ",";
	    		}
	    		else
	    		{
	    			$strPrevdep .= $user_department->ud_department_id . ",";
					
					$prev_dept_details[$user_department->ud_department_id]['dm_name'] = $user_department->dm_name;
					$prev_dept_details[$user_department->ud_department_id]['dm_description'] = $user_department->dm_description;
					$prev_dept_details[$user_department->ud_department_id]['ud_relevant_experince'] = $user_department->ud_relevant_experince;
					$prev_dept_details[$user_department->ud_department_id]['ud_is_current'] = $user_department->ud_is_current;
					$prev_dept_details[$user_department->ud_department_id]['ud_is_interested'] = $user_department->ud_is_interested;
	    		}
	    		
	    	}
		}
    	$candidate =  (array)$candidate;
    	$candidate["user_prev_departments"] = $strPrevdep;
		$candidate["prev_dept_details"] = $prev_dept_details;
    	$candidate["str_user_departments"] = $strcurrdep;
    	return $candidate;
    }
	private function object_to_post_data($candidate)
    {
    	$str="";
    	if(!empty($candidate->can_prev_experince))
    	{
	    	foreach($candidate->can_prev_experince as $prev)
	    	{
	    		$str .= $prev->ce_bank_id . ',';
	    	}
    	}
    	$candidate->can_prev_experince = $str;
    	
    	if(!empty($candidate->can_curr_experince))
    	{
    		$curarr = (array)$candidate->can_curr_experince;
	    	if(!empty($curarr))
	    	{
	    		foreach($curarr as $curr)
	    		{	
	    			$candidate->can_curr_experince = $curr->ce_bank_id;
	    		}
	    	}
    	}
    	$strcurrdep =  "";
    	$strPrevdep ="";
		$prev_dept_details = array();
		//debug($candidate->user_departments);
		if(isset($candidate->user_departments))
		{
	    	foreach($candidate->user_departments as $user_department)
	    	{
	    		 
	    		if($user_department->ud_is_current)
	    		{
	    			$strcurrdep .= $user_department->ud_department_id . ",";
	    		}
	    		else
	    		{
	    			$strPrevdep .= $user_department->ud_department_id . ",";
					
					$prev_dept_details[$user_department->ud_department_id]['dm_name'] = $user_department->dm_name;
					$prev_dept_details[$user_department->ud_department_id]['dm_description'] = $user_department->dm_description;
					$prev_dept_details[$user_department->ud_department_id]['ud_relevant_experince'] = $user_department->ud_relevant_experince;
					$prev_dept_details[$user_department->ud_department_id]['ud_is_current'] = $user_department->ud_is_current;
					$prev_dept_details[$user_department->ud_department_id]['ud_is_interested'] = $user_department->ud_is_interested;
	    		}
	    		
	    	}
		}
    	$candidate =  (array)$candidate;
    	if(isset($candidate["can_qualifications"]))
    	{
	    	foreach ($candidate["can_qualifications"] as $qaul)
	    	{
	    		
	    			if($qaul->cq_qualification_type_id == Qulification_TYPE::POSTGRADUATION)
	    			{
	    				$candidate["can_post_grd_qualifications"][] = $qaul->cq_qualification_master_id ;
	    			}	
	    			else
	    			{
	    				$candidate["can_grd_qualifications"][] = $qaul->cq_qualification_master_id ;
	    			}
	    	}
    	}
    	$candidate["user_prev_departments"] = $strPrevdep;
		$candidate["prev_dept_details"] = $prev_dept_details;
    	$candidate["str_user_departments"] = $strcurrdep;
		
    	//debug($candidate);
    	return $candidate;
    }
    private function save_form()
    {
    	$candidate = $this->post_data_to_object();
    	$is_new_can = (is_null($candidate->user_id) || $candidate->user_id==0)?true:false; 
		
    	if($is_new_can)
        {
			$candidate->user_password = $this->auth_acl->get_random_password();
			$candidate->user_id = null;
		}
		$user_id=  $this->obj_cand_manager->save($candidate,$this->get_loggedin_user_id(),"");
		/*$candidate->user_id = $user_id;
		$this->obj_cand_manager->save($candidate,$this->get_loggedin_user_id(),"departments");
		$this->obj_cand_manager->save($candidate,$this->get_loggedin_user_id(),"profile");
		$this->obj_cand_manager->save($candidate,$this->get_loggedin_user_id(),"exp");
		$this->obj_cand_manager->save($candidate,$this->get_loggedin_user_id(),"qualifications");*/
		$this->upload_files();
		return $user_id;
    }
    	
    private function upload_files()
    {
		foreach ($_FILES as $key => $value)
		{
			//debug($value);
			if($value["name"] != "")
			{
				$this->load->library('upload');
		    	$this->upload->initialize($this->set_upload_options());
	    		if (!$this->upload->do_upload($key))
				{
					throw new Exception($this->upload->display_errors(), 9999);
				}
				else
				{
					$data = $this->upload->data();
				}
			}
		}
    	
    }
	private function set_upload_options()
	{   
	//  upload an image options
	    $config = array();
	    $config['upload_path'] = './uploads/';
	    $config['allowed_types'] = 'gif|jpg|png|doc|docx|pdf';
	    $config['max_size']      = '0';
	    $config['overwrite']     = FALSE;
	
	
	    return $config;
	}
	public function can_validate()
	{
		//current and prev banks shoudl not overlap
	}
	
	
	
	
}


		