<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
include_once( APPPATH . 'models/dl/entity/user' . EXT );
include_once( APPPATH . 'models/dl/entity/departments' . EXT );
/**
 * 
 * Enter description here ...
 * @author CNC
 *
 */
class User_Bl extends CI_Model {
	
 	public function __construct () 
    {
     	 parent::__construct();
    }
    /**
     * 
     * create the user
     * @param unknown_type $user
     * @todo : created date not saving
     * $todo : move the logic of created date inside
     */
    protected function save_user($user,$login_user)
    {
   	 	$user->user_id = $user->user_id=="" ? null :$user->user_id;
    	$this->db->set("user_id",$user->user_id);
		$this->db->set("user_first_name", $user->user_first_name);
  		$this->db->set("user_last_name",$user->user_last_name);
  		$this->db->set("user_email",$user->user_email);
	  	$this->db->set("user_type_id", ($user->getUserType()));
    	if(is_null($user->user_id)|| ($user->user_id == ""))
    	{
    		$this->db->set("user_password_salt",$user->user_password_salt);
  			$this->db->set("user_password",$user->user_password);
    		$this->db->set("user_created_by",$login_user);
  			$this->db->set("user_created_date",'Now()',false);
  			$this->db->set("user_last_login",$user->user_last_login);
  			$this->db->set("user_is_active",$user->user_is_active);
  			$this->db->set("user_activation_date",$user->user_activation_date);
  			$this->db->set("user_deactived_date",$user->user_deactived_date);
  			$this->db->set("user_is_deleted", $user->user_is_deleted);
		}
    	else
    	{
  			$this->db->set("user_modified_date",'Now()',false);
  			$this->db->set("user_modified_by", $login_user);
    	}
  		$this->db->set("user_contact_mobile",$user->user_contact_mobile);
  		$this->db->set("user_alternate_email_id_1",$user->user_alternate_email_id_1);
		$this->db->set("user_alternate_email_id_2",$user->user_alternate_email_id_2);
  		$this->db->set("user_alternate_email_id_3",$user->user_alternate_email_id_3);
  		$table_name = User::TABLE_NAME;
  		$table_id_col = USER::TABLE_ID_COL;
		if(!is_null($user->getUserType()) && $user->getUserType() !=0)
    	{
	    	if(is_null($user->user_id))
	    	{
	    		if(!$this->is_duplicate_user($user))
	    		{
	    			if(!$this->db->insert($table_name))
	    			{
	    				throw new Exception($this->db->_error_message(), $this->db->_error_number());
	    			}
	        		$insert_id = $this->db->insert_id();
	        		$user->user_id = $insert_id;
	    		}
	    		else 
	    		{
	    			throw new Exception("Duplicate User",99991);
	    		}
	    	}
	    	else
	    	{
	    		
	    		if(!$this->is_duplicate_user($user))
	    		{
	    		 // Update the item
	             $this->db->where($table_id_col, $user->user_id);
	             if(!$this->db->update($table_name))
	             {
	             	throw new Exception($this->db->_error_message(), $this->db->_error_number());
	             }
	    		}
	    		else 
	    		{
	    			throw new Exception("Duplicate User",99991);
	    		}
	    		
	    	}
    	}
    	else
    	{
    		throw new Exception("User Type is not mentioned", 99992); 
    	}
    	
    	return $user->user_id;
    }
    
    /*public function validateUser($email,$password)
    {
    	$obj_user_entity = new User();
		$table = $obj_user_entity->get_user_table_name();
        $this->db->where("{$table}.user_email",$email);
        $this->db->where("{$table}.user_password",$password);
        $query = $this->db->get($table);
        if($query->num_rows == 1)
        {
            $row = $query->row();
            return $row;
        }
        return false;
    }*/
    
    public function get_user_by_user_id($user_id,$is_active=1)
    {
		$obj_user_entity = new User();
		$table = $obj_user_entity->get_user_table_name();
		$this->db->from("{$table}");
        $this->db->select("*");
        if($is_active)
        {
        	$this->db->where("{$table}.user_is_deleted =",0 );
        }
        $this->db->where("{$table}.{$obj_user_entity->get_user_table_id_name()}",$user_id );
        $query = $this->db->get();
        $data = $query->row();
        return $data;
    }
    
    public function get_users_by_type($user_type_id,$format='',$active=1)
    {
    	$filters = array(array("col" => "user_type_id","val"=>$user_type_id,"opt"=>""));
    	return $this->get_users($filters,$active,$format);
    	
    } 
    
    
    public function get_users($filters=array(),$active=1,$format='',$default_row=array())
    {
    	$obj_user_entity = new User();
		$table = $obj_user_entity->get_user_table_name();
		$this->db->from("{$table}");
        $this->db->select("*");
        if($active == 1)
        {
        	$this->db->where("{$table}.user_is_deleted =",0 );
        }
        foreach ($filters as $filter)
    	{
    		if($filter['opt'] == "in")
    		{
    			$this->db->where_in("{$table}.{$filter['col']}",$filter['val']);
    		}
    		else if($filter['opt'] == "not_in")
    		{
    			$this->db->where_not_in("{$table}.{$filter['col']}",$filter['val']);
    		}
    		else
    		{
    			$this->db->where("{$table}.{$filter['col']}",$filter['val']);	
    		}
    	}
        $query = $this->db->get();
        switch(strtolower($format))
        {
        	case 'array':
        		return $query->result_array();
        		break;
        	case 'dropdown':
        			$data = $query->result();
        			$data_formated = $default_row;
        			foreach ($data as $row)
        			{
        				$data_formated[$row->user_id] = $row->user_first_name . $row->user_last_name;
        			}
        			return $data_formated;
        	default:
        		 return $query->result();
        } 
    }
    
    /**
     * 
     * Check if the user with the given email id and contact number exists
     * @param $user
     */
    public function is_duplicate_user($user)
    {
    	if(is_null($user->user_id))
    	{
    		$table = $user->get_user_table_name();
			$this->db->from("{$table}");
        	$this->db->select("*");
        	//$this->db->where("{$table}.user_is_deleted =",0 );
        	$this->db->where("{$table}.user_email",$user->user_email );
        	$this->db->or_where("{$table}.user_alternate_email_id_1",$user->user_email );
        	$this->db->or_where("{$table}.user_alternate_email_id_2",$user->user_email );
        	$this->db->or_where("{$table}.user_alternate_email_id_3",$user->user_email );
        	$this->db->or_where("{$table}.user_contact_mobile",$user->user_contact_mobile );
        	
        	$row = $this->db->count_all_results();
        	if($row > 0)
        		return true;
        	return false;	
        	
    	}
    	else 
    	{
    		$table = $user->get_user_table_name();
			$this->db->from("{$table}");
        	$this->db->select("*");
        	//$this->db->where("{$table}.user_is_deleted =",0 );
			//this statement is duplicated to accomodate group on or where so don;t remove it
        	$this->db->where("{$table}.user_id !=",$user->user_id );
        	$this->db->where("{$table}.user_email",$user->user_email );
        	$this->db->or_where("{$table}.user_alternate_email_id_1",$user->user_email );
        	$this->db->or_where("{$table}.user_alternate_email_id_2",$user->user_email );
        	$this->db->or_where("{$table}.user_alternate_email_id_3",$user->user_email );
        	$this->db->or_where("{$table}.user_contact_mobile",$user->user_contact_mobile );
        	//this statement is duplicated to accomodate group on or where so don;t remove it
        	$this->db->where("{$table}.user_id !=",$user->user_id );
        	$row = $this->db->count_all_results();
        	if($row > 0)
        		return true;
        	return false;
    	}
    }
    
	public function get_user_departments($user_id)
  	{
  		$obj_udept = new User_Departments();
		$obj_dept = new Departments();
		$join_table = $obj_dept->get_table_name();
		$join_id = $obj_dept->get_table_id_name();
		$table = $obj_udept->get_table_name();
		$this->db->from("{$table}");
        $this->db->select("{$table}.*,{$join_table}.dm_description,{$join_table}.dm_name");
		//$this->db->where();
        $this->db->where("{$table}.{$obj_udept->get_table_id_name()}",$user_id );
		$this->db->join("{$join_table}","{$join_table}.{$join_id} = {$table}.ud_department_id");
        $this->db->order_by("{$table}.ud_is_current");
		
       	$query = $this->db->get();
        return $query->result();
  	}
	public function get_user_current_departments($user_id)
  	{
  		$obj_udept = new User_Departments();
		$table = $obj_udept->get_table_name();
		$this->db->from("{$table}");
        $this->db->select("*");
        //$this->db->where("{$table}.user_is_deleted =",0 );
        $this->db->where("{$table}.{$obj_udept->get_table_id_name()}",$user_id );
        $this->db->where("{$table}.ud_is_current",1 );
        $this->db->order("{$table}.ud_is_current");
       	$query = $this->db->get();
        return $query->result();
  	}
  	public function edit_user_departments($user_departments,$user_id)
  	{
  			
    		$obj_udept = new User_Departments();
			$table = $obj_udept->get_table_name();
			$id_name = $obj_udept->get_table_id_name();
			//delete records
    		$this->db->where("{$id_name}",$user_id );
			$this->db->delete("{$table}");
			if(!empty($user_departments))
    		{	 
				//insert new records
				$i=0;
				foreach($user_departments as $ud)
				{
					$ud["ud_user_id"] =  $user_id;
					$ud_f[$i] = elements(array( 'ud_department_id', 'ud_is_current', 'ud_is_interested', 'ud_relevant_experince', 'ud_user_id'),(array)$ud);
					$i++;
				}
				$this->db->insert_batch("{$table}", $ud_f);
    		}
  	}
	

  	/**
  	 * 
  	 * function to save other profile attributes in the databse 
  	 * @param unknown_type $user_profile_attributes
  	 * @param unknown_type $login_user
  	 * @throws Exception
  	 */
  	protected function edit_user_profile_attributes($user_profile_attributes,$login_user,$user_id,$user_type_id)
  	{
  		if(!empty($user_profile_attributes))
  		{
  			foreach ($user_profile_attributes as $p_attr)
  			{
  				$obj_p_attr = new User_Profile_Attributes();
  				$obj_p_attr = $p_attr;
  				if($obj_p_attr->uttr_id=="")
  				{
  					$obj_p_attr->uttr_created_by =$login_user;
  					$obj_p_attr->uattr_user_id = $user_id;
  					
  					$this->delete_duplicate_profile_attr($user_id, $obj_p_attr->uttr_bank_id, $obj_p_attr->uttr_bank_id);
  					$obj_p_attr_ = object_to_array($obj_p_attr);
  					foreach ($obj_p_attr_ as $key=>$value)
  					{
  						$this->db->set($key,$value);
  					}
  					$this->db->set('uttr_created_date','now()',false);
  					if(!$this->db->insert($obj_p_attr->get_table_name()))
	    			{
	    				throw new Exception($this->db->_error_message(), $this->db->_error_number());
	    			}
  				}
  				else
  				{
  					$obj_p_attr->uttr_modified_by =$login_user;
  					//$obj_p_attr->uttr_modified_date =date('m/d/Y h:m:s');
  					
  					$obj_p_attr->uattr_user_id = $user_id;
  					//$obj_p_attr->uattr_type_id = $user_type_id;
  					$obj_p_attr_ = object_to_array($obj_p_attr);
  					foreach ($obj_p_attr_ as $key=>$value)
  					{
  						$this->db->set($key,$value);
  					}
  					$this->db->set("uttr_modified_date",'now()',false);
	  				 $this->db->where($obj_p_attr->get_table_id_name(), $obj_p_attr->uttr_id);
		             if(!$this->db->update($obj_p_attr->get_table_name()))
		             {
		             	throw new Exception($this->db->_error_message(), $this->db->_error_number());
		             }
  				}	
  			}
  			//exit();
  		}
  	}
  	/**
  	 * 
  	 * Method to return all profile attributes for a user
  	 * @param $user_id
  	 * @param $Active
  	 */
  	protected function get_user_profile_attributes($user_id,$Active=-1)
  	{
  		$obj_entity = new User_Profile_Attributes();
  		$col = User_Profile_Attributes::UTTR_USER_ID;
  		$colre =User_Profile_Attributes::UTTR_IS_RELEASED;
		$table = $obj_entity->get_table_name();
		$this->db->from("{$table}");
        $this->db->select("*");
        $this->db->where("{$table}.{$col}",$user_id );
        if($Active == 1)
        {
        	$this->db->where("{$table}.{$colre} ",0 );
        }
       	$query = $this->db->get();
        return $query->result();
  	}
  	
  	protected function release_profile_attribute($user_id,$login_user)
  	{
  		$table = User_Profile_Attributes::TABLE_NAME;
  		$col = User_Profile_Attributes::UTTR_USER_ID;
  		$this->db->where("{$col}", $user_id);
  		
  		$this->db->set(User_Profile_Attributes::UTTR_IS_RELEASED,1);
  		$this->db->set(User_Profile_Attributes::UTTR_MODIFIED_BY,$login_user);
  		$this->db->set(User_Profile_Attributes::UTTR_MODIFIED_DATE,'now()',false);
		$this->db->update("{$table}"); 
  	}
  	
  	private function delete_duplicate_profile_attr($user_id,$bank_id,$rec_id)
  	{
  		$obj_entity = new User_Profile_Attributes();
  		$col = User_Profile_Attributes::UTTR_USER_ID;
  		$col_bank = User_Profile_Attributes::UTTR_BANK_ID;
  		$col_rec =  User_Profile_Attributes::UTTR_RECRUITER_ID;
		$table = $obj_entity->get_table_name();
		$this->db->from("{$table}");
        $this->db->select("*");
        $this->db->where("{$table}.{$col}",$user_id );
        $this->db->where("{$table}.{$col_bank} ",$bank_id );
        $this->db->where("{$table}.{$col_rec} ",$rec_id );
       	$query = $this->db->get();
        return $query->result();
  	}
  	
  	protected function delete_user($user_id,$login_user)
  	{
  		$user = new User();
  		$table = $user->get_user_table_name();
		$this->db->from("{$table}");
  		$this->db->where("user_id", $user_id);
  		$this->db->set("user_is_deleted",1);
  		$this->db->set("user_modified_by",$login_user);
  		$this->db->set("user_modified_date",'now()',false);
		$this->db->update("{$table}"); 
		delete_user_departments($user_id,$login_user);
  	}
	protected function delete_user_departments($user_id,$login_user)
	{
			$obj_udept = new User_Departments();
			$table = $obj_udept->get_table_name();
			$id_name = $obj_udept->get_table_id_name();
			//delete records
    		$this->db->where("{$id_name}",$user_id );
			$this->db->delete("{$table}");
	}
}
