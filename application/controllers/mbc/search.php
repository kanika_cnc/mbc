<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Model : mbc_user_bl.php
 * Class accesbile to valid logged in mbc users 
 * for User Management
 * @author CNC
 *    
 */ 
class Search extends MBC_Controller {

	private $page ='';
	public $content;
	public function __construct () 
    {
    	parent::__construct();
    	
    }
	public function index()
	{
		//$this->coming_soon('3-Dec-2013');
		//$this->mbc_user();
		$this->load_models();
		$this->set_data();
		$this->page['page_name'] = "retrievals";
		$this->load_view();
	}
	private function load_view()
    {
    	$this->masterpage->setMasterPage('mbc/master');
		$this->masterpage->setPageTitle('Candidate Upload:');
		$this->masterpage->addContentPage('mbc/search', 'content',$this->page);
        $this->masterpage->show();
    }
	private function load_models()
	{
		$this->load->model('bl/candidate/candidate_bl','obj_cand_manager');
		$this->load->model('bl/core/list_bl','objMaster');
		$this->load->model('bl/core/departments_bl','objDepartments');
		$this->load->model('bl/core/banks_bl','objBanks');
	}
	private function set_data()
	{
		
		$this->page["data"]["banks"] = "";
		$this->page["data"]["designations"] = "";
		$this->page["data"]["departments"] = "";
		$this->page["data"]["candidates"] = array();
		$this->page["data"]["can_total_experience"] = "";
		$this->page["data"]["can_ctc"] = "";
		$this->page["data"]["can_location_name"] = "";
		$this->page["data"]["str_user_departments"] = "";
		if($this->input->post('search'))
		{
			
			$this->page["data"]["banks"] = $this->input->post('banks');
			$this->page["data"]["designations"] = $this->input->post('designations');
			$this->page["data"]["departments"] = $this->input->post('departments');
			$this->page["data"]["candidates"] = $this->obj_cand_manager->search_candidate($this->input->post('departments'),$this->input->post('banks'),$this->input->post('designations'));
		}
		
		$this->load_masters();
		
	}
	private function load_masters()
	{
		$this->load->model('bl/core/list_bl','objMaster');
		$this->load->model('bl/core/departments_bl','objDepartments');
		$this->load->model('bl/core/banks_bl','objBanks');
		
		$this->page["masters"]["bank"] = $this->objMaster->get_banks_drop_down_list('',true);
		$this->page["masters"]["departments"] = $this->objDepartments->get_departments(); 
		$this->page["masters"]["hbanks"] = $this->objBanks->get_banks_hierarchical_list();
		$this->page["masters"]["exp_years"] = $this->objMaster->get_numbers_dropdown(1,30,array(array('0','Years',0),array('31','31+',9999)));
		$this->page["masters"]["exp_months"] =  $this->objMaster->get_numbers_dropdown(1,12,array(array('0','Months',0)));
		$this->page["masters"]["days"] = $this->objMaster->get_numbers_dropdown(1,31,array(array("",'Day',0)));
		$this->page["masters"]["dob_years"] = $this->objMaster->get_numbers_dropdown(1960,date("Y"),array(array("","Year",0)));
		$this->page["masters"]["dob_months"] =  $this->objMaster->get_numbers_dropdown(1,12,array(array('','Months',0)));
		$this->page["masters"]["ctc_lakhs"] =  $this->objMaster->get_numbers_dropdown(0,50,array(array('0','Lacs',0),array('51','51+',9999)));
		$this->page["masters"]["ctc_thousands"] = $this->objMaster->get_numbers_dropdown(0,99,array(array('','Thousand',0)),5);
		$this->page["masters"]["designations"] = $this->objMaster->get_designation('',true);
		$this->page["masters"]["age"] = $this->objMaster->get_numbers_dropdown(15,60,array(array('0','Years',0),array('61','61+',9999)));
		
	}
	
}


