<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 * Class accesbile to valid logged in mbc users 
 * for  dashboard
 * Model: Entity models of following objects
 * 		  Qualification/Departments/Banks/CTC/designations/Location/MembersBanks
 * Lib : Carbo
 * @author CNC
 * @todo : Filters 
 * @todo: update the inactive flag if parent is set to inactive
 * 
 *
 */
class Settings extends MBC_Controller {

	public function __construct () 
    {
    	parent::__construct();
    	if(!$this->is_admin())
    	{
    		$this->acl->process_redirection($logged_in);
    	}
    }
   //page level access if user can access this page or not
	public function index()
	{
		$this->departments('none');
		
	}
	
	/**
	 * 
	 * Enter description here ...
	 * @param unknown_type $params
	 */
	public function qualifications($params='')
	{
		$this->load->model('dl/entity/qualifications','entity_qualifications');
		$fields = $this->entity_qualifications->get_fields();
		//prepare params for gird
		
		$params = array(
            'id' => 'qualification',
            'table' => 'qualification_master',
            'table_id_name'=>'qm_id',
            'url' => 'mbc/settings/qualifications',
            'uri_param' => $params,
            'columns' => $fields,
            'columns_visible' => array(0,1,2),
            'commands' => $this->set_grid_command_view(),
            'ajax' => False,
        	'logged_in_user_id' => $this->get_loggedin_user_id()
        );
		$content["page_name"]= "settings";
		$content["tab_name"]= "qualifications";
      	$content["tab_data"]=  $this->render_grid($params,$fields);
      	$content["tab_action"]= $this->carbogrid->command;
		$this->refresh_view($content);
	}

	public function departments($params='')
	{
		$this->load->model('dl/entity/departments','entity_departments');
		$fields = $this->entity_departments->get_fields();
		//prepare params for gird
		
		$params = array(
            'id' => 'departments',
            'table' => 'department_master',
            'table_id_name'=>'dm_id',
            'url' => 'mbc/settings/departments',
            'uri_param' => $params,
            'columns' => $fields,
            'columns_visible' => array(0,2),
            'commands' => $this->set_grid_command_view(),
            'ajax' => False,
			'page_size' => 20,
        	'logged_in_user_id' => $this->get_loggedin_user_id()
        );
		$content["page_name"]= "settings";
		$content["tab_name"]= "departments";
      	$content["tab_data"]=  $this->render_grid($params,$fields);
      	$content["tab_action"]= $this->carbogrid->command;
		$this->refresh_view($content);
	}
	public function banks($params='')
	{
		$this->load->model('dl/entity/banks','entity_banks');
		$fields = $this->entity_banks->get_fields();
		//prepare params for gird
		
		$params = array(
            'id' => 'banks',
            'table' => 'bank_master',
            'table_id_name'=>'bm_id',
            'url' => 'mbc/settings/banks',
            'uri_param' => $params,
            'columns' => $fields,
            'columns_visible' => array(0,1,2,5,6,7),
            'commands' => $this->set_grid_command_view(),
            'ajax' => False,
        	'logged_in_user_id' => $this->get_loggedin_user_id()
        );
		$content["page_name"]= "settings";
		$content["tab_name"]= "banks";
      	$content["tab_data"]=  $this->render_grid($params,$fields);
      	$content["tab_action"]= $this->carbogrid->command;
		$this->refresh_view($content);
	}
	
	public function ctc($params='')
	{
		$this->load->model('dl/entity/departments','entity_departments');
		$fields = $this->entity_departments->get_fields();
		//prepare params for gird
		
		$params = array(
            'id' => 'departments',
            'table' => 'department_master',
            'table_id_name'=>'dm_id',
            'url' => 'mbc/settings/departments',
            'uri_param' => $params,
            'columns' => $fields,
            'columns_visible' => array(0,1,2,3,5,7),
            'commands' => $this->set_grid_command_view(),
            'ajax' => False,
        	'logged_in_user_id' => $this->get_loggedin_user_id()
        );
		$content["page_name"]= "settings";
		$content["tab_name"]= "departments";
      	$content["tab_data"]=  $this->render_grid($params,$fields);
      	$content["tab_action"]= $this->carbogrid->command;
		$this->refresh_view($content);
	}
	
	/**
	 * 
	 * Enter description here ...
	 * @param unknown_type $params
	 * @todo: Make the changes to associte designations to banks
	 */
	public function designations($params='')
	{
		$this->load->model('dl/entity/designations','entity_designations');
		$fields = $this->entity_designations->get_fields();
		//prepare params for gird
		
		$params = array(
            'id' => 'designations',
            'table' => 'designation_master',
            'table_id_name'=>'dm_id',
            'url' => 'mbc/settings/designations',
            'uri_param' => $params,
            'columns' => $fields,
            'columns_visible' => array(0,1,2),
            'commands' => $this->set_grid_command_view(),
            'ajax' => False,
        	'logged_in_user_id' => $this->get_loggedin_user_id()
        );
		$content["page_name"]= "settings";
		$content["tab_name"]= "designations";
      	$content["tab_data"]=  $this->render_grid($params,$fields);
      	$content["tab_action"]= $this->carbogrid->command;
		$this->refresh_view($content);
	}
	public function locations($params='')
	{
		$this->load->model('dl/entity/locations','entity_locations');
		$fields = $this->entity_locations->get_fields();
		//prepare params for gird
		
		$params = array(
            'id' => 'locations',
            'table' => 'location_master',
            'table_id_name'=>'lm_id',
            'url' => 'mbc/settings/locations',
            'uri_param' => $params,
            'columns' => $fields,
            'columns_visible' => array(0,1,2,3),
            'commands' => $this->set_grid_command_view(),
            'ajax' => False,
        	'logged_in_user_id' => $this->get_loggedin_user_id()
        );
		$content["page_name"]= "settings";
		$content["tab_name"]= "locations";
      	$content["tab_data"]=  $this->render_grid($params,$fields);
      	$content["tab_action"]= $this->carbogrid->command;
		$this->refresh_view($content);
	}
	/**
	 * 
	 * Enter description here ...
	 * @param unknown_type $params
	 * @todo : File not getting upload
	 * @todo : unique valuidation nolt working
	 */
	public function member_banks($params='')
	{
		$this->load->model('dl/entity/member_banks','entity');
		$fields = $this->entity->get_fields();
		//prepare params for gird
		
		$params = array(
            'id' => 'member_banks',
            'table' => 'member_bank',
            'table_id_name'=>'mb_id',
            'url' => 'mbc/settings/member_banks',
            'uri_param' => $params,
            'columns' => $fields,
            'columns_visible' => array(0,1,2,3),
            'commands' => $this->set_grid_command_view(),
            'ajax' => FALSE,
			'show_dialog' =>false,
        	'logged_in_user_id' => $this->get_loggedin_user_id()
        );
		$content["page_name"]= "settings";
		$content["tab_name"]= "member_banks";
      	$content["tab_data"]=  $this->render_grid($params,$fields);
      	$content["tab_action"]= $this->carbogrid->command;
      	//$post_back = $this->input->post();
      	//echo $post_back;
      	//print_r($this->input->post());
      	//echo "no". $form_no = $this->input->post("cg_member_banks_dialog_no");
      	//echo "yes". $form_yes = $this->input->post("cg_member_banks_dialog_yes");
      	//echo "add". $form_add= $this->input->post("cg_member_banks_command_add");
      	//echo "edit". $form_edit= $this->input->post("cg_member_banks_command_edit");
      	//if($this->carbogrid->command =="")
      	//{
      		//$content["tab_action"] = "";
      	//}
      	//else if($form_no != "" || $form_yes !="")
      	//{
      		//$content["tab_action"] = "";
      	//}
      	//else 
      	//{
      		//$content["tab_action"] = $form_add == ""? ($form_edit =="" ? "": "edit") : "add";
      	//}
      	//echo $this->carbogrid->command;
      	//print_r($this->input->post("cg_member_banks_dialog_no"));
      	//$content["tab_action"] = !empty($post_back) ? "add" :""; 
		$this->refresh_view($content);
		
	}	
	private function set_grid_command_view()
	{
		//Don't show multiple delete button
        $commands['delete']['toolbar'] = FALSE;
        $commands['delete']['grid'] = FALSE;
        return $commands; 
	}
	/**
	 * 
	 * Enter description here ...
	 * @param $params
	 * @param $fields
	 */
	private function render_grid($params)
	{
		$loaded = false;
        $this->load->library('carbogrid', $params);
        if ($this->carbogrid->is_ajax)
        {
            $this->carbogrid->render();
            return FALSE;
        }
		return $this->carbogrid->render();
	}
		
	private function refresh_view($content)
	{
		$this->masterpage->setMasterPage ('mbc/master');
		$this->masterpage->setPageTitle('Settings:' . $content["tab_name"]);
		$this->masterpage->addContentPage ('mbc/settings', 'content',$content);
        $this->masterpage->show();
	}
	
}

