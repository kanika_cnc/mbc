<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Home page controller for non-logged in user accessing the mbc portal
 * @author Fintotal
 * @todo : implement javascript funtion
 */
class Home extends CI_Controller {

	/**
	 * 
	 * function to load the login view
	 */
	public function index()
	{
		$this->load->library('form_validation');
		$data['javascript'] =$this->form_validation->javascript('mbc_user_login');
		$this->load->view( "forms/login",$data );
	}
}

