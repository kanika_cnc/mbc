<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Model : mbc_user_bl.php
 * Class accesbile to valid logged in mbc users 
 * for User Management
 * @author CNC
 *   
 */ 
class User_Management extends MBC_Controller {

	public $content;
	public function __construct () 
    {
    	parent::__construct();
    	
    }
	public function index()
	{
		//$this->coming_soon('3-Dec-2013');
		$this->mbc_user();
	}
	public function coming_soon($rel_date='',$rel_build='current')
	{
		$this->masterpage->setMasterPage ('mbc/master');
		$this->masterpage->setPageTitle('Coming Soon ');
		$content["rel_date"] = $rel_date;
		$content["rel_build"] = $rel_build;
        $this->masterpage->addContentPage ('mbc/coming_soon', 'content',$content);
        $this->masterpage->show();
	}
	public function mbc_user($uri='',$errors ='',$form_data=array())
	{
		if(!$this->is_admin())
    	{
    		$this->edit();
    		return;
    	}
		
		$this->load->model('bl/mbc/mbc_user_bl','obj_user_manager');
		$this->content["tab_data"]["users"] = $this->obj_user_manager->fetch(UserType::MBC_USER,'',array($this->get_loggedin_user_id()),'',0);
		$i=0;
		foreach($this->content["tab_data"]["users"] as $u)
		{
			if($u->user_is_deleted == 1)
			{
				//debug($u);
				$this->content["tab_data"]["deleted_users"][$i] = $u;
			}
			$i++;
		}
		//render edit form
		$this->render_edit_view($uri,$errors,$form_data);
		//render mbc user list
		
				
	}
	private function render_edit_view($uri='',$errors ='',$form_data=array())
	{
		//$this->load->model('bl/mbc/mbc_user_bl','obj_user_manager');
		$this->content["tab_data"]["params"] = array(
            'id' => 'edit_mbc_users',
            'url' => 'mbc/user_management/edit_mbc_user',
            'uri_param' => $uri,
        	'logged_in_user_id' => $this->get_loggedin_user_id()
        	);
		$this->populate_masters($form_data);
		if(empty($form_data))
		{
        	$this->content["tab_data"]["form_data"] = new Mbc_user();
		}
		else
		{
			$this->content["tab_data"]["form_data"] = $form_data;
		}	
		$this->content['page_name'] = "user_management";
		$this->content["tab_name"]= "mbc_user";
		$this->content["tab_data"]["errors"] =$errors;
		$this->masterpage->setMasterPage ('mbc/master');
		$this->masterpage->setPageTitle('User Management');
        $this->masterpage->addContentPage ('mbc/user_management', 'content',$this->content );
        $this->masterpage->show();
		
	}
	public function valid_password($user_id="",$password="")
	{
		$this->load->model('bl/mbc/mbc_user_bl','obj_user_manager');
		if(empty($password))
		$password = $this->input->post("old_password");
		if(empty($user_id))
		$user_id = $this->get_loggedin_user_id();
		$logged_in=$this->session->userdata('logged_in');
		$user_id  = $logged_in['user_id'];
		$this->load->library('auth_acl');
		$password = $this->auth_acl->encrypt_password($password);
		$user = $this->obj_user_manager->get($user_id,1);
		if($user->user_password != $password)
		{
			$this->form_validation->set_message('valid_password', 'Old password incorrect');
		    return false;
		}
		else
		return true;
		
	}
	public function view_user_details ($user_id){
		$user_departments = array();
		$user = array();
		$this->load->model('bl/mbc/mbc_user_bl','obj_user_manager');
		$user = $this->obj_user_manager->get_user_by_user_id($user_id);
		$user_departments = $this->obj_user_manager->get_user_departments($user_id);
		$user->user_departments = $user_departments;
		$data['user'] = $user;
		$this->load->view('core/view_user_details',$data);
	}
	public function change_password()
	{
		$error = "";
		$loggedin_user = $this->get_loggedin_user();
		$content["tab_name"]= "change_pass";
		$this->load->library('form_validation');
		$data['javascript'] =$this->form_validation->javascript('mbc_user_change_password');
		$is_valid = $this->form_validation->run('mbc_user_change_password');
		$content["tab_data"]["error"]= $error;
		if(!$is_valid)
		{
			$content["tab_data"]["error"]="";
		}
		else
		{
			$old_pass = $this->input->post('old_password');
			$new_pass = $this->input->post('new_password');
			$new_pass_r = $this->input->post('new_password_r');
			$this->load->library('Auth_acl');
			$new_pass = $this->auth_acl->encrypt_password($new_pass);
			$this->load->model('bl/core/acl_bl','obj_user_acl');
			if(!$this->obj_user_acl->change_user_password($new_pass,$this->get_loggedin_user_id()))
			$error = "Some Problem in saving password. Please try again";
			
			else
			{
				
				$error = "Password changed succesfully";
				$this->load->library("Emailtemplate");
				$this->emailtemplate->sendEmail('change_password','info@mybankconnect.com',array($loggedin_user['user_email']),array(""),array($new_pass_r));
			}
			$content["tab_data"]["error"]=$error;
		}
		$content['page_name'] = "user_management";
		$this->masterpage->setMasterPage ('mbc/master');
		$this->masterpage->setPageTitle('Change Password ');
        $this->masterpage->addContentPage ('mbc/user_management', 'content',$content );
        $this->masterpage->show();
	}

	public function bank_user()
	{
		$content["tab_name"]= "bank_user";
		$content["tab_data"]= "";
		$content['page_name'] = "user_management";
		$this->masterpage->setMasterPage ('mbc/master');
		$this->masterpage->setPageTitle('User Management ');
        $this->masterpage->addContentPage ('mbc/user_management', 'content',$content );
        $this->masterpage->show();
		
	}
	public function edit_mbc_user()
	{
		//get action
		$action = $this->input->post('submit');
		$is_new_user = false;
		
   		//load validation
		$this->load->library('form_validation');
		$is_valid = false;

		//get model
   		$this->load->model('bl/mbc/mbc_user_bl','obj_user_manager');
   		$user = new Mbc_user();
   		$user = $this->convert_post_to_mbc_object();
   		if(!is_null($user))
		{
			$user->user_id == "" ?null : $user->user_id;
			$is_new_user = ($user->user_id == "" || is_null($user->user_id)) ? true : false;
		} 
   		switch($action)
    	{
	    	case 'Save':
	    		if($this->is_admin())
	    		{
	    			$is_valid = $this->form_validation->run('admin_mbc_user_save');
	    		}
	    		else
	    		{
	    			$is_valid = $this->form_validation->run('user_save');
	    			
	    		}
		    	if (!$is_valid)
		    	{
					$this->mbc_user('','',$user);
		    	}
		    	else
		    	{	
		    		//Save user in database;
		    		if($is_new_user)
		        	{	$user->user_password = $this->auth_acl->get_random_password(); 
		        	}	
		    		
		        	try 
		    		{
		    			if($this->is_admin())
		    			{
		    				
		    				$this->obj_user_manager->save($user,$this->get_loggedin_user_id());
		    				if($is_new_user)
		        			{	
		        				$this->load->library("Emailtemplate");
								$this->emailtemplate->sendEmail(Emailtemplate::CREATEPASSWORD,'info@mybankconnect.com',array($user->user_email),array(""),array($this->auth_acl->decrypt_password($user->user_password)));
		        			}
							redirect(base_url()."mbc/user_management/");
		    			}
		    			else
		    			{
		    				$this->obj_user_manager->save($user,$this->get_loggedin_user_id(),"user");
		    				redirect(base_url()."mbc/user_management/edit");
		    			}
		    			
		    		}
		    		catch(Exception $ex)
		    		{
		    			$this->mbc_user('',$ex->getMessage(),$user);
		    		}
		    	}	
	    		break;
	    	case 'Edit':
	    		$edit_user_id = $this->input->post('user_id');
	    		$user = $this->obj_user_manager->get($edit_user_id,-1);
	    		$user = $this->convert_object_to_postdata($user);
	    		$this->mbc_user('','',$user);
	    		break;
	    	case 'Delete':
	    		$edit_user_id = $this->input->post('user_id');
	    		$user = $this->obj_user_manager->delete($edit_user_id,$this->get_loggedin_user_id(),0);
	    		$this->mbc_user('','User Deleted Sucessfully',$user);
	    		break;
			case 'Restore':
				$edit_user_id = $this->input->post('user_id');
	    		$user = $this->obj_user_manager->delete($edit_user_id,$this->get_loggedin_user_id(),1);
	    		$this->mbc_user('','User Restored Sucessfully',$user);
	    		break;
	    	default:		
	    		$this->mbc_user('','',$user);
    	}
	}
	public function validate_cm()
	{
		$user_is_cm = $this->input->post('user_is_cm');
		$user = new Mbc_user();
		$user = $this->convert_post_to_mbc_object();
	
		$invalid = false;
		if($user_is_cm)
		{
			
			foreach($user->user_profile_attributes as $attr)
			{
				if($attr->uttr_bank_id == "" || $attr->uttr_recruiter_id == "")
				{
					$invalid = true;
					$this->form_validation->set_message('validate_cm', 'Please select the Member bank and recruiter for which user is CM');
				}
			}
			
			if($invalid == true)
			{
				return false;
			}
			return true;
		}
		return true;
	}
	private function populate_masters($user=array())
	{
		$cm_user_id ='';
        if(!empty($user))
        {
        	if($user->is_cm == 1)
        	{
        		$cm_user_id = $user->user_id;
        	}
        }
		$this->load->model('bl/core/departments_bl','objDepartments');
		$data_departments = $this->objDepartments->get_departments(TRUE);
		//print_r($data_departments);	
        $this->content["tab_data"]["masters"]["departments"] = ($data_departments);

        $this->load->model('bl/core/list_bl','objmaster');
        
		$data_member_banks = $this->objmaster->get_available_member_banks($cm_user_id,'dropdown',array(""=>"Select Bank"));
		$this->content["tab_data"]["masters"]["member_banks"] =$data_member_banks;
		$this->content["tab_data"]["masters"]["recruiters"] = $this->obj_user_manager->fetch(UserType::MBC_USER,'dropdown',array($this->get_loggedin_user_id()),array(""=>"Select Recruiter"));
		$this->content["tab_data"]["loggin_user"] = $this->get_loggedin_user();
	}
	private function convert_post_to_mbc_object()
	{
		$user = new Mbc_user();
		
   		if($this->input->post())
   		{

	   			$user =  objectToObject(array_to_object($this->input->post()),"Mbc_user");
	   			
	   			$user_is_cm = $this->input->post('user_is_cm');
				
				if(!$this->is_admin())
				{
					$user->is_cm = $this->is_cm();
				}
			    if($this->is_admin())
			    {
	   				$temp = (array)$user->user_profile_attributes;
					if(!empty($temp))
			    	{
			    		$user->user_profile_attributes =  $this->obj_user_manager->convert_attribute_to_object($user->user_profile_attributes,$user->user_id,$user->user_type_id);
					}
			    		if($user_is_cm)
			    		{	$user->is_cm = true;  		}
			    		else {
			    		$user->is_cm = false;  		}
			    }
		}
		return $user;
	}
	private function convert_object_to_postdata($user)
	{
		$strdep = "";
		foreach ($user->user_departments as $dep) {
			$strdep .= $strdep ==""? $dep->ud_department_id : "," . $dep->ud_department_id;
		}
		$user->user_departments =  $strdep;
		return $user;
	}
	public function edit()
	{
		//get model
   		$this->load->model('bl/mbc/mbc_user_bl','obj_user_manager');
   		$user = new Mbc_user();
   		$edit_user_id = $this->get_loggedin_user_id();
	    $user = $this->obj_user_manager->get($edit_user_id,-1);
	    $user = $this->convert_object_to_postdata($user);
	    //$this->mbc_user('','',$user);
	    $this->render_edit_view('','',$user);
	}
}


