<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 * Controller takes care of all access rights for the login user
 * Model : core/Acl_bl
 * Lib : Acl
 * @author fintotal
 * @todo : Remember me
 * @todo : Forgot password
 * @todo : Activation link email 
 * @todo : many failed login attempts
 * @todo : login activity update  
 */ 
class Acl extends CI_Controller {
	
 	public function __construct () 
    {
     	 parent::__construct();
    }
    
	public function login()
	{
		$this->load->library('form_validation');
		$data['javascript'] =$this->form_validation->javascript('mbc_user_login');
		$is_valid = $this->form_validation->run('mbc_user_login');
		if(!$is_valid)
		{
			$this->load->view( "forms/login",$data );
		}
		else
		{
			$this->mbc_auth->process_redirection();
			
		}
	} 
	
	public function logout()
	{
		$this->session->sess_destroy();
		redirect('/mbc/', 'refresh');
	}
	
	public function validate_user($email,$password)
	{
		$email = $this->input->post('email');
		$password = $this->input->post('password');
		$password = $this->auth_acl->encrypt_password($password);
		$this->load->model('bl/core/acl_bl','acl_bl_model');
		$result_user = $this->acl_bl_model->validateUser($email,$password);
		if($result_user)
		{
			$data = array(
                    'user_id' => $result_user->user_id,
                    'first_name' => $result_user->user_first_name,
                    'last_name' => $result_user->user_last_name,
                    'user_email' => $result_user->user_email,
                    'user_validated' => true,
            		'user_activated' =>$result_user->user_is_active,
            		'user_type_id' => $result_user->user_type_id,
                    );
            if($data["user_activated"] == 0)
            {
            	$this->form_validation->set_message('validate_user', 'Your account has not been activated yet.Please check you email box for activation link or click to generate new activation link');
		     	return false;	
            }
			$this->acl_bl_model->update_last_logged_in_date($result_user->user_id);        
            $this->session->set_userdata('logged_in',$data);
            $this->auth_acl->process_redirection($this->session->userdata('logged_in'));
            return true;
            
		}
		else {
				$this->form_validation->set_message('validate_user', 'Invalid username or password');
		     	return false;
			
		}
	}
	
	/**
	 * 
	 * Enter description here ...
	 * @todo right to logic to sent email and reset password
	 */
	public function forgot_password($email="",$password="")
	{
		$data["email"] ="";
		$data["email_sent"] =0;
		$this->load->library('form_validation');
		$data['javascript'] =$this->form_validation->javascript('mbc_user_forgot_password');
		$is_valid = $this->form_validation->run('mbc_user_forgot_password');
		if(!$is_valid)
		{
			$this->load->view( "forms/forgot_password",$data );
		}
		else
		{
			$email = $this->input->post("email");
			$password = $email["password"];
			$this->load->library('Auth_acl');
			$password = $this->auth_acl->decrypt_password($email["password"]);
			$data["email"] = $email["email"];
			$this->load->library("Emailtemplate");
			$this->emailtemplate->sendEmail('forgot_password','info@mybankconnect.com',array($email["email"]),array(""),array($password));
			$data["email_sent"] =1;
			$this->load->view( "forms/forgot_password",$data );	
		}
	} 
	
	public function activate_user()
	{
		throw Exception ("Need implemenation",9980);
	}
	
	public function generate_activation_link()
	{
		throw Exception ("Need implemenation",9981);
	} 
	public function valid_email($email)
	{
		
		$email = $this->input->post('email');
		$this->load->model('bl/core/acl_bl','acl_bl_model');
		if(!$result_user = $this->acl_bl_model->get_user_by_email($email))
		{
				$this->form_validation->set_message('valid_email', 'This email does not belong to any of the accounts.');
		     	return false;
		}
		else
		{
			$password = $result_user->user_password;
			$data["email"] = $email;
			$data["password"] = $password;
			return $data;
			
		}
	} 
}

