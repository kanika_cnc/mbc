<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends MBC_Controller {

	public function index()
	{
		$this->load->library('form_validation');
		$data['javascript'] =$this->form_validation->javascript('mbc_user_login');
		$this->load->view( "forms/login",$data );
	}
}

