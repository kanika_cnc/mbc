<?php
$config = array(
				 'mbc_user_login' => array(
                                    	array(
                                            'field' => 'email',
                                            'label' => 'Email',
                                            'rules' => 'trim|required|xss_clean|valid_email'
                                         ),
										array(
                                            'field' => 'password',
                                            'label' => 'Password',
                                            'rules' => 'trim|required|xss_clean|callback_validate_user'
                                         )
                                    ),
                 'mbc_user_forgot_password' =>  array(
                                    	array(
                                            'field' => 'email',
                                            'label' => 'Email',
                                            'rules' => 'trim|required|xss_clean|valid_email|callback_valid_email'
                                         )),
				   'mbc_user_change_password' =>  array(
                                    	array(
                                            'field' => 'old_password',
                                            'label' => 'Old Password',
                                            'rules' => 'trim|required|xss_clean|callback_valid_password'
                                         ),
										 array(
                                            'field' => 'new_password',
                                            'label' => 'New Password',
                                            'rules' => 'trim|required|xss_clean|matches[new_password_r]'
                                         ),
										 array(
                                            'field' => 'new_password_r',
                                            'label' => 'New Password Again',
                                            'rules' => 'trim|required|xss_clean'
                                         )
								),                   
                 'mbc_candidate_upload_save' => array(
                                    array(
                                            'field' => 'user_first_name',
                                            'label' => 'First Name',
                                            'rules' => 'required'
                                         ),
                                    array(
                                            'field' => 'user_email',
                                            'label' => 'Email',
                                            'rules' => 'required|valid_email'
                                         ),
										  array(
                                            'field' => 'user_contact_mobile',
                                            'label' => 'Contact Mobile',
                                            'rules' => 'required|exact_length[10]|numeric'
                                         ),
                                         array(
                                            'field' => 'dob_day',
                                            'label' => 'Date of birth',
                                            'rules' => 'callback_invalid_date'
                                         ), 
                                         
                                    ),
                 'admin_candidate_upload_verify_save' => array(
                                    array(
                                            'field' => 'user_email',
                                            'label' => 'Email',
                                            'rules' => 'required|valid_email'
                                         ),
                                    array(
                                            'field' => 'user_first_name',
                                            'label' => 'First Name',
                                            'rules' => 'required'
                                         ),
                                    array(
                                            'field' => 'user_contact_mobile',
                                            'label' => 'Contact Mobile',
                                            'rules' => 'required|exact_length[10]|numeric'
                                         ),
                                    array(
                                            'field' => 'ddl_exp_years',
                                            'label' => 'Total Years Of experince',
                                            'rules' => 'callback_required_total_experience'
                                         ),     
                                    array(
                                            'field' => 'can_curr_experince',
                                            'label' => 'Current Bank',
                                            'rules' => 'required'
                                         ),
                                    array(
                                            'field' => 'user_departments',
                                            'label' => 'Current Departments',
                                            'rules' => 'required'
                                         ),          
                                    array(
                                            'field' => 'can_age',
                                            'label' => 'Age',
                                            'rules' => 'callback_required_age'
                                         ),
                                      array(
                                            'field' => 'dob_day',
                                            'label' => 'Date of birth',
                                            'rules' => 'callback_invalid_date'
                                         ),     
									/*array(
                                            'field' => 'can_ctc_lks',
                                            'label' => 'Current CTC',
                                            'rules' => 'callback_required_current_ctc'
                                         ),*/
									array(
                                            'field' => 'can_designation_id',
                                            'label' => 'Current Designation',
                                            'rules' => 'required'
                                         ),
									array(
                                            'field' => 'txtCurrLocation',
                                            'label' => 'Current Location',
                                            'rules' => 'required'
                                         ),
									array(
                                            'field' => 'f1Resume_h',
                                            'label' => 'Resume',
                                            'rules' => 'required'
                                         )
										 ),
				'admin_mbc_user_save' => array(
                                    array(
                                            'field' => 'user_email',
                                            'label' => 'Email',
                                            'rules' => 'required|valid_email'
                                         ),
                                    array(
                                            'field' => 'user_first_name',
                                            'label' => 'First Name',
                                            'rules' => 'required'
                                         ),
                                    array(
                                            'field' => 'user_contact_mobile',
                                            'label' => 'Contact Mobile',
                                            'rules' => 'required'
                                         ),
                                    array(
                                            'field' => 'user_departments',
                                            'label' => 'Departments',
                                            'rules' => 'required'
                                         ),     
                                    array(
                                    		'field' => 'user_is_cm',
                                    		'label' => 'CM',
                                            'rules' => 'callback_validate_cm'
                                    )     
                                    ),
						'user_save' => array(
                                    array(
                                            'field' => 'user_email',
                                            'label' => 'Email',
                                            'rules' => 'required|valid_email'
                                         ),
                                    array(
                                            'field' => 'user_first_name',
                                            'label' => 'First Name',
                                            'rules' => 'required'
                                         ),
                                    array(
                                            'field' => 'user_contact_mobile',
                                            'label' => 'Contact Mobile',
                                            'rules' => 'required'
                                         )),
                                                                   
               );