<?php
/**
 * 
 * Enter description here ...
 * @param $name
 * @param $options
 * @param $selected
 * @param $call_back
 * @param $parent_selectable
 * @param $mode
 * @param $item_id : this is a patch to handle tree in corbo grid
 */
function form_hcheckboxTree($name,$options,$selected="",$call_back="",$parent_selectable=false,$mode=3,$item_id='')
{
	$selected_ =$selected;
	$data_formatted = array();
	
	if($item_id != '')
	{
		$selected_ = $selected_."," .$item_id; 
			
	}
	if($selected_ !="" && !empty($selected_))
	{
		$selected_ = explode(',', $selected_);
	}
	else
	{
		$selected_ = array();
	}

	foreach($options as $index => $row)
    {
    	  $reformat_row = array();
    	  $row = (array)$row;	
          $reformat_row["title"] = $row["title"];
          $reformat_row["key"] = $row["key"];  
          $reformat_row["parent_key"] = $row["parent_key"];
          if(isset($row["unselectable"]))
          	$reformat_row["unselectable"] = $row["unselectable"]== 1? true:false; 
          if(isset($row["hideCheckbox"]))
          {
          	$reformat_row["hideCheckbox"] = $row["hideCheckbox"]== 1? true:false;
	          //If item is in selected list then make it editable
	          //Done for user management
	          if(in_array($row["key"], $selected_))
	          {
	          	$reformat_row["unselectable"] = false;
	          	$reformat_row["hideCheckbox"] = false;
	          }
          }
          
          
          
          $reformat_row["select"] = in_array($row["key"], $selected_)? true :false;
          //$reformat_row["expand"] = in_array($row["key"], $selected_)? true :false;
		  if(isset($row["expand"]))
          $reformat_row["expand"] = $row["expand"]== 1?true:false;
         else
          $reformat_row["expand"] = in_array($row["key"], $selected_)? true :false;
          $reformat_row["icon"] = false;
          $data_formatted[] = $reformat_row;
		  
    }

    $tree = buildTree($data_formatted,0,'parent_key','key',$parent_selectable,$mode);
	//debug($tree);
    $ctrlname = $name;
	$name =  "tr_".$name;
	
	echo '<script type="text/javascript" src="'.asset_url().'scripts/jquery.dynatree.js" ></script>
	 <link href="'.asset_url().'styles/ui.dynatree.css" type="text/css" rel="stylesheet" />';
	echo '<script>';
	echo '$(function(){
         $("#'.$name.'").dynatree({
         ';
         if($mode == 1)
         {
         echo 'classNames: {checkbox: "dynatree-radio"},';
         }
         else
         {
         	echo 'checkbox: true,';
         }
         
	echo '
		 icon: false,
		 selectMode: '. $mode .'.,
    	 children: '. json_encode($tree) .',
    	 /*onPostInit: function(tree) {
    	 	alert(tree.getSelectedNodes());
    	 },*/
    	 onSelect: function(select, node) {
		 
		 
    	var path = node.getKeyPath().split("/");
        var pathString ="";
        for(var i=1;i<path.length;i++)
        {
            var node1 = node.tree.getNodeByKey(path[i]);
        	pathString = pathString + "/" + node1.data.title;
        }
        ';
        
        if($call_back!="")
        {
        	switch($call_back)
        	{
        		case "updatepath":
        			echo "updatepath(pathString,node.getKeyPath())";	
        			break; 
				case "getdepid":
        			echo "getdepid()";	
        			break; 
        		default:
        			echo "addRow('tbCurrDepartmentDetails',pathString,node.data.key,'".$ctrlname ."',select);";	
        	}
        	
        }
        
        // Get a list of all selected nodes, and convert to a key array:
        echo " 
        	var selKeys = $.map(node.tree.getSelectedNodes(), function(node){
          return node.data.key;
        });
        	
        	var partsel = new Array();
	        $('#".$name." .dynatree-partsel:not(.dynatree-selected)').each(function () {
	            var node = $.ui.dynatree.getNode(this);
	            partsel.push(node.data.key);
	        });
			
	        selKeys = selKeys.concat(partsel);
	       
        	var selRootNodes = node.tree.getSelectedNodes(true);
        	
        	var selRootKeys = $.map(selRootNodes, function(node){
	          return node.data.key;
	        });
	      	     ";
        if($parent_selectable)
        {
			
        	echo '$("[name='.$ctrlname.']").val(selKeys.join(","));';
        	//echo '$("[name='.$ctrlname.']").val(selRootKeys);';
        }
        else
        {
			
        	echo '$("[name='.$ctrlname.']").val(selKeys.join(","));';
        }
        // Get a list of all selected TOP nodes
        //var selRootNodes = node.tree.getSelectedNodes(false);
        // ... and convert to a key array:
        //var selRootKeys = $.map(selRootNodes, function(node){
          //  return node.data.key;
        //});
        //$("#echoSelectionRootKeys3").text(selRootKeys.join(", "));
        //$("#echoSelectionRoots3").text(selRootNodes.join(", "));
        
      echo "},
      onDblClick: function(node, event) {
        node.toggleSelect();
      },
      onKeydown: function(node, event) {
        if( event.which == 32 ) {
          node.toggleSelect();
          return false;
        }
      },
      cookieId: 'mbc-" .$name ."',
      idPrefix: 'mbc-" .$name."' });";
    echo '$("#btnToggleSelect").click(function(){
      $("#'.$name.'").dynatree("getRoot").visit(function(node){
        node.toggleSelect();
      });
      return false;
    });
    $("#btnDeselectAll").click(function(){
      $("#'. $name.'").dynatree("getRoot").visit(function(node){
        node.select(false);
      });
      return false;
    });
    $("#btnSelectAll").click(function(){
      $("#'.$name.'").dynatree("getRoot").visit(function(node){
        node.select(true);
      });
      return false;
    });
  });
  
</script>';
 echo '<div id="'.$name.'"></div>';
 $selected_temp = ""; 
 if(is_array($selected))
 {
 		$selected_temp = implode(',',$selected);
 } 
 else 
 {
 	$selected_temp = $selected;
 } 
 echo form_hidden($ctrlname,$selected_temp);
}
function form_monthdropdown($name,$selectedvalue,$extra)
{
	$month=array(
			''=>'Month',
            '01'=>'January',
            '02'=>'February',
            '03'=>'March',
            '04'=>'April',
            '05'=>'May',
            '06'=>'June',
            '07'=>'July',
            '08'=>'August',
            '09'=>'September',
            '10'=>'October',
            '11'=>'November',
            '12'=>'December');
	return form_dropdown($name, $month, $selectedvalue);
}
function form_ctc_lksdropdown($name,$selectedvalue,$extra)
{
	$data = array();
	$increament = 1;
 	for($i = 0 ; $i<50 ; $i=$i+$increament)
 	{
 		$data[$i] = $i;
 	}
 	$increament = 10;
	for($i = 50 ; $i<=100 ; $i=$i+$increament)
 	{
 		$data[$i] = $i . "+";
 	}
	$increament = 20;
	for($i = 100 ; $i<=200 ; $i=$i+$increament)
 	{
 		$data[$i] = $i . "+";
 	}
	$increament = 50;
	for($i = 200 ; $i<=400 ; $i=$i+$increament)
 	{
 		$data[$i] = $i . "+";
 	}
	return form_dropdown($name, $data, $selectedvalue);
}
function from_tree_grid(array $tree,$level=1,$parent=0,$key = 0)
{
	$alternate = 1;
	foreach ($tree as $leaf)
	{
		
		if($leaf['parent_key'] == "")
		{
			
			$alternate = $alternate==1?2:1;
			$level = 1;
			render_leaf($leaf,$level,$alternate);
		}
	}
}
 function render_leaf($leaf,$level,$alternate)
	{
		$alternate = $alternate==1?2:1;
		echo '<tr class="line'.$alternate.'" id="row_'.$leaf['key'].'"><td class="line3 level-'.$level.'"><img src="'.asset_url().'images/bullet'.$level.'.png"> '.$leaf["title"].'</td><td class="total-count" id="'.$leaf['key'].'">'.$leaf['count'].'</td></tr>';
		if(array_key_exists("children",$leaf))
		{
			++$level;
			foreach($leaf["children"] as $subleaf)
			{
				if(array_key_exists("children",$subleaf))
				{
					render_leaf($subleaf,$level,$alternate);
				}
				else
				{
					$alternate = $alternate==1?2:1;
					echo '<tr class="line'.$alternate.'" id="row_'.$subleaf['key'].'"><td class="line3 level-'.$level.'"><img src="'.asset_url().'images/bullet'.$level.'.png"> '.$subleaf["title"].'</td><td class="total-count" id="'.$subleaf['key'].'">'.$subleaf['count'].'</td></tr>';
				}
			}
		}
	}
