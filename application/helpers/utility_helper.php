<?php  if (!defined('BASEPATH')) exit('No direct script access allowed');
function asset_url(){
   return base_url().'assets/';
}


function debug($array)
{
	echo "<pre>";
	print_r($array);
	echo "<pre/>";	
}

function get_months($years=0,$months=0)
{
	if($years == 0)
	{
		return $months;
	}
	else {
		return ($years*12) + $months;
	}
}

function convert_months_to_year_array($months=0)
{
	$date = array();
	$years =0;
	$date["months"] = $months%12;
	$years = ($months - $months%12)/12;
	$date["years"] = $years; 
	
	return $date;
}
 function buildTree(array $elements, $parentId = 0,$parent_id_field_name,$id_field_name,$parent_selectable=false,$mode) 
    {
    $branch = array();

    foreach ($elements as $element) {
        if ($element[$parent_id_field_name] == $parentId) {
            $children = buildTree($elements, $element[$id_field_name],$parent_id_field_name,$id_field_name,$parent_selectable,$mode);
            if ($children) {
            	//debug($children);
            	//debug($element);
            	$element['unselectable']= !$parent_selectable;
            	if($mode == 1)
            	{
            		$element['select'] =  is_children_selected($children);
            		$element['expand'] = is_children_selected($children);
            	}
            	//$element['hideCheckbox']= true;
                $element['children'] = $children;
            }
			
            $branch[] = $element;
        }
    }
    return $branch;
    }
        
    function is_children_selected($children)
    {
    	foreach ($children as $child)
    	{
    		if($child["select"] == 1)
    		{
    			return true;
    		}
    	}
    	return FALSE;
    }
    
    function get_age($dob){
    $dob_day = $dob->format('d');
    $dob_month = $dob->format('m');
    $dob_year = $dob->format('Y');	
    $year   = gmdate('Y');
    $month  = gmdate('m');
    $day    = gmdate('d');
     //seconds in a day = 86400
    $days_in_between = (mktime(0,0,0,$month,$day,$year) - mktime(0,0,0,$dob_month,$dob_day,$dob_year))/86400;
    $age_float = $days_in_between / 365.242199; // Account for leap year
    $age = (int)($age_float); // Remove decimal places without rounding up once number is + .5
    return $age;
}
