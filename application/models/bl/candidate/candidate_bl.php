<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
include_once( APPPATH . 'models/bl/core/user_bl' . EXT );
include_once( APPPATH . 'models/dl/entity/candidate' . EXT );
/**
 * 
 * Enter description here ...
 * @author CNC
 *
 */
class Candidate_Bl extends User_Bl {
	
 	public function __construct () 
    {
     	 parent::__construct();
    }
    
    /**
     * 
     * Enter description here ...
     * @param $candidate 
     */
    public function save($candidate,$login_user,$section_name="")
    {
    	$candidate->user_type_id = UserType::CANDIDATE;
    	switch(strtolower($section_name))
    	{
    		case 'user':
    			$user_id = $this->save_user($candidate,$login_user);
    			$candidate->user_id = $user_id;
    			break;
    		case 'profile':
    			$this->edit_profile($candidate,$login_user);
    			break;
    		case 'exp':
    			$candidate = $this->format_exp($candidate);
    			$total_exp = array_merge($candidate->can_curr_experince,$candidate->can_prev_experince);
    			$this->edit_experince($total_exp,$candidate->can_user_id);
    			break;
    		case 'departments':
    			$this->edit_user_departments($candidate->user_departments,$candidate->user_id);
    			break;
    		case 'qualifications':
    			
    			$candidate->can_qualifications = $this->format_qaulification($candidate->can_grad_qualifications,$candidate->can_postgrad_qualifications,$candidate->user_id);
    			$this->edit_qualifications($candidate->can_qualifications,$candidate->user_id);
    			
    			break;
    		case 'prefernces':
    			$this->edit_preferences($candidate->can_prefernces,$candidate->can_user_id);
    			break;		
    			default:	
    			$user_id = $this->save_user($candidate,$login_user);

    			if($candidate->user_id > 0)
    			{
    				$this->edit_profile($candidate,$login_user);
    				
    				$candidate = $this->format_exp($candidate);
    				$total_exp = array_merge($candidate->can_curr_experince,$candidate->can_prev_experince);
    				$this->edit_experince($total_exp,$candidate->user_id);
					 $candidate->user_id;
    				$this->edit_user_departments($candidate->user_departments,$candidate->user_id);
    				$this->edit_preferences($candidate->can_prefernces,$candidate->user_id);
    				$candidate->can_qualifications = $this->format_qaulification($candidate->can_grad_qualifications,$candidate->can_postgrad_qualifications,$candidate->user_id);
    				$this->edit_qualifications($candidate->can_qualifications,$candidate->user_id);
    			}
    	}
    	return $candidate->user_id;
    }
    /**
     * 
     * filters the candidate tables columns from the candidate object
     * @param unknown_type $candidate
     */
    private function get_candidate_columns($candidate)
    {
    	return  elements(array('can_user_id', 'can_code', 'can_photo', 'can_dob', 'can_gender', 'can_marital_status', 'can_ctc', 'can_total_experience', 'can_resume_file', 'can_is_verified', 'can_verified_by', 'can_status_id', 'can_verified_date', 'can_band','can_created_by','can_created_date','can_modified_date','can_modified_by'),(array)$candidate);
    }
    public function candidate_exists($can_user_id)
    {
    	$obj_candidate = new Candidate();
		$table = $obj_candidate->get_table_name();
		$this->db->from("{$table}");
        $this->db->select("1");
        //$this->db->where("{$table}.user_is_deleted =",0 );
        $this->db->where("{$table}.can_user_id",$can_user_id );
       	$row = $this->db->count_all_results();
        	if($row > 0)
        		return true;
        	return false;
    }
	public function edit_candidate_resume($candidate_resume_blob)
	{
		//debug($candidate);
		$obj_can_resume = new Candidate_Resume_Blob();
		$table = $obj_can_resume->get_table_name();
		$id_name = $obj_can_resume->get_table_id_name();
		//delete records
    	$this->db->where("{$id_name}",$candidate_resume_blob->cr_user_id);
		$this->db->delete("{$table}");
		$this->db->set("cr_user_id",$candidate_resume_blob->cr_user_id);
		$this->db->set("cr_resume",$candidate_resume_blob->cr_resume);
		$this->db->insert("{$table}");
  
    		
	}
	public function save_candidate_files($files, $candidate_id)
	{
		//debug($files);
		if(isset($files['resume']))
		$this->db->set("can_resume_file",$files['resume']);
		if(isset($files['photo']))
		$this->db->set("can_photo",$files['photo']);
		$this->db->where('can_user_id', $candidate_id);
	    if(!$this->db->update('mbc_candidate'))
	    {
	        throw new Exception($this->db->_error_message(), $this->db->_error_number());
	    }
	}
    /**
     * 
     * Add/Edit candidate profile
     * @param $candidate
     * @param $login_user
     */
    private function edit_profile($candidate,$login_user)
    {
    	//debug($candidate);
    	
    	if($candidate->user_id > 0)
    	{
    		
    		$is_new = $this->candidate_exists($candidate->user_id) ? false:true; 
			$candidate->can_user_id = $candidate->user_id;
    		//set fields
    		$this->db->set("can_user_id",$candidate->user_id);
    		if($candidate->can_dob != "")
    		{
    			$can_dobdate = new DateTime($candidate->can_dob);
    			$this->db->set("can_dob",$can_dobdate->format('Y-m-d'));
    			//If DOB is given then autocalculate the age
    			$candidate->can_age = get_age($can_dobdate);
			}
    		else
			{
				$this->db->set("can_dob",null);
			}
   			$this->db->set("can_age",$candidate->can_age);
    		
    		$this->db->set("can_gender",$candidate->can_gender);
    		$this->db->set("can_marital_status",$candidate->can_marital_status);
    		$this->db->set("can_type_id",$candidate->can_type_id);
    		$this->db->set("can_ctc",$candidate->can_ctc);
    		$this->db->set("can_ctc_notes",$candidate->can_ctc_notes);
    		$this->db->set("can_total_experience",$candidate->can_total_experience);
			$this->db->set("can_resume_file",$candidate->can_resume_file);
			$this->db->set("can_photo",$candidate->can_photo);
    		$this->db->set("can_band",$candidate->can_band);
    		$this->db->set("can_designation_id",$candidate->can_designation_id);
    		$this->db->set("can_location_id",$candidate->can_location_id);
    		$this->db->set("can_current_branch_id",$candidate->can_current_branch_id);
    		
    		$this->db->set("can_certifications",$candidate->can_certifications);
	    	if($is_new)
    		{
    			$this->db->set("can_created_by",$login_user);
    			$this->db->set("can_created_date",'now()',false);
    			if(!$this->db->insert($candidate->get_table_name()))
	    		{
 			
	    				throw new Exception($this->db->_error_message(), $this->db->_error_number());
	    		}
	    	}
    		else
    		{
    			$this->db->set("can_modified_by",$login_user);
    			$this->db->set("can_modified_date",'now()',false);
    			$this->db->where($candidate->get_table_id_name(), $candidate->user_id);
	             if(!$this->db->update($candidate->get_table_name()))
	             {
	             	throw new Exception($this->db->_error_message(), $this->db->_error_number());
	             }
    		}
			/// Save resume in blob
			//$this->edit_candidate_resume($candidate);
    	}
    }
    
    private function format_exp($candidate)
    {
    	$expcurr = new Candidate_Experince();
    	$expcurr->ce_user_id = $candidate->user_id;
    	$expcurr->ce_bank_id = $candidate->can_curr_experince;
    	$expcurr->ce_is_current_bank = 1;
    	$expcurr_[] = (array)$expcurr;
    	$candidate->can_curr_experince = $expcurr_; 
    	
    	$totol_exp = array();
    	$temp_arr = explode(',',$candidate->can_prev_experince);
    	foreach($temp_arr as $pbanks)
  		{
  			$expprev = new Candidate_Experince();
    		$expprev->ce_user_id = $candidate->user_id;
    		$expprev->ce_bank_id = $pbanks;
    		$expprev->ce_is_current_bank = 0;
			$totol_exp[] = (array)$expprev;
  		}
    	$candidate->can_prev_experince = $totol_exp;
    	return $candidate;
    }
    
    private function format_qaulification($strgrad,$strpostgrad,$candidate_id)
    {
    	$Candidate_Qualifications = array();
		if($strgrad != '')
    	{
    		$temp_can_grad = explode(',',$strgrad);
    		$temp_can_grad = array_unique($temp_can_grad); 
	    	foreach($temp_can_grad as $value=>$key) 
	    	{
	    		$Candidate_Qualification = new Candidate_Qualifications();
	    		$Candidate_Qualification->cq_user_id = $candidate_id;
	    		$Candidate_Qualification->cq_qualification_master_id = $key;
	    		$Candidate_Qualification->cq_qualification_type_id = Qulification_TYPE::GRADUATION;
	    		$Candidate_Qualifications[] =(array) $Candidate_Qualification;
	    	} 
		}
    	if($strpostgrad!='')
    	{
    		$temp_can_postgrad = explode(',',$strpostgrad);
    		$temp_can_postgrad = array_unique($temp_can_postgrad);
	    	foreach($temp_can_postgrad as $value=>$key) 
	    	{
	    		$Candidate_Qualification = new Candidate_Qualifications();
	    		$Candidate_Qualification->cq_user_id = $candidate_id;
	    		$Candidate_Qualification->cq_qualification_master_id = $key;
	    		$Candidate_Qualification->cq_qualification_type_id = Qulification_TYPE::POSTGRADUATION;
	    		$Candidate_Qualifications[] =(array) $Candidate_Qualification;
	    	}
    	} 
    	return (array) $Candidate_Qualifications;
    }
    
    /**
     * 
     * Add/Edit the candidate experince
     * @todo : check for only one current exp.
     */
    private function edit_experince($can_experince,$can_user_id)
    {
    	if(!empty($can_experince))
    	{
    		$obj_ce_exp = new Candidate_Experince();
			$table = $obj_ce_exp->get_table_name();
			//delete records
    		$this->db->where("{$table}.ce_user_id", $can_user_id);
			$this->db->delete("{$table}"); 
			//insert new records
			$this->db->insert_batch("{$table}", $can_experince);
    	}
    }
    public function get_prev_experince($can_user_id)
    {
    	$obj_ce_exp = new Candidate_Experince();
		$table = $obj_ce_exp->get_table_name();
		$this->db->from("{$table}");
        $this->db->select("*");
        //$this->db->where("{$table}.user_is_deleted =",0 );
        $this->db->where("{$table}.ce_user_id",$can_user_id );
        $this->db->where("{$table}.ce_is_current_bank",0 );
        $this->db->order_by("{$table}.ce_is_current_bank Asc");
       	$query = $this->db->get();
        return $query->result();
    }
	public function get_current_experince($can_user_id)
    {
    	$obj_ce_exp = new Candidate_Experince();
		$table = $obj_ce_exp->get_table_name();
		$this->db->from("{$table}");
        $this->db->select("*");
        //$this->db->where("{$table}.user_is_deleted =",0 );
        $this->db->where("{$table}.ce_user_id",$can_user_id );
        $this->db->where("{$table}.ce_is_current_bank",1 );
       	$query = $this->db->get();
        return $query->result();
    }
	
    public function get_qualifications($can_user_id)
    {
    	$obj_cq = new Candidate_Qualifications();
		$table = $obj_cq->get_table_name();
		$this->db->from("{$table}");
        $this->db->select("*");
        //$this->db->where("{$table}.user_is_deleted =",0 );
        //$this->db->where("{$table}.cq_qualification_type_id", $qualification_type_id);
        $this->db->where("{$table}.{$obj_cq->get_table_id_name()}",$can_user_id );
       	$query = $this->db->get();
        return $query->result();
    }
	private function edit_qualifications($can_qualifications,$can_user_id)
	{
		if(!empty($can_qualifications))
    	{
    		$obj_cq = new Candidate_Qualifications();
			$table = $obj_cq->get_table_name();
			//delete records
    		$this->db->where("{$table}.{$obj_cq->get_table_id_name()}", $can_user_id);
    		//$this->db->where("{$table}.cq_qualification_type_id", $qualification_type_id);
			$this->db->delete("{$table}"); 
			//insert new records
			$temp_can_qualifications = array();
			foreach($can_qualifications as $qual)
			{
				 $qual["cq_user_id"] = $can_user_id;
				 $temp_can_qualifications[] = $qual ;
				 
			}
			//$temp_can_qualifications_f = elements(array('cq_qualification_master_id', 'cq_qualification_type_id', 'cq_user_certification_name', 'cq_user_id'),(array)$temp_can_qualifications);
			$this->db->insert_batch("{$table}", $temp_can_qualifications);
    	}
    
	}
	private function edit_preferences($can_prefernces,$can_user_id)
	{
			if(!empty($can_prefernces))
    		{
    			$obj_cp = new Candidate_Preferences();
				$table = $obj_cp->get_table_name();
				//delete records
    			$this->db->where("{$table}.{$obj_cp->get_table_id_name()}", $can_user_id);
				$this->db->delete("{$table}"); 
				//insert new records
    			if(!$this->db->insert($obj_cp->get_table_name(),$can_prefernces))
	    		{
	    				throw new Exception($this->db->_error_message(), $this->db->_error_number());
	    		}
    		}
    
	}
 	public function get_preferences($can_user_id)
    {
    	$obj_cp = new Candidate_Preferences();
		$table = $obj_cp->get_table_name();
		$this->db->from("{$table}");
        $this->db->select("*");
        //$this->db->where("{$table}.user_is_deleted =",0 );
        $this->db->where("{$table}.{$obj_cp->get_table_id_name()}",$can_user_id );
       	$query = $this->db->get();
        return $query->result();
    }	
	
    //todo : use join
    public function get_by_can_user_id($can_user_id=null,$active=1)
    {
		
    	$candidate = new Candidate();

    	$obj_user_entity = new User();
    	$table = $candidate->get_table_name();
		$table_id_name = $candidate->get_table_id_name();
    	
		$table_user_id_name = $obj_user_entity->get_user_table_id_name();
		$table_user = $obj_user_entity->get_user_table_name();
		
		$table_loc = "mbc_location_master";
		$this->db->from("{$table_user}");
        $this->db->select("*");
        $this->db->join("{$table}","$table_id_name = $table_user_id_name","left");
        $this->db->join("{$table_loc}","lm_id = can_location_id","left");
        if($active==1)
        {
        	$this->db->where("{$table_user}.user_is_deleted =",0 );
        }
        $this->db->where("{$table_user}.{$table_user_id_name}",$can_user_id );
        $query = $this->db->get();
        $candidate =  objectToObject($query->row(), 'Candidate');
		$candidate->can_location_name = $candidate->lm_name; 
        $candidate->can_prev_experince = $this->get_prev_experince($can_user_id);
        $candidate->can_curr_experince = $this->get_current_experince($can_user_id);
        $candidate->user_departments = $this->get_user_departments($can_user_id);
        $candidate->can_qualifications = $this->get_qualifications($can_user_id);
        if(!empty($candidate->can_qualifications))
        {
	   		foreach ($candidate->can_qualifications as $qaul)
	    	{
	    			if($qaul->cq_qualification_type_id == Qulification_TYPE::POSTGRADUATION)
	    				$candidate->can_postgrad_qualifications .= $candidate->can_postgrad_qualifications == "" ? $qaul->cq_qualification_master_id : "," .$qaul->cq_qualification_master_id ;
	    			else
	    				 $candidate->can_grad_qualifications .= $candidate->can_grad_qualifications == "" ? $qaul->cq_qualification_master_id : "," .$qaul->cq_qualification_master_id ;
	    	}
        }
    	return $candidate; 
    }
 /**
	 * 
	 * This function should return an array of objects (Candidate)
	 * Candidate object should have following properties : id, first name, last name, designation, current bank and location
	 * @author CNC
	 *
	 */
	public function search_candidate($department_id,$bank_id,$designation_id)
    {
		//return array(0=>$this->get_user_by_user_id(6));
		//if($department_id != "")
		{
			$this->db->distinct();
			$this->db->select('mbc_users.*');
			$this->db->select('mbc_candidate.*');
			$this->db->select('mbc_location_master.lm_name As can_location_name');
			$this->db->select('mbc_bank_master.bm_name As can_curr_experince');
			$this->db->select('mbc_designation_master.dm_name As can_designation_name');
			$this->db->where_in('mbc_users.user_type_id',UserType::CANDIDATE);
			if($department_id != "")
				$this->db->where_in('mbc_user_departments.ud_department_id',array_map('intval', explode(",", $department_id)));
			if($designation_id !="")
				$this->db->where_in('mbc_candidate.can_designation_id',$designation_id);
			if($bank_id !="")
			{	
				$this->db->where_in('mbc_can_experince.ce_bank_id',$bank_id);
			}
			//$this->db->where('mbc_user_departments.ud_is_current',1);
			$this->db->join('mbc_candidate','mbc_users.user_id = mbc_candidate.can_user_id','left');
			$this->db->join('mbc_designation_master','mbc_candidate.can_designation_id = mbc_designation_master.dm_id','left');
			$this->db->join('mbc_user_departments','mbc_user_departments.ud_user_id =  mbc_users.user_id','left');
			$this->db->join('mbc_can_experince','mbc_can_experince.ce_user_id = mbc_users.user_id and mbc_can_experince.ce_is_current_bank = 1','left');
			$this->db->join('mbc_bank_master','mbc_bank_master.bm_id = mbc_can_experince.ce_bank_id','left');
			$this->db->join('mbc_location_master','mbc_location_master.lm_id = mbc_candidate.can_location_id','left');
			
			$this->db->from('mbc_users');
			$query = $this->db->get();
			return $query->result();
		}
		return new Candidate_Bl();
		/*and mbc_can_experince on mbc_can_experince.ce_bank_id in ($bank_id)
		and mbc_user_departments.ud_department_id in ($departments)
		and mbc_user_departments.ud_is_current = 1
		and mbc_can_experince.ce_is_current_bank = 1*/
		
		/*select mbc_users.*,mbc_candidate.* from mbc_candidate
		left join mbc_user_departments on mbc_user_departments.ud_department_id =  mbc_candidate.can_user_id
		left join mbc_can_experince on mbc_can_experince.ce_user_id = mbc_candidate.can_user_id
		left join mbc_users ON mbc_users.user_id = mbc_candidate.can_user_id*/
    }
}