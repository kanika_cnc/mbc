<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
include_once( APPPATH . 'models/bl/core/user_bl' . EXT );
include_once( APPPATH . 'models/dl/entity/mbc_user' . EXT );
/**
 * 
 * Enter description here ...
 * @author CNC
 *
 */
class Mbc_User_Bl extends User_Bl {
	
 	public function __construct () 
    {
     	 parent::__construct();
    }
    
    public function save($user,$login_user,$section="")
    {
    	$user->user_type_id = ($user->is_cm == true) ? UserType::MBC_CM :UserType::MBC_Recruiter;
    	//auto activate mbc cm and recruters
    	$user->user_is_active = 1;
    	$user->user_activation_date = date('Y-m-d H:i:s');
    	$last_user_id = parent::save_user($user, $login_user);
   	
    	switch($section)
    	{
    		case "user":
	    			return $last_user_id;
	    		break;
    	}
		if($last_user_id > 0)
		{
	    	//Save departments
    		$temp_arr = explode(',',$user->user_departments);
    		$user->user_departments = array();
	    	foreach($temp_arr as $departments)
	  		{
	  			$user_departments = new User_Departments();
				$user_departments->ud_department_id = $departments;
				$user_departments->ud_user_id = $last_user_id;
				$user->user_departments[] = (array)$user_departments;
	  		}
  			$this->edit_user_departments($user->user_departments,$last_user_id);
	
  			//Save profile attributes
  			//release all user before saving
  			$this->release_profile_attribute($last_user_id,$login_user);
  			if($user->is_cm)
  			{
	  			$this->edit_user_profile_attributes($user->user_profile_attributes, $login_user,$last_user_id,$user->user_type_id);
  			}
		}
    }
    
    public function fetch($user_type_id,$format,$exclude_user=array(),$default_row=array(),$active=1)
    {
    	if($user_type_id == UserType::MBC_USER)
    	{
    		$user_type_id =  array(UserType::MBC_CM, UserType::MBC_Recruiter);	
    	}
    	$filters = array(array("col" => "user_type_id","val"=>$user_type_id,"opt"=>"in"));
    	
    	if(!empty($exclude_user))
    	{
    		$filters[] = (array("col" => "user_type_id","val"=>$exclude_user,"opt"=>"not_in"));
    	}
    	return $this->get_users($filters,$active,$format,$default_row);
    }
    
    public function get($user_id,$active)
    {
    	$obj_user = new Mbc_user();
    	$obj_user =  objectToObject(array_to_object($this->get_user_by_user_id($user_id,$active)),"Mbc_user");
    	$obj_user->is_cm = $obj_user->user_type_id == UserType::MBC_CM ? true:false;
    	$obj_user->user_departments =  $this->get_user_departments($user_id);
    	$obj_user->user_profile_attributes =  $this->get_user_profile_attributes($user_id,1);
    	return $obj_user;
    }
    public function convert_attribute_to_object($user_profile_attributes,$last_user_id,$usertype_id)
    {
    		$temp_arr = (array)$user_profile_attributes;
  			$user_profile_attributes = array();
  			$count = count((array)$temp_arr["uttr_id"]);

  			for($index = 0;$index <$count;$index++)
  			{
  				$temp_arr_uttr_id = array_values((array)$temp_arr["uttr_id"]);
  				$temp_arr_uttr_bank_id = array_values((array)$temp_arr["uttr_bank_id"]);
  				$temp_arr_uttr_recruiter_id =array_values((array)$temp_arr["uttr_recruiter_id"]);
  				
  				$user_attr = new User_Profile_Attributes();
  				$user_attr->uttr_id = $temp_arr_uttr_id[$index];
				$user_attr->uattr_user_id = $last_user_id;
				$user_attr->uttr_type_id = $usertype_id;
				$user_attr->uttr_bank_id =  $temp_arr_uttr_bank_id[$index];
				$user_attr->uttr_recruiter_id =  $temp_arr_uttr_recruiter_id[$index];
				$user_attr->uttr_is_released = 0;
				$user_profile_attributes[] = $user_attr;
  			}
  			return $user_profile_attributes;
    }
    public function delete($user_id,$login_user,$is_restore=0)
    {
		if($is_restore == 0)
    	$this->delete_user($user_id, $login_user);
		else
		$this->restore_user($user_id, $login_user);
    }
	
    
}
