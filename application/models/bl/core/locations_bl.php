<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 * @author CNC
 *
 */
class Locations_Bl extends Base_Model{

	/*
	 * @todo: If state is 0 then don;t show parent
	 */
	public function get_locations($keyword)
	{
		 $this->load->model("dl/entity/locations","obj_loc_entity");
		 $table = $this->obj_loc_entity->get_table_name();
		 $this->db->from("{$table}");
         $this->db->select("{$table}.lm_id,{$table}.lm_name As city,state.lm_name As states");
         $this->db->join("$table As state","state.lm_id = {$table}.lm_parent_lm_id");
         $this->db->where("{$table}.lm_is_deleted = ","0" );
         $this->db->where("{$table}.lm_parent_lm_id is not null" );
         $this->db->where("state.lm_is_deleted = ","0" );
         $this->db->where("{$table}.lm_name like '{$keyword}%'");
         $this->db->order_by("state.lm_name");
         $query = $this->db->get();
         $data = $query->result();
         return $data;
          
	}
	
	function buildTree(array $elements, $parentId = 0) {
    $branch = array();

    foreach ($elements as $element) {
        if ($element['dm_parent_dm_id'] == $parentId) {
            $children = $this->buildTree($elements, $element['dm_id']);
            if ($children) {
            	$element['unselectable']= true;
            	//$element['hideCheckbox']= true;
                $element['children'] = $children;
            }
            $branch[] = $element;
        }
    }
    return $branch;
}
	
}