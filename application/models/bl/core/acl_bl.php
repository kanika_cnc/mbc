<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
include_once( APPPATH . 'models/bl/core/user_bl' . EXT );
include_once( APPPATH . 'models/dl/entity/user' . EXT );

/**
 * 
 * Model for ACL
 * @author CNC
 *
 */ 
class Acl_bl extends User_Bl {
	
 	public function __construct () 
    {
     	 parent::__construct();
    }
    
    public function validateUser($email,$password)
    {
		//echo $password; 
    	$obj_user_entity = new User();
		$table = $obj_user_entity->get_user_table_name();
        $this->db->where("{$table}.user_email",$email);
        $this->db->where("{$table}.user_password",$password);
        $query = $this->db->get($table);
        if($query->num_rows == 1)
        {
            $row = $query->row();
            return $row;
        }
        return false;
    }
    public function get_user_by_email($email)
    {
		$obj_user_entity = new User();
		$table = $obj_user_entity->get_user_table_name();
        $this->db->where("{$table}.user_email",$email);
		$query = $this->db->get($table);
		if($query->num_rows == 1)
        {
            $row = $query->row();
            return $row;
        }
        return false;
	}
    public function change_user_password($new_password,$user_id)
	{
		$obj_user_entity = new User(); 
    	$obj_user_entity->user_id= $user_id;
		$this->db->where('user_id', $obj_user_entity->user_id);
		$data = array(
               'user_password' => $new_password
            );
		if(!$this->db->update($obj_user_entity->get_user_table_name(), $data))
		return false; 
		else
		return true;
	}
	public function update_last_logged_in_date($user_id)
	{
		$dt = date("Y-m-d H:i:s");
		$this->db->set("user_last_login",$dt);
		$this->db->where('user_id', $user_id);
	    $this->db->update('mbc_users');
	}
}
