<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
include_once( APPPATH . 'models/dl/entity/departments' . EXT );
/**
 * 
 * @author CNC 
 * 
 */ 
class Departments_Bl extends Base_Model{

	private $valid_status = array(-1,0,1);
	/**
	 * 
	 * Method to return list of all departments
	 * @param bool $exclude_alloted_departments : 
	 * 	Exclude departments which are already alloted to other system users. This feature
	 * 	specifically exclude on admin users not for candidates and banks
	 * @param int (0,1,-1)0 : Deleted, 1 : Active ,-1 :All 
	 */
	public function get_departments($exclude_alloted_departments=FALSE,$status=1)
	{
		
		if(!in_array($status, $this->valid_status))
			throw new Exception ("Invalid status passed in fucntion",9997);
		$table = Departments::TABLE;
		$this->db->from("{$table}");
		$this->db->select("dm_name As title");
		$this->db->select("dm_id As 'key'");
		$this->db->select("dm_parent_dm_id As parent_key");
		if($exclude_alloted_departments)
		{
			$this->db->select("(case coalesce(tmpalloted.ud_department_id,0) when 0 then false else true end) As hideCheckbox",false);
			$this->db->select("(case coalesce(tmpalloted.ud_department_id,0) when 0 then false else true end) As unselectable",false);
			$this->db->join("(select distinct ud_department_id from mbc_user_departments
     						left join mbc_users on mbc_users.user_id = mbc_user_departments.ud_user_id
     						where mbc_users.user_type_id in (" .UserType::MBC_CM .",".UserType::MBC_Recruiter.")) tmpalloted","mbc_department_master.dm_id= tmpalloted.ud_department_id","left");
		}
		if($status != -1)
			$this->db->where("{$table}.dm_is_deleted != ",$status);
        $this->db->order_by("title");
        $query = $this->db->get();
       	$data = (array)$query->result();
        return $data;
	}
	
	public function get_departments_with_users($bank_id,$designation_id)
	{
		
		$designation  = $designation_id !="" ? " and mc.can_designation_id in (" . $designation_id . ")":"";
		$bank = $bank_id!="" ? " and mce.ce_bank_id = " . $bank_id . " and mce.ce_is_current_bank = 1": "";
		$this->db->select("(mdm.dm_id) As 'key'");
		$this->db->select("(mdm.dm_name) As 'title'");
		//on mu.user_id = mud.ud_user_id
		// mce.ce_user_id = mu.user_id
		//mc.can_user_id = mu.user_id
		if(!empty($designation) && !empty($bank))
		{
		$this->db->select ("concat('(',COALESCE((select count(mud.ud_user_id) from mbc_users mu
			  	left join mbc_user_departments mud on mu.user_id = mud.ud_user_id
  				left join mbc_can_experince mce on mce.ce_user_id = mu.user_id
  				left join mbc_candidate mc on mc.can_user_id = mu.user_id
  				where  mud.ud_department_id =  mdm.dm_id
  				and mud.ud_is_current = 1
  				and mu.user_type_id = 3".$designation .$bank  .  " GROUP BY mud.ud_department_id),0),')') As 'count'",false);
		}
		elseif(!empty($designation))
		{
			$this->db->select ("concat('(',COALESCE((select count(mud.ud_user_id) from mbc_users mu
			  	left join mbc_user_departments mud on mu.user_id = mud.ud_user_id
  				left join mbc_candidate mc on mc.can_user_id = mu.user_id
  				where  mud.ud_department_id =  mdm.dm_id
  				and mud.ud_is_current = 1
  				and mu.user_type_id = 3".$designation .  " GROUP BY mud.ud_department_id),0),')') As 'count'",false);
		}
		elseif(!empty($bank))
		{
			$this->db->select ("concat('(',COALESCE((select count(*) from mbc_users mu
			  	left join mbc_user_departments mud on mu.user_id = mud.ud_user_id
  				left join mbc_can_experince mce on mce.ce_user_id = mu.user_id
  				where  mud.ud_department_id =  mdm.dm_id
  				and mud.ud_is_current = 1
  				and mu.user_type_id = 3".$bank  .  " GROUP BY mud.ud_department_id),0),')') As 'count'",false);
		}
		else
		{
			$this->db->select ("concat('(',COALESCE((select count(*) from mbc_users mu
			  	left join mbc_user_departments mud on mu.user_id = mud.ud_user_id
  				where  mud.ud_department_id =  mdm.dm_id
  				and mud.ud_is_current = 1
  				and mu.user_type_id = 3 ),0),')') As 'count'",false);
		}
		//$this->db->select ("concat(dm_name,'(',count(mbc_user_departments.ud_user_id),')') As title",false);
		$this->db->select ("mdm.dm_parent_dm_id As parent_key");
		$this->db->select ("1 As expand",false);
		/*$this->db->join('mbc_user_departments','mbc_user_departments.ud_department_id =  mbc_department_master.dm_id and mbc_user_departments.ud_is_current = 1 and mbc_user_departments.ud_user_id in (select user_id from mbc_users where user_type_id= 3)','left');
		if($bank_id != "")
		{

			$this->db->join('mbc_can_experince','mbc_can_experince.ce_user_id = mbc_user_departments.ud_user_id and mbc_can_experince.ce_is_current_bank = 1 and mbc_can_experince.ce_bank_id='.$bank_id,'left');
			//$this->db->where('mbc_can_experince.bank_id',$bank_id);
		}
		$this->db->group_by(array('dm_id','dm_name','dm_parent_dm_id'));*/
		//$this->db->group_by(array('mu.user_id'));
		$this->db->from('mbc_department_master As mdm');
		$query = $this->db->get();
	
	return $query->result();
 
/*left join mbc_candidate on mbc_candidate.can_user_id =   mbc_user_departments.ud_user_id 
left join mbc_can_experince on mbc_can_experince.ce_bank_id =  $bank_id
where 
mbc_candidate.can_designation_id = $designation_id
and mbc_can_experince.ce_is_current_bank = 1
and mbc_user_departments.ud_is_current = 1
group by dm_id,dm_name,dm_parent_dm_id*/
	}

	
}