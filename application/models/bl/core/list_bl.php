<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
include_once( APPPATH . 'models/dl/entity/member_banks' . EXT );
include_once( APPPATH . 'models/dl/entity/qualifications' . EXT );
include_once( APPPATH . 'models/bl/core/banks_bl' . EXT );
/**
 * 
 * Enter description here ...
 * @author CNC
 *
 */
class List_Bl extends Base_Model {
	
	const FORMAT_JSON=1;
	const FORMAT_HTML=2;
	const FORMAT_TREE=3;
	
 	/**
 	 * 
 	 * Enter description here ...
 	 */
	public function fetch_banks($keyword='',$default_row=array(),$active=1)
 	{
		$data = $this->get_table_dropdown('bank_master', 'bm_id', 'bm_name', '',array("cond"=>"bm_parent_bm_id is not null and bm_is_deleted = 0 and mbm.bm_parent_bm_id in (select bm_id from mbc_bank_master where bm_parent_bm_id is null
		and bm_is_deleted = 0) and bm_name like '%".$keyword ."%'"),$default_row);
		return $data;
 	}
 	public function get_banks_drop_down_list($keyword='',$default_row=array())
 	{
 		//@todo: add logic to add default row.
 		$data = $this->get_table_dropdown('bank_master', 'bm_id', 'bm_name', '',array("cond"=>"bm_parent_bm_id is not null and bm_is_deleted = 0 and bm_parent_bm_id in (select bm_id from mbc_bank_master where bm_parent_bm_id is null
		and bm_is_deleted = 0) and bm_name like '%".$keyword ."%'"),$default_row);
		return $data;
 		
 	}
 	public function get_banks_hierarchical_list()
 	{
 		$this->load->model("dl/entity/banks","obj_bank_entity");
		$table = $this->obj_bank_entity->get_table_name();
		$this->db->from("{$table}");
        $this->db->select("*");
        
        $this->db->where("{$table}.bm_is_deleted = 0 and " . $this->db->dbprefix.$table.  ".bm_parent_bm_id in " . "(select bm_id from ". $this->db->dbprefix.$table ." where bm_parent_bm_id is null and bm_is_deleted = 0)");
        //$in_clause = "(select bm_id from ". $this->db->dbprefix.$table ." where bm_parent_bm_id is null and bm_is_deleted = 0)";
        //$this->db->where_in("{$table}.bm_parent_bm_id",$in_clause,false); 
        $this->db->or_where("{$table}.bm_parent_bm_id is null ");
         // $this->db->order_by("bm_name");
          $query = $this->db->get();
          $data = (array)$query->result();
          $data_formatted = array();
          foreach($data as $index => $row)
          {
          	$reformat_row = array();
          	//$row = (arra$row;
          	$reformat_row["title"] = $row->bm_name;
          	$reformat_row["key"] = $row->bm_id;  
          	$reformat_row["parent_key"] = $row->bm_parent_bm_id;
          	$data_formatted[$index] = $reformat_row;
          }
          $tree = $this->buildTree($data_formatted,0,'parent_key','key',false);
          return($tree);
 	}
	
 	/**
 	 * 
 	 * Function to return the branch list as key,value pair
 	 * @param $bank_id : int
 	 * @param $custom_title : location will appened with branch name
 	 * @param $default_row : blank row with select
 	 */
	public function get_branches($bank_id,$custom_title=false,$default_row=true)
 	{
 		$bank_model = new Banks_Bl();
 		$data = $bank_model->get_branches($bank_id);
 		if($default_row)
 			$format_data = array(""=>"Select Branch");
 		else
 			$format_data = array();
 		if(!empty($data))
 		{	
	 		foreach ($data as $branch)
			{
				if($custom_title)
					$title =  $branch->bm_name . ' , ' . $branch->city . ' ' . $branch->state;
				else 
					$title =  $branch->bm_name;	 
				$format_data[$branch->bm_id] = $title;
			}
 		}
 		return $format_data;
 	}
 	
	/**
	 * 
 	 * Function to return the list of qualification
 	 * 
 	 * @param const $type : Qualifications.GRDUATATION 18;Qualifications.POSTGRDUATATION
	 * @param $default_row : return data includes Default blank row with default text 
	 * @param $format : List_Bl.FORMAT_JSON,List_Bl.FORMAT_HTML,List.Bl.FORMAT_TREE
	 */
	public function get_qualifications($type,$format=self::FORMAT_HTML,$default_row=false,$include_deleted=false)
 	{
 		if($type == '')
 			return "Qualification Type  missing";
 		switch ($format)
 		{
 			case self::FORMAT_HTML:
 					return $this->get_table_dropdown('qualification_master', 'qm_id', 'qm_name', '',array("cond"=>"qm_parent_qm_id = ". $type ." and qm_is_deleted = 0 and qm_name like '%".$keyword ."%' "),$default_row);
 				break;
 			case self::FORMAT_JSON:
 					throw new Exception("Need to be implemented", 9998);
 				break;
 			case self ::FORMAT_TREE:
 					$this->db->select("qm_id As 'key'");
 					$this->db->select("qm_name As 'title'");
 					$this->db->select("qm_parent_qm_id As parent_key");
 					$this->db->select("(1) As expand");
 					$this->db->where("qm_parent_qm_id",$type);
 					$this->db->or_where("qm_id",$type);
 					if(!$include_deleted)
 						$this->db->where("qm_is_deleted",0);
 					$query = $this->db->get("qualification_master");
 					$parent =  $query->result();
 					
 					$this->db->select("(mbc_child.qm_id) As 'key'");
 					$this->db->select("(mbc_child.qm_name) As title");
 					$this->db->select("(mbc_child.qm_parent_qm_id) As parent_key");
 					$this->db->select("(0) As expand");
 					$this->db->where("(mbcparent.qm_parent_qm_id)",$type);
 					if(!$include_deleted)
 						$this->db->where("mbc_child.qm_is_deleted",0);
 					$this->db->join("mbc_qualification_master mbcparent","mbc_child.qm_parent_qm_id = mbcparent.qm_id","left");
 					$query = $this->db->get("qualification_master As mbc_child");
 					$child =  $query->result();
 					return array_merge($parent,$child);
 				break;	
 		}
 		return false;
 	}
 	
 	/**
 	 * 
 	 * return the list of all active member banks
 	 * @param $format : '' in case require as row objects
 	 * array : for array object
 	 * dropdown : for key value pair
 	 */
 	public function get_member_banks($format='',$default_row=array())
 	{
 		$obj_entity = new Member_Banks();
		$table = $obj_entity->get_table_name();
		$this->db->from("{$table}");
        $this->db->select("mb_id,bm_name");
        $this->db->join("bank_master","bm_id=mb_master_bank_id");
        $this->db->where("{$table}.mb_is_deleted",0 );
        $this->db->where("bank_master.bm_is_deleted",0 );
        //$this->db->where("{$table}.{$obj_user_entity->get_table_id_name()}",$user_id );
        $query = $this->db->get();
        
        switch(strtolower($format))
        {
        	case 'array':
        		return $query->result_array();
        		break;
        	case 'dropdown':
        			$data = $query->result();
        			$data_formated = array();
        			if(!empty($default_row))
        			{
        				foreach ($default_row as $key=>$val)
        				$data_formated[$key] = $val;
        			}
        			
        			foreach ($data as $row)
        			{
        				$data_formated[$row->mb_id] = $row->bm_name;
        			}
        			return $data_formated;
        	default:
        		return $query->result(); 
        } 
 	}
 	
	/**
 	 * 
 	 * return the list of all active member banks
 	 * @param $format : '' in case require as row objects
 	 * array : for array object
 	 * dropdown : for key value pair
 	 */
 	public function get_available_member_banks($cm_user_id='',$format='',$default_row=array())
 	{
 		$obj_entity = new Member_Banks();
		$table = $obj_entity->get_table_name();
		$this->db->from("{$table}");
        $this->db->select("mb_id,bm_name");
        $this->db->join("bank_master","bm_id=mb_master_bank_id");
        $this->db->where("{$table}.mb_is_deleted",0 );
        $this->db->where("bank_master.bm_is_deleted",0 );
        $user_condition = '';
        if($cm_user_id != '')
        {
        	
        	$user_condition = ' and mu.user_id <> ' .$cm_user_id;
        }
        
        	$this->db->where("{$table}.mb_id not in (select uttr_bank_id FROM 
													mbc_user_profile_attributes 
												left join mbc_users mu on mu.user_id = uattr_user_id
												where uttr_is_released = 0
												and mu.user_type_id = ". UserType::MBC_CM .$user_condition .")");
       
        //$this->db->where("{$table}.{$obj_user_entity->get_table_id_name()}",$user_id );
        $query = $this->db->get();
        
        switch(strtolower($format))
        {
        	case 'array':
        		return $query->result_array();
        		break;
        	case 'dropdown':
        			$data = $query->result();
        			$data_formated = array();
        			if(!empty($default_row))
        			{
        				foreach ($default_row as $key=>$val)
        				$data_formated[$key] = $val;
        			}
        			
        			foreach ($data as $row)
        			{
        				$data_formated[$row->mb_id] = $row->bm_name;
        			}
        			return $data_formated;
        	default:
        		return $query->result(); 
        } 
 	}
	/**
 	 * 
 	 * Enter description here ...
 	 * @param unknown_type $type : All,Banks,Branches
 	 */
	public function get_designation($keyword='',$default_row=true)
 	{
 		$data = $this->get_table_dropdown('designation_master', 'dm_id', 'dm_name', '',array("cond"=>"dm_is_deleted = 0 and dm_name like '%".$keyword ."%'"),$default_row);
 		return $data;
 	}
	/**
 	 * 
 	 * Enter description here ...
 	 * @param $start
 	 * @param $end
 	 * @param $intro_text
 	 * @param $order : Asc, Desc
 	 */
 	public function get_exp_years()
 	{
 		$data = $this->get_numbers_dropdown(1,30,'Years','Asc');
 		$data['31'] ="30+";
 		return $data;
 	}
 	
 	/**
 	 * 
 	 * Enter description here ...
 	 * @param unknown_type $start
 	 * @param unknown_type $end
 	 * @param unknown_type $increament
 	 * @param unknown_type $order
 	 * @param unknown_type $extra_items : array of extra items to be inserted in the list 0 for last, 9999 for first 
 	 */
 	public function get_numbers_dropdown($start=1,$end=10,$extra_items=array("","Please select",0),$increament=1,$order='Asc')
 	{
 		
 		$data = array();
 		if(!empty($extra_items))
 		{
 			foreach ($extra_items as $item)
 			{
 				if($item[2] == 0)
 				{
 					$data[$item[0]] =  $item[1];
 				}
 			}
 		}
 		
 		if(strtolower($order)=="asc")
 		{
	 		for($i = $start ; $i<=$end ; $i=$i+$increament)
	 		{
	 			$data[$i] = $i;
	 		}
 		}
 		else
 		{
	 		for($i = $end ; $i>=$start ; $i=$i-$increament)
	 		{
	 			$data[$i] = $i;
	 		}
 		}
 		if(!empty($extra_items))
 		{
 			foreach ($extra_items as $item)
 			{
 				if($item[2] == 9999)
 				{
 					$data[$item[0]] =  $item[1];
 				}
 			}
 		}
 		return $data;
 	}
}