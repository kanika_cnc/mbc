<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 
 * Enter description here ...
 * @author CNC
 *
 */
class Banks_Bl extends Base_Model {
	
	public function get_banks_hierarchical_list_by_user($user_id)
 	{
 		$this->load->model("dl/entity/banks","obj_bank_entity");
		$table = $this->obj_bank_entity->get_table_name();
		$this->db->from("{$table}");
        $this->db->select("*,(select count(*) from mbc_can_experince where ce_bank_id = mbc_bank_master.bm_id and ce_user_id =". $user_id.") As userex");
        $this->db->where("{$table}.bm_is_deleted = 0 and " . $this->db->dbprefix.$table.  ".bm_parent_bm_id in " . "(select bm_id from ". $this->db->dbprefix.$table ." where bm_parent_bm_id is null and bm_is_deleted = 0)");
        //$in_clause = "(select bm_id from ". $this->db->dbprefix.$table ." where bm_parent_bm_id is null and bm_is_deleted = 0)";
        //$this->db->where_in("{$table}.bm_parent_bm_id",$in_clause,false); 
        $this->db->or_where("{$table}.bm_parent_bm_id is null ");
         // $this->db->order_by("bm_name");
          $query = $this->db->get();
          $data = (array)$query->result();
          $data_formatted = array();
          foreach($data as $index => $row)
          {
          	$reformat_row = array();
          	//$row = (arra$row;
          	$reformat_row["title"] = $row->bm_name;
          	$reformat_row["key"] = $row->bm_id;  
          	$reformat_row["parent_key"] = $row->bm_parent_bm_id;
          	//$reformat_row["bm_id"] = $row->bm_id;
          	$reformat_row["select"] = $row->userex == 1 ? true :false;
          	$reformat_row["expand"] = $row->userex == 1 ? true :false;
          	$data_formatted[$index] = $reformat_row;
          }
          //$tree = $this->buildTree($data_formatted,0,'bm_parent_bm_id','bm_id',false);
          return($tree);
 	}
	public function get_banks_hierarchical_list()
 	{
 		$this->load->model("dl/entity/banks","obj_bank_entity");
		$table = $this->obj_bank_entity->get_table_name();
		$this->db->from("{$table}");
        $this->db->select("*");
        
        $this->db->where("{$table}.bm_is_deleted = 0 and " . $this->db->dbprefix.$table.  ".bm_parent_bm_id in " . "(select bm_id from ". $this->db->dbprefix.$table ." where bm_parent_bm_id is null and bm_is_deleted = 0)");
        //$in_clause = "(select bm_id from ". $this->db->dbprefix.$table ." where bm_parent_bm_id is null and bm_is_deleted = 0)";
        //$this->db->where_in("{$table}.bm_parent_bm_id",$in_clause,false); 
        $this->db->or_where("{$table}.bm_parent_bm_id is null ");
         // $this->db->order_by("bm_name");
          $query = $this->db->get();
          $data = (array)$query->result();
          $data_formatted = array();
          foreach($data as $index => $row)
          {
          	$reformat_row = array();
          	//$row = (arra$row;
          	$reformat_row["title"] = $row->bm_name;
          	$reformat_row["key"] = $row->bm_id;  
          	$reformat_row["parent_key"] = $row->bm_parent_bm_id;
          	$data_formatted[$index] = $reformat_row;
          }
          
          return($data_formatted);
 	}		
	
 	/**
 	 * 
 	 * Function to fetch the branches for a given bank.
 	 * @param int $bank_id : Bank id
 	 * @param string $where_condition : condition string 
 	*/
 	public function get_branches($bank_id,$includedeleted=false)
	{
		$this->db->select('bank_master.*');
		$this->db->select('lmcity.lm_name As city');
		$this->db->select('lmstate.lm_name As state');
		$this->db->join("mbc_location_master lmcity","lmcity.lm_id =  mbc_bank_master.bm_location_id","left");
		$this->db->join("mbc_location_master lmstate","lmstate.lm_id = lmcity.lm_parent_lm_id","left");
		$this->db->where("bm_parent_bm_id",$bank_id);
		$this->db->where("bm_parent_bm_id is not null");
		if(!$includedeleted)
			$this->db->where("bm_is_deleted = 0");
		$query = $this->db->get("bank_master");
		return $query->result();
	}
}

