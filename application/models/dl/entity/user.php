<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/**
 * 
 * Enter description here ...
 * @author CNC
 *
 */
class User {
	public function __construct (){} 
	
	public $user_id = null;
	public $user_first_name ="";
  	public $user_last_name ="";
  	public $user_email="";
  	/**
  	 * to be set in the child
  	 */
  	public $user_type_id= null;
  	public $user_password_salt ="";
  	public $user_password ="";
  	public $user_last_login ="";
  	
  	public $user_created_by =0;
  	public $user_created_date = null;
  	public $user_modified_by =0;
  	public $user_modified_date =null;
  
  	public $user_contact_mobile ="";
  	public $user_is_active =0;
  	public $user_activation_date =null;
  	public $user_deactived_date =null;
  	public $user_is_deleted = 0;
  	public $user_alternate_email_id_1 ="";
	public $user_alternate_email_id_2 ="";
  	public $user_alternate_email_id_3="";
	
  	public $user_departments = array();
  	
  	public $user_profile_attributes = array();
  	public function getUserType()
  	{
  		return $this->user_type_id;	
  	}
	public function setUserType($user_type_id)
  	{
  		return $this->user_type_id=$user_type_id;	
  	}
  	public function get_user_table_name() {return  "users";}
	public function get_user_table_id_name() {return "user_id";}
	const TABLE_NAME = "users";
	const TABLE_ID_COL ="user_id";
  	//public $member_bank_id` int(11) DEFAULT NULL,
	
}





class User_Departments
{
	
	public $ud_user_id =0;
   	public $ud_department_id =0;
  	public $ud_relevant_experince =0;
  	public $ud_is_current = false;
  	public $ud_is_interested = false;
	public $dm_path = "";
	public function get_table_name() {return  "user_departments";}
	public function get_table_id_name() {return "ud_user_id";}
  
}

class User_Profile_Attributes
{
	public $uttr_id = null;
  	public $uattr_user_id;
  	public $uttr_type_id;
  	public $uttr_bank_id;
  	public $uttr_recruiter_id;
  	public $uttr_is_released =false;
  	public $uttr_created_by =0;
  	//public $uttr_created_date = null;
  	public $uttr_modified_by =0;
  	//public $uttr_modified_date =null;
  
	
  	public function get_table_name() {return  "user_profile_attributes";}
	public function get_table_id_name() {return "uttr_id";}
	const UTTR_IS_RELEASED = "uttr_is_released"; 
	const TABLE_NAME = "user_profile_attributes";
	const UTTR_USER_ID ="uattr_user_id";
	const UTTR_MODIFIED_BY = "uttr_modified_by";
	const UTTR_MODIFIED_DATE = "uttr_modified_date";
	const UTTR_BANK_ID = "uttr_bank_id";
	const UTTR_RECRUITER_ID ="uttr_recruiter_id";
}