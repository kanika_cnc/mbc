<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 * @author CNC
 *
 */
class Departments{
	const TABLE =  "department_master"; 
	public function get_table_name()
	{
		return "department_master";
	}
	public function get_table_id_name()
	{
		return "dm_id";
	}
	public function get_fields()
	{
		 $columns = array(
            0 => array(
                'name' => 'Name',
                'db_name' => 'dm_name',
                'header' => 'Name',
                'group' => 'Department',
                'required' => TRUE,
                'unique' => False,
                'form_control' => 'text_long',
                'type' => 'string'),
            1 => array(
                'name' => 'Parent',
                'db_name' => 'dm_parent_dm_id',
                'header' => 'Parent Category',
                'group' => 'Department',
                'required' => False,
                'unique' => False,
            	'ref_table_db_name' => 'department_master',
            	'ref_table_id_name'=>'dm_id',
                'ref_field_db_name' => 'dm_name',
            	'ref_field_parent_db_name' => 'dm_parent_dm_id',
            	'ref_field_parent_selectable' =>true,
                'ref_field_type' => 'int',
				'ref_filter_string' => array(),
                'form_control' => 'htree',
            	'form_control_mapped_column' => '2', //number from this array only
                'type' => '1-n'),
            2 => array( 'name' => 'Full Path',
                'db_name' => 'dm_description',
                'header' => 'Full Path',
                'group' => 'Department',
                'required' => False,
                'unique' => False,
                'form_control' => 'text_long',
                'type' => 'string'),
            3 => array(
                'name' => 'Deactive',
                'db_name' => 'dm_is_deleted',
                'header' => 'Deleted',
                'group' => 'Department',
                'form_control' => 'checkbox',
                'type' => 'boolean'),
            4 => array(
                'name' => 'created_date',
                'db_name' => 'dm_created_date',
                'header' => 'Created Date',
                'group' => 'Department',
                'form_control' => 'created_date',
                'type' => 'created_date',
            	),
           5 => array(
                'name' => 'modified_date',
                'db_name' => 'dm_modified_date',
                'header' => 'Modified Date',
                'group' => 'Department',
                'form_control' => 'read_only',
                'type' => 'updated_date'),
          6 => array(
                'name' => 'created_by',
                'db_name' => 'dm_created_by',
                'header' => 'Created By',
                'group' => 'Department',
                'form_control' => 'logged_in_by',
                'type' => 'created_by'
          		),
           7 => array(
                'name' => 'modified_by',
                'db_name' => 'dm_modified_by',
                'header' => 'Modified By',
                'group' => 'Department',
                'form_control' => 'logged_in_by',
                'type' => 'integer')  
        );
        return $columns;


		
	} 
}