<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 * @author CNC
 *
 */
class Locations extends CI_Model {

	public function get_table_name()
	{
		return "location_master";
	}
	public function get_table_id_name()
	{
		return "lm_id";
	}
	public function get_fields()
	{
		 $columns = array(
            0 => array(
                'name' => 'Name',
                'db_name' => 'lm_name',
                'header' => 'Name',
                'group' => 'Location',
                'required' => TRUE,
                'unique' => False,
                'form_control' => 'text_long',
                'type' => 'string'),
            1 => array( 'name' => 'Type',
                'db_name' => 'lm_type',
                'header' => 'Type',
                'group' => 'Location',
                'required' => True,
                'unique' => False,
            	'ref_table_db_name' => 'enum_master',
            	'ref_table_id_name'=>'em_key',
                'ref_field_db_name' => 'em_value',
                'ref_field_type' => 'string',
				'ref_filter_string' => array('cond'=>array("em_name"=>"location_type")),
                'form_control' => 'dropdown',
                'type' => '1-n'),
            2 => array(
                'name' => 'State',
                'db_name' => 'lm_parent_lm_id',
                'header' => 'State',
                'group' => 'Location',
                'required' => False,
                'unique' => False,
            	'ref_table_db_name' => 'location_master',
            	'ref_table_id_name'=>'lm_id',
                'ref_field_db_name' => 'lm_name',
                'ref_field_type' => 'string',
				'ref_filter_string' => array('cond'=>'lm_parent_lm_id is null'),
                'form_control' => 'dropdown',
                'type' => '1-n'),
            3 => array(
                'name' => 'Deactive',
                'db_name' => 'lm_is_deleted',
                'header' => 'Deleted',
                'group' => 'Location',
                'form_control' => 'checkbox',
                'type' => 'boolean'),
            4 => array(
                'name' => 'created_date',
                'db_name' => 'lm_created_date',
                'header' => 'Created Date',
                'group' => 'Location',
                'form_control' => 'created_date',
                'type' => 'created_date',
            	),
           5 => array(
                'name' => 'modified_date',
                'db_name' => 'lm_modified_date',
                'header' => 'Modified Date',
                'group' => 'Location',
                'form_control' => 'read_only',
                'type' => 'updated_date'),
          6 => array(
                'name' => 'created_by',
                'db_name' => 'lm_created_by',
                'header' => 'Created By',
                'group' => 'Location',
                'form_control' => 'logged_in_by',
                'type' => 'created_by'
          		),
           7 => array(
                'name' => 'modified_by',
                'db_name' => 'lm_modified_by',
                'header' => 'Modified By',
                'group' => 'Location',
                'form_control' => 'logged_in_by',
                'type' => 'integer')  
        );
        return $columns;
	}

	function get_location_type()
    {
        return array("" => "Select","1"=>"State","2"=>"City");
    }
}