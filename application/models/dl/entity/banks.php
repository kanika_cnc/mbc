<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 * @author CNC
 *
 */
class Banks extends CI_Model {
	
	public function get_table_name()
	{
		return "bank_master";
	}
	public function get_table_id_name()
	{
		return "bm_id";
	}
	public function get_fields()
	{
		 $columns = array(
            0 => array(
                'name' => 'Bank/Branch Name',
                'db_name' => 'bm_name',
                'header' => 'Bank/Branch Name',
                'group' => 'Bank',
                'required' => TRUE,
                'unique' => True,
                'form_control' => 'text_long',
                'type' => 'string'),
            1 => array( 'name' => 'Description',
                'db_name' => 'bm_description',
                'header' => 'Description',
                'group' => 'Bank',
                'required' => False,
                'unique' => False,
                'form_control' => 'text_long',
                'type' => 'string'),
            2 => array(
                'name' => 'Select Type of bank (in case adding the Bank) / Bank (in case you are adding a branch)',
                'db_name' => 'bm_parent_bm_id',
                'header' => 'Bank (of branch)',
                'group' => 'Bank',
                'required' => False,
                'unique' => False,
            	'ref_table_db_name' => 'bank_master',
            	'ref_table_id_name'=>'bm_id',
                'ref_field_db_name' => 'bm_name',
            	'ref_field_parent_db_name' => 'bm_parent_bm_id',
            	'ref_field_parent_selectable' =>true,
                'ref_field_type' => 'string',
				'ref_filter_string' => array('cond'=>'bm_parent_bm_id is null or (bm_parent_bm_id in (select bm_id from mbc_bank_master where bm_parent_bm_id is null) ) '),
                'form_control' => 'htree',
                'type' => '1-n'),
            3 => array(
                'name' => 'Address 1 (in case of branch)',
                'db_name' => 'bm_location_address1',
                'header' => 'Address 1',
                'group' => 'Bank',
                'required' => false,
                'unique' => True,
                'form_control' => 'text_long',
                'type' => 'string'),
            4 => array(
                'name' => 'Address 2  (in case of branch)',
                'db_name' => 'bm_location_address2',
                'header' => 'Address 2',
                'group' => 'Bank',
                'required' => false,
                'unique' => True,
                'form_control' => 'text_long',
                'type' => 'string'), 
           5 => array(
                'name' => 'Select Location for the branch',
                'db_name' => 'bm_location_id',
                'header' => 'Location',
                'group' => 'Bank',
                'required' => False,
                'unique' => False,
            	'ref_table_db_name' => 'location_master',
            	'ref_table_id_name'=>'lm_id',
                'ref_field_db_name' =>'lm_name',
				'ref_field_parent_db_name' => 'lm_parent_lm_id',
            	'ref_field_parent_selectable' =>false,           
                'ref_field_type' => 'string',
				'ref_filter_string' => array(),
                'form_control' => 'htree',
                'type' => '1-n'),//Put address1 and address2
           6 => array( 'name' => 'Phone',
                'db_name' => 'bm_contact_phone',
                'header' => 'Phone',
                'group' => 'Bank',
                'required' => False,
                'unique' => False,
                'form_control' => 'text_long',
                'type' => 'string'),  
           7 => array(
                'name' => 'Deactive',
                'db_name' => 'bm_is_deleted',
                'header' => 'Deleted',
                'group' => 'Bank',
                'form_control' => 'checkbox',
                'type' => 'boolean'),
            8 => array(
                'name' => 'created_date',
                'db_name' => 'bm_created_date',
                'header' => 'Created Date',
                'group' => 'Bank',
                'form_control' => 'created_date',
                'type' => 'created_date',
            	),
           9 => array(
                'name' => 'modified_date',
                'db_name' => 'bm_modified_date',
                'header' => 'Modified Date',
                'group' => 'Bank',
                'form_control' => 'read_only',
                'type' => 'updated_date'),
          10 => array(
                'name' => 'created_by',
                'db_name' => 'bm_created_by',
                'header' => 'Created By',
                'group' => 'Bank',
                'form_control' => 'logged_in_by',
                'type' => 'created_by'
          		),
          11 => array(
                'name' => 'modified_by',
                'db_name' => 'bm_modified_by',
                'header' => 'Modified By',
                'group' => 'Bank',
                'form_control' => 'logged_in_by',
                'type' => 'integer'),
        );
        return $columns;


		
	} 
}