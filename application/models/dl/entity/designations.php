<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 * @author CNC
 *
 */
class Designations extends CI_Model {

	public function get_fields()
	{
		 $columns = array(
            0 => array(
                'name' => 'Name',
                'db_name' => 'dm_name',
                'header' => 'Name',
                'group' => 'Designation',
                'required' => TRUE,
                'unique' => False,
                'form_control' => 'text_long',
                'type' => 'string'),
            1 => array( 'name' => 'Description',
                'db_name' => 'dm_description',
                'header' => 'Description',
                'group' => 'Designation',
                'required' => TRUE,
                'unique' => False,
                'form_control' => 'text_long',
                'type' => 'string'),
           /* For later use when designations to be associted with banks
            * 2 => array(
                'name' => 'Parent',
                'db_name' => 'dm_parent_dm_id',
                'header' => 'Parent Category',
                'group' => 'Designation',
                'required' => False,
                'unique' => False,
            	'ref_table_db_name' => 'designation_master',
            	'ref_table_id_name'=>'dm_id',
                'ref_field_db_name' => 'dm_name',
                'ref_field_type' => 'string',
				'ref_filter_string' => array(),
                'form_control' => 'dropdown',
                'type' => '1-n',
            	'visible'=>false),*/
            3 => array(
                'name' => 'Deactive',
                'db_name' => 'dm_is_deleted',
                'header' => 'Deleted',
                'group' => 'Designation',
                'form_control' => 'checkbox',
                'type' => 'boolean'),
            4 => array(
                'name' => 'created_date',
                'db_name' => 'dm_created_date',
                'header' => 'Created Date',
                'group' => 'Designation',
                'form_control' => 'created_date',
                'type' => 'created_date',
            	),
           5 => array(
                'name' => 'modified_date',
                'db_name' => 'dm_modified_date',
                'header' => 'Modified Date',
                'group' => 'Designation',
                'form_control' => 'read_only',
                'type' => 'updated_date'),
          6 => array(
                'name' => 'created_by',
                'db_name' => 'dm_created_by',
                'header' => 'Created By',
                'group' => 'Designation',
                'form_control' => 'logged_in_by',
                'type' => 'created_by'
          		),
           7 => array(
                'name' => 'modified_by',
                'db_name' => 'dm_modified_by',
                'header' => 'Modified By',
                'group' => 'Designation',
                'form_control' => 'logged_in_by',
                'type' => 'integer')  
        );
        return $columns;


		
	} 
}