<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
include_once( APPPATH . 'models/dl/entity/user' . EXT );
/**
 * 
 * Enter description here ...
 * @author CNC
 *
 */
class Candidate extends User{
	
 	public function __construct () 
    {
     	 parent::__construct();
     	 $this->user_type_id  = UserType::CANDIDATE;
    }
    
    public $can_user_id = null;
  	public $can_code =	"";
  	public $can_photo =	"";
  	public $can_dob =	"";
  	public $can_age =0;
  	public $can_gender ="";
  	public $can_marital_status ="";
  	public $can_ctc ="0.0";
  	public $can_ctc_notes ="";
  	public $can_total_experience = "";
  	public $can_resume_file = "";
  	public $can_is_verified = 0;
  	public $can_verified_by = 0;
  	public $can_status_id =0;
  	public $can_verified_date ="";
  	public $can_created_by =0;
  	public $can_created_date ="";
  	public $can_modified_date = "";
  	public $can_modified_by = 0;
  	public $can_band ="";
  	public $can_curr_experince = array();
  	public $can_prev_experince = array();
  	public $can_qualifications = array();
  	public $can_grad_qualifications = "";
  	public $can_postgrad_qualifications = "";
  	public $can_certifications ="";
  	public $can_designation_id ="";
  	public $can_location_id ="";
  	public $can_prefernces = array();
  	public $can_type_id ;
  	public $can_location_name ="";
  	public $can_current_branch_id ="";
  	public $can_resume_blob = "";
  	public function get_table_name(){return "candidate";}
	public function get_table_id_name(){return "can_user_id";}
}
class Candidate_Resume_Blob
{
	public function __construct (){}
	public $cr_id;
	public $cr_user_id;
	public $cr_resume="";
	public function get_table_name(){return "mbc_can_resume";}
	public function get_table_id_name(){return "cr_id";}
}
class Candidate_Experince
{
	public function __construct (){}
	public $ce_id;
	public $ce_user_id;
	public $ce_bank_id;
	public $ce_is_current_bank;
	public $ce_bank_experince_period ="";
	public $ce_other_bank_name ="";
	public function get_table_name(){return "can_experince";}
	public function get_table_id_name(){return "ce_user_id";}
}

class Candidate_Qualifications
{
	public function __construct (){}
	public $cq_qualification_master_id;
	public $cq_user_id;
	public $cq_user_certification_name;
	public $cq_qualification_type_id;
	public function get_table_name(){return "can_qualifications";}
	public function get_table_id_name(){return "cq_user_id";}
}
class Qulification_TYPE
{
	const GRADUATION = 1;
	const POSTGRADUATION = 2;
	const CERTIFICATION = 3;
}

class Candidate_Preferences
{
	public function __construct (){}
	public $cp_location;
	public $cp_bank_id;
	public $cp_user_id;
	public function get_table_name(){return "can_preferences";}
	public function get_table_id_name(){return "cp_user_id";}
}





