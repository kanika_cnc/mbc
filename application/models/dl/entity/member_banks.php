<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 * @author CNC
 *
 */
class Member_Banks extends CI_Model {

	public function get_table_name() {return  "member_bank";}
	public function get_table_id_name() {return "mb_id";}
	
	public function get_fields()
	{
		 $columns = array(
            0 => array(
                'name' => 'Bank Name',
                'db_name' => 'mb_master_bank_id',
                'header' => 'Bank Name',
                'group' => 'MemberBank',
                'required' => TRUE,
                'unique' => True,
                'ref_table_db_name' => 'bank_master',
            	'ref_table_id_name'=>'bm_id',
                'ref_field_db_name' => 'bm_name',
                'ref_field_type' => 'string',
				'ref_filter_string' => array('cond'=>'bm_parent_bm_id is not null and bm_parent_bm_id in (select bm_id from mbc_bank_master where bm_parent_bm_id is null)'),
                'form_control' => 'dropdown',
                'type' => '1-n'),
            1 => array( 'name' => 'Description',
                'db_name' => 'mb_description',
                'header' => 'Description',
                'group' => 'MemberBank',
                'required' => False,
                'unique' => False,
                'form_control' => 'text_long',
                'type' => 'string'),
            2 => array(
                'name' => 'Face Email ID',
                'db_name' => 'mb_primary_email',
                'header' => 'Face Email ID',
                'group' => 'MemberBank',
                'required' => False,
                'unique' => False,
            	'form_control' => 'text_long',
                'type' => 'string'),
            3 => array( 'name' => 'Logo',
                'db_name' => 'mb_logo',
                'header' => 'Logo',
                'group' => 'MemberBank',
                'required' => False,
                'unique' => False,
            	'upload_path'=> './uploads/bank_logos',
            	'upload_path_temp' => './uploads/bank_logos/temp',
                'form_control' => 'file',
                'type' => 'file'),  
            4 => array(
                'name' => 'Deactive',
                'db_name' => 'mb_is_deleted',
                'header' => 'Deleted',
                'group' => 'MemberBank',
                'form_control' => 'checkbox',
                'type' => 'boolean'),
            5 => array(
                'name' => 'created_date',
                'db_name' => 'mb_created_date',
                'header' => 'Created Date',
                'group' => 'MemberBank',
                'form_control' => 'created_date',
                'type' => 'created_date',
            	),
           6 => array(
                'name' => 'modified_date',
                'db_name' => 'mb_modified_date',
                'header' => 'Modified Date',
                'group' => 'MemberBank',
                'form_control' => 'read_only',
                'type' => 'updated_date'),
          7 => array(
                'name' => 'created_by',
                'db_name' => 'mb_created_by',
                'header' => 'Created By',
                'group' => 'MemberBank',
                'form_control' => 'logged_in_by',
                'type' => 'created_by'
          		),
           8 => array(
                'name' => 'modified_by',
                'db_name' => 'mb_modified_by',
                'header' => 'Modified By',
                'group' => 'MemberBank',
                'form_control' => 'logged_in_by',
                'type' => 'integer'),
        );
        return $columns;


		
	} 
}