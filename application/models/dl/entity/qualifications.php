<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 * @author CNC
 *
 */
class Qualifications extends CI_Model {
	const GRADUATION = 1;
	const POSTGRADUATION = 18;
	public function get_fields()
	{
		 $columns = array(
            0 => array(
                'name' => 'Name',
                'db_name' => 'qm_name',
                'header' => 'Name',
                'group' => 'Qualification',
                'required' => TRUE,
                'unique' => FALSE,
                'form_control' => 'text_long',
                'type' => 'string'),
            1 => array(
                'name' => 'Parent',
                'db_name' => 'qm_parent_qm_id',
                'header' => 'Parent Category',
                'group' => 'Qualification',
                'required' => False,
                'unique' => False,
            	'ref_table_db_name' => 'qualification_master',
            	'ref_table_id_name'=>'qm_id',
                'ref_field_db_name' => 'qm_name',
            	'ref_field_parent_db_name' => 'qm_parent_qm_id',
            	'ref_field_parent_selectable' =>true,
                'ref_field_type' => 'string',
				'ref_filter_string' => array(),
                'form_control' => 'htree',
                'type' => '1-n'),
            2 => array(
                'name' => 'Deactive',
                'db_name' => 'qm_is_deleted',
                'header' => 'Deleted',
                'group' => 'Qualification',
                'form_control' => 'checkbox',
                'type' => 'boolean'),
            3 => array(
                'name' => 'created_date',
                'db_name' => 'qm_created_date',
                'header' => 'Created Date',
                'group' => 'Qualification',
                'form_control' => 'created_date',
                'type' => 'created_date',
            	),
           4 => array(
                'name' => 'modified_date',
                'db_name' => 'qm_modified_date',
                'header' => 'Modified Date',
                'group' => 'Qualification',
                'form_control' => 'read_only',
                'type' => 'updated_date'),
          5 => array(
                'name' => 'created_by',
                'db_name' => 'qm_created_by',
                'header' => 'Created By',
                'group' => 'Qualification',
                'form_control' => 'logged_in_by',
                'type' => 'created_by'
          		),
           6 => array(
                'name' => 'modified_by',
                'db_name' => 'qm_modified_by',
                'header' => 'Modified By',
                'group' => 'Qualification',
                'form_control' => 'logged_in_by',
                'type' => 'integer')  
        );
        return $columns;
	} 
}