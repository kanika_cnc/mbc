<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/**
 * 
 * This is a base controller for all logged in users.
 * The controller will do basic authorization for page access.
 * @author Fintotal
 *
 */
class MBC_Controller extends CI_Controller {

 	 /**
     * @access public
     */
    public function __construct () 
    {
    	parent::__construct();
     /*
      * Checks the user objects
      * load the user seesion 
      * check the type of user
      *Later :  auto set the master page based upon the type of user
      * set the header and footer dynamic content based upon the user
      * */  
    	$this->checkPageAccess();
    }
    public function get_loggedin_user_id()
    {
    	$logged_in = $this->session->userdata('logged_in');
    	if(!empty($logged_in))
    	{
    		return $logged_in["user_id"];
    	}
    	return -1;
    }
	public function get_loggedin_user()
    {
    	return $this->session->userdata('logged_in');
    	
    }
    public function is_cm()
    {
    	$logged_in = $this->session->userdata('logged_in');
    	if(!empty($logged_in))
    	{
    		if($logged_in["user_type_id"] ==  UserType::MBC_CM)
    		{
    			return true;
    		}
    	}
    	return false;
    }
	public function is_admin()
    {
    	$logged_in = $this->session->userdata('logged_in');
    	if(!empty($logged_in))
    	{
    		if($logged_in["user_type_id"] ==  UserType::MBC_ADMIN)
    		{
    			return true;
    		}
    	}
    	return false;
    }
	public function is_recruiter()
    {
    	$logged_in = $this->session->userdata('logged_in');
    	if(!empty($logged_in))
    	{
    		if($logged_in["user_type_id"] ==  UserType::MBC_Recruiter)
    		{
    			return true;
    		}
    	}
    	return false;
    }
	public function checkPageAccess()
	{
		$logged_in = $this->session->userdata('logged_in');
		if(empty($logged_in))
		{
			redirect('home', 'refresh');
		} 
		else
		{
			$logged_in = (object)$logged_in;
			$access_directory =  strtolower($this->router->fetch_directory());
			switch($logged_in->user_type_id)
			{
				case UserType::MBC_CM :
				case UserType::MBC_Recruiter :	
				case UserType::MBC_ADMIN:
					if(($access_directory != "mbc/") && ($access_directory != "base/") && ($access_directory != "carbo/")  )
						$this->mbc_auth->process_redirection($logged_in);
					break;
				case UserType::CANDIDATE:
					if(($access_directory != "candidates/" ) && ($access_directory != "base/") && ($access_directory != "carbo/"))
						$this->mbc_auth->process_redirection($logged_in);
				break;
				case UserType::BANK:
					if(($access_directory != "banks/")  && ($access_directory != "base/") && ($access_directory != "carbo/"))
						$this->mbc_auth->process_redirection($logged_in);
				break;
				default:
					//redirect('home', 'refresh');
			}
		}
	}
		
}

