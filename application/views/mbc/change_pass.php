<?php
//$objmasters =(object)$masters;
////$bank_list = $objmasters->bank;
//$department_list = $objmasters->departments;
////$banks_hlist = $objmasters->hbanks;
//$is_admin = $loggin_user["user_type_id"] == UserType::MBC_ADMIN ? true:FALSE;
?>

<div class="box">
  <div class="box-search">
            <h5>Change Your Password</h5>
            </div>  <!--end box-search-->
<?php
	
 	$attributes = array('id' => 'change_password');
    echo form_open('mbc/user_management/change_password', $attributes); ?>
  					<span style="color:red">
  					<?php echo validation_errors(); ?>
  					<?php  echo $error;?></span>
  					
                    <table width="80%" border="0" cellspacing="0" cellpadding="5">
                        <tr>
                            <td>
                                <?php echo form_label("Old Password<span style='color:red'>*</span>","old_password")?>
                            </td>
                            <td>
                               <?php echo form_password('old_password', "", 'id="user_old_password"  class="" placeholder="Old Password"') ;?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <?php echo form_label("New Password<span style='color:red'>*</span>","new_password")?>
                            </td>
                            <td>
                               <?php 
									echo form_password('new_password', '', 'id="new_password" class="" placeholder="New Password"'); 
                               	?>

                            </td>
                        </tr>
                        <tr>
                            <td>
                                <?php echo form_label("New Password Again<span style='color:red'>*</span>","new_password_r")?>
                            </td>
                            <td>
                               <?php 
	                               		 echo form_password('new_password_r', "", 'id="new_password_r" class="" placeholder="New Password Again"');?> 
                            </td>
                        </tr>
                       
						</table>
				<div class="box-2">
                    <center>
                        <?php  echo form_submit("submit","Change Password","class='submit'")?>
						<?php //echo form_hidden("user_id",$form_data->user_id);?>
                     </center>
                </div>
               

	      
         <?php echo form_close();?>   
				            </div><!--end box-->
<div style="clear:both"></div>
