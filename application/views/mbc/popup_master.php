<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="<?php echo asset_url();?>/styles/style.css" rel="stylesheet" type="text/css" />
<link href="<?php echo asset_url();?>/styles/msgBoxLight.css" type="text/css" rel="stylesheet" />
<script type='text/javascript' src='http://code.jquery.com/jquery-1.9.1.min.js'></script>
<script type='text/javascript' src='<?php echo asset_url();?>/scripts/jquery.validate.min.js'></script>
<link rel="stylesheet" href="<?php echo asset_url(); ?>css/carbo/jquery-ui-1.8.16.custom.css" type="text/css" media="all" />
<link href="<?php echo asset_url();?>css/carbo.grid.css" rel="stylesheet" type="text/css" media="all" />
<!--[if lt IE 7]><link href="<?php echo asset_url(); ?>css/carbo.grid.ie6.css" rel="stylesheet" type="text/css" media="all" /><![endif]-->
<link href="<?php echo asset_url();?>css/carbo.form.css" rel="stylesheet" type="text/css" media="all" />
<link href="<?php echo asset_url();?>css/960.css" rel="stylesheet" type="text/css" />
<link href="<?php echo asset_url();?>css/style.css" rel="stylesheet" type="text/css" />
<title><mp:Title/> | Mybankconnect.com</title>
</head>

<body>
<div id="wrapper">
	<div id="main">	
			<mp:Content/>
		</div><!--end right-box-->
    <div class="clear"></div>
</div><!--end wrapper-->
</body>
</html>
