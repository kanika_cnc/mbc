<?php
$objmasters =(object)$masters;
//$bank_list = $objmasters->bank;
$department_list = $objmasters->departments;
//$banks_hlist = $objmasters->hbanks;
$is_admin = $loggin_user["user_type_id"] == UserType::MBC_ADMIN ? true:FALSE;
?>
<script>
var vis_admin =  <?php echo $is_admin ?1:0;?>;
set_view();
function set_view()
{
	if(vis_admin == 0)
	{
		$("#user_is_cm").attr('disabled','disabled');
	}
}
$(document).ready(function(){
$("#deleted_table").hide();	
<?php 
if(empty($form_data->user_email) && !validation_errors())
{ ?>
$("#create_table").hide();
$("#create").find("span").html("&#x25B6;");
$("#btnAddCmDetails").attr('disabled','disabled');
$("#div_cm_details").hide();

<?php } else { ?>

$("#view_table").hide();
$("#view").find("span").html("&#x25B6;");
$("#create").find("span").html("&#x25BC;");
<?php if(!$form_data->is_cm) { ?>
$("#btnAddCmDetails").attr('disabled','disabled');
$("#div_cm_details").hide();
<?php } ?>
<?php } ?>

	$("#create").click(function(){
		$("#create_table").show(500);
		$("#deleted_table").hide();	
		$("#view_table").hide();
		$("#view").find("span").html("&#x25B6;");
		$("#deleted").find("span").html("&#x25B6;");
		$(this).find("span").html("&#x25BC;");
		
	});
	$("#view").click(function(){
		$("#create_table").hide();
		$("#view_table").show(500);	
		$("#deleted_table").hide();	
		$("#create").find("span").html("&#x25B6;");
		$("#deleted").find("span").html("&#x25B6;");
		$(this).find("span").html("&#x25BC;");
	});
	$("#deleted").click(function(){
		$("#create_table").hide();
		$("#view_table").hide();
		$("#deleted_table").show(500);	
		$("#create").find("span").html("&#x25B6;");
		$("#view").find("span").html("&#x25B6;");
		$(this).find("span").html("&#x25BC;");
	});
    $("#user_is_cm").change(function(){
        if($(this).is(':checked'))
        {
        	$("#btnAddCmDetails").removeAttr('disabled'); 
        	$("#div_cm_details").show();
        }
        else
        {
        	$("#btnAddCmDetails").attr('disabled','disabled');
        	$("#div_cm_details").hide();
        	
        }
    });
    $("#btnAddCmDetails").click(function(){
    		var table=document.getElementById("tr_cm_details");
    		$('#tr_cm_details tr:last').clone().find("select").val('').end().find("input").val('').end().insertAfter('#tr_cm_details tr:last');
    		 
    });
    /*$("#tr_cm_details").on(".removebtn", "click", function () {
        $(this).closest('.tr').remove();
    });*/
    $("#tr_cm_details").on('click', ".removebtn", function () {
    	var rowCount = $('#tr_cm_details tr').length;
    	 if (rowCount > 2) {
        	$(this).closest('tr').remove();
    	 }
    	 else
    	 {
        	 alert("There must be one bank and recruiter asscosiated with CM");
    	 }
    });
});
</script>
<div class="box">
  <div class="box-search">
            <h5 id="create" style="cursor:pointer"><span> &#x25B6; </span>&nbsp;&nbsp;&nbsp;Create MBC Recruiter/CM</h5>
            </div>  <!--end box-search-->
<?php
	$form =(object)$params;
 	$attributes = array('id' => 'edit_mbc_user');
    echo form_open($form->url, $attributes); ?>
  		
  					<?php echo validation_errors(); ?>
  					<?php echo $errors;?>
  					
                    <table width="70%" border="0" cellspacing="0" cellpadding="5" id="create_table">
                        <tr>
                            <td width="120">
                                <?php echo form_label("Name<span style='color:red'>*</span>","user_first_name")?>
                            </td>
                            <td width="290">
                               <?php echo form_input('user_first_name', $form_data->user_first_name, 'id="user_first_name"  class="" placeholder="First Name"') . "&nbsp;-&nbsp;" . form_input('user_last_name', $form_data->user_last_name, 'id="user_last_name" class="" placeholder="Last Name"');?>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <?php echo form_label("Email<span style='color:red'>*</span>","user_email")?>
                            </td>
                            <td>
                               <?php 
									echo form_input('user_email', $form_data->user_email, 'id="user_email" class=""'); 
                               	?>

                            </td>
                        </tr>
                        <tr>
                            <td>
                                <?php echo form_label("Contact Number<span style='color:red'>*</span>","user_contact_mobile")?>
                            </td>
                            <td>
                               <?php 
	                               		 echo form_input('user_contact_mobile', $form_data->user_contact_mobile, 'id="user_contact_mobile" class=""');?> 
                            </td>
                        </tr>
                        <tr>
                        	<td>
                        		Select departments <?php if($form_data->user_type_id == 10) { ?><br /> <span style="color:#660000">(Not editable even if selected) </span> <?php } ?>
                        	</td>
                        	<td>
                        		<?php
									
									echo form_hcheckboxTree("user_departments",$department_list,$form_data->user_departments); 
								?>
                        	</td>
                        	</tr>
                        <?php if($is_admin){?> 	
                        <tr>
                            <td>
                                <?php echo form_label("Is CM","user_is_cm")?>
                            </td>
                            <td>
                               <?php 
	                              echo form_checkbox('user_is_cm',1,$form_data->is_cm ," id='user_is_cm'  class='' ");?> 
                            </td>
                        </tr>
                        <?php } else {?>
                        	<td>
                                <?php echo form_label("Details","user_is_cm")?>
                            </td>
                            <td>
                            	<?php 
	                              echo form_checkbox('user_is_cm',1,$form_data->is_cm ," id='user_is_cm'  class='' style='display:none' ");?>
                            </td>
                        <?php }?>
                         <tr>
                            <td>
                               
                            </td>
                            <td>
                               		<?php 
                               				if($is_admin)
                               				{
                        						echo form_button("btnAddCmDetails","Add Bank and Recruiter",'id="btnAddCmDetails"');
                               				}
                        			?>
                        		 
                            </td>
                        </tr>
                        <tr>
                        	<td>
                        	</td>
                        	<td>
                        		<div id="div_cm_details">
                        		<table id="tr_cm_details" width="100%" border="1" cellspacing="0" cellpadding="5">
                        			<tr>
			                        	<td>
			                        		<?php echo form_label("Select Member Bank","member_bank")?>
			                        	</td>
			                        	
			                        	<td>
			                        		<?php echo form_label("Select Recruitrers","recruiters")?>
			                        	</td>
			                        	
			                        		<?php
			                        		if($is_admin)
			                        		{ 
			                        			echo "<td>"; 
			                        			echo form_label("Action");
			                        			echo "</td>"; 
			                        		}?>
			                        	
			                        </tr>
			                        <?php 
			                        
			                        	$temp = (array)$form_data->user_profile_attributes;
			                        	if(!empty($temp))
			                        		{
			                        			foreach ($form_data->user_profile_attributes as $attr)
			                        			{ 
			                        ?>
			                        <tr>
			                        	<td>
			                        		<?php echo form_hidden("user_profile_attributes[uttr_id][]",$attr->uttr_id);?>
			                        		<?php
			                        		
			                        			$disable = $is_admin?"":"disabled";  
			                        			echo form_dropdown("user_profile_attributes[uttr_bank_id][]", $objmasters->member_banks, ($attr->uttr_bank_id),'id="member_bank" '. $disable   .' class=""');
			                        		?>
			                        	</td>
			                        	<td>
			                        		<?php echo form_dropdown("user_profile_attributes[uttr_recruiter_id][]", $objmasters->recruiters,($attr->uttr_recruiter_id), 'id="recruiters" '. $disable   .'  class="" ');
											$atts = array(
												  'width'      => '800',
												  'height'     => '600',
												  'scrollbars' => 'yes',
												  'status'     => 'yes',
												  'resizable'  => 'yes',
												  'screenx'    => '0',
												  'screeny'    => '0'
												);
												echo anchor_popup('mbc/user_management/view_user_details/'.$attr->uttr_recruiter_id, 'View Recruter Details', $atts);
											
											?>
											
			                        	</td>
			                        	
			                        		<?php
			                        			if($is_admin)
			                        			{ 
			                        				echo "<td>"; 	
			                        				echo form_button("btnRemove[]","Release","id='btnRemove' class='removebtn'");
			                        				echo "</td>"; 
			                        			}?>
			                        			
			                        	
			                        </tr>
			                        <?php } }
			                        if($is_admin && empty($temp))
			                        {
			                        ?>
			                        	<tr>
			                        	<td>
			                        		<?php echo form_hidden("user_profile_attributes[uttr_id][]");?>
			                        		<?php echo form_dropdown("user_profile_attributes[uttr_bank_id][]", $objmasters->member_banks, "",'id="member_bank" class=""');?>
			                        	</td>
			                        	<td>
			                        		<?php echo form_dropdown("user_profile_attributes[uttr_recruiter_id][]", $objmasters->recruiters,"" ,'id="recruiters" class=""');?>
			                        	</td>
			                        	
			                        		<?php
			                        			if($is_admin)
			                        			{
			                        				echo "<td>"; 
			                        				echo form_button("btnRemove[]","Release","id='btnRemove' class='removebtn'");
			                        				echo "</td>";
			                        			}
			                        		?>
			                        	
			                        </tr>
									<?php }?>
                        		</table>
                        		</div>
                        	</td>
                        </tr>
                        
						<tr>
						<td colspan="2">
				<div class="box-2">
                    <center>
                        <?php  echo form_submit("submit","Save","class='submit'")?>
                        <?php  echo form_button("btnCancel","Cancel","class='submit' onclick='Cancel_click()'")?>
                     </center>
                </div>
               </td>
			   </tr>
			   </table>
<?php 
if($is_admin)
{ 
?>
	<div class="box-search">
	<h5 id="view" style="cursor:pointer"><span> &#x25BC; </span>&nbsp;&nbsp;&nbsp;View MBC Recruiter/CM</h5>
	</div>
	<table width="100%" border="0" cellspacing="0" cellpadding="5" id="view_table" >
		<tr> <td colspan="2" style="color:red">  </td> </tr>
		<?php echo form_hidden("user_id",$form_data->user_id); ?>
		<?php
		foreach($users as $user) 
		{
			if($user->user_is_deleted == 0)
			{
			?>
				<tr class="<?php echo $user->user_type_id==10?"blue":"orange";?>" <?php if($user->user_is_deleted == 1) { ?> style="background:#ffa4a4;" <?php } ?>>
				<td width="26"><img src="<?php echo asset_url();?>images/small_user.png" width="22" alt=" " /></td>
				<td width="142"><strong><?php
				if($user->user_is_deleted == 1)
				echo "<strike>";
				echo $user->user_first_name . $user->user_last_name;
				if($user->user_is_deleted == 1)
				echo "</strike>";
				?></strong></td>
				<td width="188"></td>
				<td width="75"><?php echo $user->user_type_id==10?"Recruiter":"CM";?></td>
				<?php if($is_admin){?>
					<td width="131" align="center"><?php 
					if($user->user_is_deleted == 0)
					echo form_submit("submit","Edit","class='submit' onclick='setID(".$user->user_id.")'")?></a></td>
					<td width="55" align="center">
					<?php  
					if($user->user_is_deleted == 0)
					echo form_submit("submit","Delete","class='submit' onclick='deleteUser(".$user->user_id.")'"); 
					else
					echo form_submit("submit","Restore","class='submit' onclick='restoreUser(".$user->user_id.")'"); 
					?></td>
				<?php } ?>
				</tr>
				<?php 
			} // end if($user->user_is_deleted == 0)
		} // end foreach
	?>
	</table>
	<?php
		if(!empty($deleted_users))
		{
	 ?>		
	 		<div class="box-search">
	 		<h5 id="deleted" style="cursor:pointer"><span> &#x25B6; </span>&nbsp;&nbsp;&nbsp;Deleted MBC Recruiter/CM</h5>
			</div>
			<table width="100%" border="0" cellspacing="0" cellpadding="5" id="deleted_table" >
			<tr> <td colspan="2" style="color:red">  </td> </tr>
			<?php
				foreach($deleted_users as $user)
				{
			?>
				<tr class="<?php echo $user->user_type_id==10?"blue":"orange";?>" <?php if($user->user_is_deleted == 1) { ?> style="background:#ffa4a4;" <?php } ?>>
                <td width="26"><img src="<?php echo asset_url();?>images/small_user.png" width="22" alt=" " /></td>
                <td width="142"><strong><?php
				if($user->user_is_deleted == 1)
				echo "<strike>";
				 echo $user->user_first_name . $user->user_last_name;
				 if($user->user_is_deleted == 1)
				echo "</strike>";
				 ?></strong></td>
                <td width="188"></td>
                <td width="75"><?php echo $user->user_type_id==10?"Recruiter":"CM";?></td>
                <?php if($is_admin){?>
					<td width="131" align="center"><?php 
					if($user->user_is_deleted == 0)
					 echo form_submit("submit","Edit","class='submit' onclick='setID(".$user->user_id.")'")?></a></td>
					<td width="55" align="center">
					<?php  
					if($user->user_is_deleted == 0)
					echo form_submit("submit","Delete","class='submit' onclick='deleteUser(".$user->user_id.")'"); 
					else
					echo form_submit("submit","Restore","class='submit' onclick='restoreUser(".$user->user_id.")'"); 
					?></td>
                <?php	} ?>
              </tr>
			<?php	} // end foreach
			?>
			</table>
			   <?php } }?>
			  
            <br />
         <?php echo form_close();?>   
				            </div><!--end box-->
<div style="clear:both"></div>

<script>
function setID(id)
{
	
		$("[name='user_id']").val(id);
}
function deleteUser(id)
{
	
	if(confirm('Are you sure you want to delete this user. This user might be handling some departments or banks.Please release them and reallocate to other users else they will be released automatically.'))
	{
		$("[name='user_id']").val(id);
	}
	else
	{
		return false;
	}
	
}

function restoreUser(id)
{
	if(confirm('Are you sure you want to restore this user? Departments of the users won\'t be restored. You will have to manually assign departments again.'))
	{
		$("[name='user_id']").val(id);
	}
	else
	{
		return false;
	}
}

	function Cancel_click()
	{
		window.location.href = '<?php echo base_url(). 'mbc/user_management/mbc_user'?>';
		return false;
	}


</script>