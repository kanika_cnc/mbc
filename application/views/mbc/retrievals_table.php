<?php
$objmasters =(object)$masters;
$form_data = (object)$data;
 ?>

<tr><th width="80%" class="table-heading"><span style="float:left"><select class="heading-select" >
					<option> Departments </option>
                    </select></span><span style="float:right"><select class="heading-select">
					<option value="0">All</option>
					<?php foreach ($objmasters->departments as $dept) {
					if(is_null($dept->parent_key)) { ?>
                            	<option value="<?php echo $dept->key ?>"><?php echo $dept->title ?></option>
					<?php } } ?>
                    </select></span></th><th>Count</th></tr>
					<?php
					  from_tree_grid($objmasters->departments_tree);   
					?>
                    
					
 <script type="text/javascript">
 	$(document).ready(function(){
		$(".total-count").on('click', function() {
			
			var id = $(this).attr('id');
			//alert($("#dd_banks_dd").val());
			$.ajax({
					type: "POST",
					url: "<?php echo base_url() ?>mbc/retrievals/",
					data: { departments: id, designations: $("#dd_designations_dd").val(), banks:$("#dd_banks_dd").val(), candidates:'submit'}
					}).done(function(msg) {
					//$(".search-details").hide();  Uncomment this line if the previously  opened candidate teble is to be closed.
					$("#row_"+id).next(".search-details").hide();
					$("#row_"+id).after(msg);
			});
		});
	});
 </script>					
