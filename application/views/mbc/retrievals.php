<?php
$content['data'] = $data;
$content['masters'] = $masters;
$objmasters =(object)$masters;
$form_data = (object)$data;
//debug($objmasters->departments)
?>
<ul class="tab-menu ">
                	<li><a href="<?php echo base_url(); ?>mbc/search">Search</a></li>
                    <li><a href="<?php echo base_url(); ?>mbc/retrievals" class="last active">Visual</a></li>
                </ul>
                
            <div class="tab-box">
			<div class="box-search-filter">
                	<div class="box-filter">
                        <ul>
                        	<li>
                            <select class="filter-select unselected" id="sel_one">
								<option> Filter By </option>
                            	<option value="dd_banks">Banks</option>
								<option value="dd_designations">Designation</option>
                    </select></li>
                            <li>
                            <select class="filter-select unselected" id="sel_two">
							<option> Filter By </option>
                            	<option value="dd_banks">Banks</option>
								<option value="dd_designations">Designation</option>
                    </select></li>
                            <!--<li>
                            <select class="filter-select unselected">
                            	<option>Select</option>
                          </select></li>-->
                        </ul>
                    </div>
                    <div class="box-filter-fields" id="filter_level_2">
 						<div class="sec_dd hidden" id="dd_banks">
							Select Bank:
							<?php  echo form_dropdown('banks',  $objmasters->bank,$form_data->banks ,'id="dd_banks_dd"');?>
							<input type="button" name="set" id="set_bank" value="Set" class="aja_button set" />
							<input type="button" name="UnSet" id="remove_banks" value="UnSet" class="aja_button unset" />
						</div>
						<div class="sec_dd hidden" id="dd_designations">
							Select Designation :
							<?php  echo form_dropdown('designations',  $objmasters->designations,$form_data->designations ,'id="dd_designations_dd"');?>
							<input type="button" name="set" value="Set" id="set_designation" class="aja_button set" />
							<input type="button" name="UnSet" id="remove_designation" value="UnSet" class="aja_button unset" />
						</div>
                  </div>
                    <div class="clear"></div>
                </div><!--end box-search-->
				<div class="box-search-result">
					<table width="100%" border="0" cellpadding="5" cellspacing="0" class="result-table" id="result_table">
                    <?php $this->load->view('mbc/retrievals_table',$content); ?>
                    </table>
                </div>
            </div><!--end tab-box-->
<script type="text/javascript">
	$(document).ready(function(){
		$(".hidden").hide();
		$(".filter-select").change(function(){
			var val = $(this).val();
			var id = $(this).attr('id');
			$(this).attr('disabled','disabled');
			$("#"+val).show();
			$("#"+val).append('<input type="hidden" value="'+id+'" class="parent_filter" />')
		});
		$(".aja_button").click(function(){
			if($(this).hasClass('unset'))
			{
				$(this).parent().find('select').val('0');
				
				$(this).parent().hide();
				var parent_filter_id = $(this).parent().find(".parent_filter").val();
				$("#"+parent_filter_id).removeAttr('disabled');
				$("#"+parent_filter_id).val('0');
				$(this).parent().find("input[type=hidden]").remove();
			}
			$.ajax({
				type: "POST",
				url: "<?php echo base_url() ?>mbc/retrievals/",
				data: { departments: "", banks: $("#dd_banks_dd").val(), designations:$("#dd_designations_dd").val(), search:'submit'}
				})
				.done(function(msg) {
				$("#result_table").html(msg);
			});
		});
	});
</script>
