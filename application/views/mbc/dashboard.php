
<div style='diplay:none'>
<div class="dashbox span1">
          <div class="visual">Total</div><!--end visual-->
          <div class="details">
				<div class="number" style="float:left;">1349</div> <div class="desc" style="float:left; padding-top:20px; margin-left:10px;">Resumes</div>
			</div><!--end details-->
          </div><!--end span1-->
          <div style="clear:both;"></div>  
            
          <div class="dashbox span2">
            <div class="visual">Position wise</div><!--end visual-->
            <div class="details">
				<div class="number">1349</div>
					<div class="desc">Resumes</div>
			</div><!--end details-->
            <a class="more" href="#">View more <i class="m-icon-swapright m-icon-white"></i></a>
            <table width="100%" border="0" cellspacing="0" cellpadding="5">
              <tr class="tdgray">
                <td width="80%">Position Name</td>
                <td width="20%">20</td>
              </tr>
              <tr class="tdwhite">
                <td>Position Name</td>
                <td>20</td>
              </tr>
              <tr class="tdgray">
                <td>Position Name</td>
                <td>20</td>
              </tr>
              <tr class="tdwhite">
                <td>Position Name</td>
                <td>20</td>
              </tr>
              <tr class="tdgray">
                <td>Position Name</td>
                <td>20</td>
              </tr>
            </table>

            </div><!--end span-2-->
            
            
            <div class="dashbox span3">
            <div class="visual">
            Bank wise
            </div><!--end visual-->
            <div class="details">
				<div class="number">1349</div>
					<div class="desc">Resumes</div>
			</div><!--end details-->
            <a class="more" href="#">View more <i class="m-icon-swapright m-icon-white"></i></a>
            <table width="100%" border="0" cellspacing="0" cellpadding="5">
              <tr class="tdgray">
                <td width="80%">Bank Name</td>
                <td width="20%">20</td>
              </tr>
              <tr class="tdwhite">
                <td>Bank Name</td>
                <td>20</td>
              </tr>
              <tr class="tdgray">
                <td>Bank Name</td>
                <td>20</td>
              </tr>
              <tr class="tdwhite">
                <td>Bank Name</td>
                <td>20</td>
              </tr>
              <tr class="tdgray">
                <td>Bank Name</td>
                <td>20</td>
              </tr>
            </table>
            </div><!--end span-3-->
            
            <div class="dashbox span4">
            <div class="visual">
            Location wise
            </div><!--end visual-->
            <div class="details">
				<div class="number">1349</div>
					<div class="desc">Resumes</div>
			</div><!--end details-->
            <a class="more" href="#">View more <i class="m-icon-swapright m-icon-white"></i></a>
            <table width="100%" border="0" cellspacing="0" cellpadding="5">
              <tr class="tdgray">
                <td width="80%">Location Name</td>
                <td width="20%">20</td>
              </tr>
              <tr class="tdwhite">
                <td>Location Name</td>
                <td>20</td>
              </tr>
              <tr class="tdgray">
                <td>Location Name</td>
                <td>20</td>
              </tr>
              <tr class="tdwhite">
                <td>Location Name</td>
                <td>20</td>
              </tr>
              <tr class="tdgray">
                <td>Location Name</td>
                <td>20</td>
              </tr>
            </table>
            </div><!--end span-4-->
            <div style="clear:both;"></div>
            <br />
            
            
          <div class="dashbox span5">
            <div class="visual">Profile alerts</div><!--end visual-->
            <div class="details">
				<div class="number">1349</div>
					<div class="desc">Profiles</div>
			</div><!--end details-->
            <a class="more" href="#">View more <i class="m-icon-swapright m-icon-white"></i></a>
            
            <table width="100%" border="0" cellspacing="0" cellpadding="5">
              <tr class="tdgray">
                <td width="8%" align="center"><img src="images/small_user.png" width="22" alt=" " /></td>
                <td width="92%"><strong>Vishal Chaskar</strong></td>
              </tr>
              <tr class="tdwhite">
                <td align="center"><img src="images/small_user.png" width="22" alt=" " /></td>
                <td><strong>Vishal Chaskar</strong></td>
              </tr>
              <tr class="tdgray">
                <td align="center"><img src="images/small_user.png" width="22" alt=" " /></td>
                <td><strong>Vishal Chaskar</strong></td>
              </tr>
              <tr class="tdwhite">
                <td align="center"><img src="images/small_user.png" width="22" alt=" " /></td>
                <td><strong>Vishal Chaskar</strong></td>
              </tr>
              <tr class="tdgray">
                <td align="center"><img src="images/small_user.png" width="22" alt=" " /></td>
                <td><strong>Vishal Chaskar</strong></td>
              </tr>
            </table>

            </div><!--end span5-->
            
            
            <div class="dashbox span6">
            <div class="visual">Jd alerts</div><!--end visual-->
            <div class="details">
				<div class="number">1349</div>
					<div class="desc">JD's</div>
			</div><!--end details-->
            <a class="more" href="#">View more <i class="m-icon-swapright m-icon-white"></i></a>
            <table width="100%" border="0" cellspacing="0" cellpadding="5">
              <tr class="tdgray">
                <td width="8%" align="center"><img src="images/bank.png" width="22" alt=" " /></td>
                <td width="92%"><strong>Bank Name</strong></td>
              </tr>
              <tr class="tdwhite">
                <td  align="center"><img src="images/bank.png" width="22" alt=" " /></td>
                <td><strong>Bank Name</strong></td>
              </tr>
              <tr class="tdgray">
                <td align="center"><img src="images/bank.png" width="22" alt=" " /></td>
                <td><strong>Bank Name</strong></td>
              </tr>
              <tr class="tdwhite">
                <td align="center"><img src="images/bank.png" width="22" alt=" " /></td>
                <td><strong>Bank Name</strong></td>
              </tr>
              <tr class="tdgray">
                <td align="center"><img src="images/bank.png" width="22" alt=" " /></td>
                <td><strong>Bank Name</strong></td>
              </tr>
            </table>

            </div><!--end span6--> 
            </div>