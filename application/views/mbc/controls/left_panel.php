<?php $user_data = $this->session->userdata('logged_in');
	$is_admin = $user_data["user_type_id"]== UserType::MBC_ADMIN ? true: false;
?>
<ul>
<li>
	<img src="<?php echo asset_url();?>images/icon-dashboard<?php echo $page_name == 'dashboard'?'-active':''; ?>.png" alt=" " />
	<a href="<?php echo base_url()?>mbc/dashboard" class="<?php echo $page_name == 'dashboard'?'active':''; ?>">Dashboard</a>
</li>
<li>
	<img src="<?php echo asset_url();?>images/icon-retrievals<?php echo $page_name == 'retrievals'?'-active':''; ?>.png" alt=" " />
	<a	href="<?php echo base_url()?>mbc/retrievals"  class="<?php echo $page_name == 'retrievals'?'active':''; ?>">Retrievals</a>
</li>
<li>
	<img src="<?php echo asset_url();?>images/icon-upload<?php echo $page_name == 'upload'?'-active':''; ?>.png" alt=" " />
		<a href="<?php echo base_url()?>mbc/candidate_upload" class="<?php echo $page_name == 'upload'?'active':''; ?>">Upload</a>
</li>
<li>
	<img src="<?php echo asset_url();?>images/icon-verify<?php echo $page_name == 'verify'?'-active':''; ?>.png" alt=" " />
	<a href="<?php echo base_url()?>mbc/dashboard/verify" class="<?php echo $page_name == 'verify'?'active':''; ?>">Verify</a>
</li>
<li>
	<img src="<?php echo asset_url();?>images/icon-recruitment<?php echo $page_name == 'recrutment_assistance'?'-active':''; ?>.png" alt=" " />
	<a href="<?php echo base_url()?>mbc/dashboard/recruitment" class="<?php echo $page_name == 'recrutment_assistance'?'active':''; ?>">Recruitment Assistance</a>
</li>
<?php if($is_admin) {?>
<li>
	<img src="<?php echo asset_url();?>images/icon-setting<?php echo $page_name == 'settings'?'-active':''; ?>.png" alt=" " />
	<a href="<?php echo base_url()?>mbc/settings" class="<?php echo $page_name == 'settings'?'active':''; ?>">Settings</a>
</li>
<?php }?>
</ul>