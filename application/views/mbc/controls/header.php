<div class="header-top">
<div class="usernav">
<?php $user_data = $this->session->userdata('logged_in');
	$is_admin = $user_data["user_type_id"]== UserType::MBC_ADMIN ? true: false;
	$editprofile_url = $is_admin? "":base_url()."mbc/user_management/edit";
?>
<ul>
	<li>
		<a href="<?php echo $editprofile_url;?>" title="">
		 <img
			src="<?php echo asset_url();?>images/profile.png" alt=""> 
		<span>
	<?php 
		
		echo $user_data["first_name"] . "&nbsp;" . $user_data["last_name"];
	?>	
	</span> <!--<img src="<?php echo asset_url();?>images/down.png" class="menu" alt=""> -->
	</a></li>
	<?php 
		switch ($user_data["user_type_id"])
		{
			case UserType::MBC_ADMIN:
	 ?>
		<li><a href="<?php echo base_url()?>mbc/user_management" title=""> <img
			src="<?php echo asset_url();?>images/management-icon.png" alt=""> <span>User
		Management</span> </a></li>
	<?php 			
		}
	?>	
	<li><a href="<?php echo base_url()?>auth/acl/logout" title=""> <img
		src="<?php echo asset_url();?>images/logout.png" alt=""> <span>Logout</span>
	</a></li>
	
</ul>
</div>
<!--end usernav-->
<div class="clear"></div>
</div>
<div class="logo">
	<img src="<?php echo asset_url();?>images/logo.png" width="250" alt=" " />
</div><!--end logo-->
        
<!-- <div class="search-box">
	<input name="search" type="text" placeholder="To search type and hit enter..." class="search" />
</div> --><!--end search-box-->
<div class="clear"></div>
