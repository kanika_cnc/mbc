<?php
$form =(object)$params;
$objmasters =(object)$masters;
$form_data = (object)$data;
?>
<style>
 .ui-autocomplete-loading {
    background: white url('<?php echo asset_url()?>styles/loading.gif') right center no-repeat;
  }  
</style>
<script
	type="text/javascript"
	src="<?php echo asset_url();?>scripts/jquery.ui.custom.combobox.js">
</script>
<script type="text/javascript" language="javascript">
  $(function() {
    $( "#can_curr_experince" ).combobox({
    	options:"can_curr_experince",
    	 select: function(event, ui) {
    		fillbranches();
    	}    	    
	});
    $( "#can_current_branch_id" ).combobox({
    	options:"can_current_branch_id",
    	select: function(event, ui) {
    	check_current_bank();
   	}    	    
	});
    $( "#can_current_branch_id" ).combobox();
    $( "#toggle" ).click(function() {
      $( "#can_curr_experince" ).toggle();
      $( "#can_current_branch_id" ).toggle();
    });
  });
function ShowToolTip(id) {
    if (document.getElementById(id).value == "5") {
        document.getElementById('lblToolTip').style.display = "block";
		document.getElementById('current_bank_label').innerHTML= "Currently which bank are you working for?";
		document.getElementById('current_department_label').innerHTML= "Your current department";
		document.getElementById('current_band_label').innerHTML= "Current Internal Band";
		document.getElementById('current_designation_label').innerHTML= "Current Designation";
		document.getElementById('current_branch_label').innerHTML= "Current Bank Branch";
		document.getElementById('previous_experience_title').innerHTML= "Prev Banking Experince";
		document.getElementById('previous_bank_label').innerHTML= "Which other banks you have worked for";
		var ctc = document.getElementById('ctc_tr').innerHTML;
		var loc = document.getElementById('loaction_tr').innerHTML;
		if(ctc == "" && loc == "")
		{
			ctc= document.getElementById('new_cuur_ctc').innerHTML;
			document.getElementById('ctc_tr').innerHTML= ctc;
			loc= document.getElementById('new_curr_loc').innerHTML;
			document.getElementById('loaction_tr').innerHTML= loc;
			$("#txtCurrLocation").autocomplete(autocomp_opt);
		}
		document.getElementById('new_ctc_box').style.display = "none";
    }
	else if(document.getElementById(id).value == "7"){
		document.getElementById('current_bank_label').innerHTML= "Which credit rating agency you are working for?";
		document.getElementById('lblToolTip').style.display = "none";
		document.getElementById('current_department_label').innerHTML= "Your current department";
		document.getElementById('current_band_label').innerHTML= "Current Internal Band";
		document.getElementById('current_designation_label').innerHTML= "Current Designation";
		document.getElementById('current_branch_label').innerHTML= "Current Branch";
		document.getElementById('previous_experience_title').innerHTML= "Prev Banking Experince";
		document.getElementById('previous_bank_label').innerHTML= "Which other banks you have worked for";
		var ctc = document.getElementById('ctc_tr').innerHTML;
		var loc = document.getElementById('loaction_tr').innerHTML;
		if(ctc == "" && loc == "")
		{
			ctc= document.getElementById('new_cuur_ctc').innerHTML;
			document.getElementById('ctc_tr').innerHTML= ctc;
			loc= document.getElementById('new_curr_loc').innerHTML;
			document.getElementById('loaction_tr').innerHTML= loc;
			$( "#txtCurrLocation" ).autocomplete(autocomp_opt);
		}
		document.getElementById('new_ctc_box').style.display = "none";
	}
	else if(document.getElementById(id).value == "8"){
		document.getElementById('current_bank_label').innerHTML= "Last bank you have worked for";
		document.getElementById('current_department_label').innerHTML= "Bank departrments you have worked for";
		document.getElementById('current_band_label').innerHTML= "Internal band";
		document.getElementById('current_designation_label').innerHTML= "Designated As";
		document.getElementById('current_branch_label').innerHTML= "Bank Branch worked for";
		document.getElementById('previous_experience_title').innerHTML= "Prev Banking Experince";
		document.getElementById('previous_bank_label').innerHTML= "Which other banks you have worked for";
		var ctc = document.getElementById('ctc_tr').innerHTML;
		var loc = document.getElementById('loaction_tr').innerHTML;
		document.getElementById('ctc_tr').innerHTML= "";
		document.getElementById('loaction_tr').innerHTML= "";
		document.getElementById('new_cuur_ctc').innerHTML= ctc;
		document.getElementById('new_curr_loc').innerHTML= loc;
		$( "#txtCurrLocation" ).autocomplete(autocomp_opt);
		document.getElementById('new_ctc_box').style.display = "block";
		document.getElementById('lblToolTip').style.display = "none";
	}
    else {
		document.getElementById('current_bank_label').innerHTML= "Currently which bank are you working for?";
		document.getElementById('current_department_label').innerHTML= "Your current department";
		document.getElementById('current_band_label').innerHTML= "Current Internal Band";
		document.getElementById('current_designation_label').innerHTML= "Current Designation";
		document.getElementById('current_branch_label').innerHTML= "Current Bank Branch";
		document.getElementById('previous_experience_title').innerHTML= "Prev Banking Experince";
		document.getElementById('previous_bank_label').innerHTML= "Which other banks you have worked for";
		var ctc = document.getElementById('ctc_tr').innerHTML;
		var loc = document.getElementById('loaction_tr').innerHTML;
		if(ctc == "" && loc == "")
		{
			ctc= document.getElementById('new_cuur_ctc').innerHTML;
			document.getElementById('ctc_tr').innerHTML= ctc;
			loc= document.getElementById('new_curr_loc').innerHTML;
			document.getElementById('loaction_tr').innerHTML= loc;
			$( "#txtCurrLocation" ).autocomplete(autocomp_opt);
		}
		document.getElementById('new_ctc_box').style.display = "none";
        document.getElementById('lblToolTip').style.display = "none";
    }
}
function check_current_bank()
{
	var currbank = $("#can_curr_experince").val();
	
	if(currbank =="")
	{
		alert("Please select current bank first");
		return false;
	}
}
function fillbranches(request, response)
{
	
	var currbank = $("#can_curr_experince").val();
	
	if(currbank =="")
	{
		alert("Please select current bank first");
		return;
	}
 	$.ajax({
        url: "<?php echo base_url()?>base/master_list/branches/" + currbank + "/json/true" ,
        dataType: "json",
        data: {
          style: "full",
          maxRows: 12
        },
        success: function(data) {
        	$('select#can_current_branch_id').empty();
        	$('#input-can_current_branch_id').val('');
            var toAppend='';
        	$.each(data, function(i, optionHtml){
        		toAppend += '<option value ="' + optionHtml.id+'">'+optionHtml.name+'</option>';
             });
        	$('#can_current_branch_id').append(toAppend);
        }
      });
}
 
 /* $(function()
{  $( "#txtCurrLocation" ).autocomplete(*/

		var autocomp_opt={
    	source: function( request, response ) {
		var searchstring = '';
		if(request.term == '')
		{
			searchstring = '0000000';
		}
		else
		{
			searchstring =  request.term;
		}
        $.ajax({
          url: "<?php echo base_url()?>base/master_list/locations/json/" + searchstring,
          dataType: "json",
          data: {
            style: "full",
            maxRows: 12
          },
          success: function(data) {
            response( $.map(data.options,function( item ) {
             $("#can_location_id").removeClass( "ui-autocomplete-loading" );  
              return {
                label: item.name,
                value: item.name,
                option: item.id
              };
            }));
          }
        });
      },
      minLength: 2,
      select: function( event, ui ) {
          $("#can_location_id").val(ui.item.option);
      },
      open: function() {
        $( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
      },
      close: function() {
        $( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
      }
      
    };
 $(function()
	{  
		$( "#txtCurrLocation" ).autocomplete(autocomp_opt);
});
function addRow(tableID,selectedDepartment,selKeys,ctrlid,selected) {
	
	var table = document.getElementById(tableID);

	var rowCount = table.rows.length;
	
	if(selected == true)
	{
		if(!document.getElementById(selKeys))
		{
		var row = table.insertRow(rowCount);
		row.id=selKeys;
		var cell1 = row.insertCell(0);
		var element1 = document.createTextNode(selectedDepartment);
		//lblDepartmentName.appendChild(document.createTextNode(selectedDepartment));
		cell1.appendChild(element1);
		var element2 = document.createElement("input");
		element2.type = "hidden";
		element2.value = selectedDepartment;
		element2.name = "ud_dm_description[]";
		//element2.name = "ud_is_current_temp[]";
		cell1.appendChild(element2);
		var element2 = document.createElement("input");
	
		var cell2 = row.insertCell(1);
		var element2 = document.createElement("input");
		element2.type = "checkbox";
		element2.value = 1 ;
		if(ctrlid == "user_departments")
		{
			element2.setAttribute("checked", "checked");
		}
		//element2.name = "ud_is_current_temp[]";
		element2.setAttribute("disabled", "disabled");
		cell2.appendChild(element2);
		var element2 = document.createElement("input");
		element2.type = "hidden";
		element2.value = 1 ;
		if(ctrlid == "user_departments")
		{
			element2.setAttribute("value", "1");
		}
		element2.name = "ud_is_current_temp[]";
		element2.setAttribute("readonly", "1");
		cell2.appendChild(element2);
		
		var cell3 = row.insertCell(2);
		var element2 = document.createElement("input");
		element2.type = "checkbox";
		element2.value = 1 ;
		element2.name = "ud_is_interested_temp[]";
		element2.setAttribute("class", "depinterested");
		
		
		cell3.appendChild(element2);
		var cell4 = row.insertCell(3);
		var element3 = document.createElement("input");
		element3.type = "text";
		element3.name = "ud_relevant_experince[]";
		cell4.appendChild(element3);
	
		//var cell4 = row.insertCell(3);
		var element4 = document.createElement("input");
		element4.type = "hidden";
		element4.name = "ud_department_id[]";
		element4.setAttribute("class", "departmentId");
		element4.value = selKeys;
		cell4.appendChild(element4);
	
		var element4 = document.createElement("input");
		element4.type = "hidden";
		element4.name = "ud_is_current[]";
		if(ctrlid == "user_departments")
		{element4.value = 1;}
			else
		{element4.value = 0;}
		
		cell4.appendChild(element4);
		
		var element5 = document.createElement("input");
		element5.type = "hidden";
		element5.name = "ud_is_interested[]";
		element5.setAttribute("class", "depinterested_f");
		cell4.appendChild(element5);
		$(".depinterested").click(function () {
				//alert("this");
				if($(this).is(':checked'))
				{
					$(this).closest('tr').find('.depinterested_f').val("1");
				}
				else
				{
					$(this).closest('tr').find('.depinterested_f').val("0");
				}
			});
		}
	}
	else
	{
		var rowtobedeleted = document.getElementById(selKeys);
		var rowindex = rowtobedeleted.rowIndex;
		table.deleteRow(rowindex);
	}
	}
</script>
<?php
$attributes = array('id' => $form->id);
echo form_open_multipart($form->url.$form->uri, $attributes); ?>
<?php echo validation_errors(); ?>
<?php echo $errors;?>
<?php echo $messages;?>
<?php echo $this->session->flashdata('message');?>
<div class="box-2" id="Registration">
<h1 class="title">Registration</h1>
<div style="clear: both;"></div>
<br />
<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td valign="top" width="30%"><?php echo form_label("Name<span style='color:red'>*</span>","user_first_name",array('class'=>'input-label'))?>
		</td>
		<td><?php echo form_input('user_first_name', $form_data->user_first_name, 'id="user_first_name"  class="input-type-text size1" placeholder="First Name"')  . form_input('user_last_name', $form_data->user_last_name, 'id="user_last_name" class="input-type-text lalign size1" placeholder="Last Name"');?>
		</td>
	</tr>
	<tr>
		<td valign="top"><?php echo form_label("Email<span style='color:red'>*</span>","user_email",array('class'=>'input-label'))?>
		</td>
		<td><?php 
		echo form_input('user_email', $form_data->user_email, 'id="user_email" class="input-type-text size1"');
		?></td>
	</tr>
	<tr>
		<td valign="top"><?php echo form_label("Contact Number<span style='color:red'>*</span>","user_contact_mobile",array('class'=>'input-label'))?>
		</td>
		<td><?php 
		echo "0 - ". form_input('user_contact_mobile', $form_data->user_contact_mobile, 'id="user_contact_mobile" class="input-type-text" maxlength="10"');?>
		</td>
	</tr>
	<tr>
		<td valign="top"><label class="input-label">Total Years of Experience</label></td>
		<td><?php
		$exp = (convert_months_to_year_array($form_data->can_total_experience));
		echo form_dropdown("ddl_exp_years", $objmasters->exp_years,$exp["years"] ,'id="ddl_exp_years" class="input-type-select"') ." Years " . form_dropdown("ddl_exp_months",$objmasters->exp_months,$exp["months"] ,'id="ddl_exp_months" " class="input-type-select"')." Months" ;?>
		</td>
	</tr>
	<tr>
		<td valign="top"><span class="input-label">What best suites your status?</span></td>
		<td>
		<?php
			$arrcantype=array(""=>"select",5=>"I am corporate banker",6 =>"My client is a corporate bank in India",
			7=>"I work for credit rating agencies in India",
			8=>"I earlier worked for a corporate bank in India",
			9=>"Fresher") ;
			echo form_dropdown("can_type_id",$arrcantype,$form_data->can_type_id,'id ="can_type_id" class="input-type-select" onchange="ShowToolTip(this.id)"')
		?>
		<br />
		<label id="lblToolTip" for="ddlCandidateType" style="display: none">Tip:
		<i>Added with this option includes operations, Administration, Central
		Processing Unit & support functions for Corporate Branch in India</i></label>

		</td>
	</tr>
</table>
<!--end addmore-->
<div style="clear: both;"></div>
</div>
<div
	class="box-2" id="experience"><!--<img src="images/icon-3.jpg" width="40" alt="" align="left" /> -->
<h1 class="title">Experience</h1>
<div style="clear: both;"></div>
<br />
<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td valign="top" width="30%"><label id="current_bank_label" class="input-label"> Currently which bank are you working for?</label>
		</td>
		<td><?php  echo form_dropdown('can_curr_experince',  $objmasters->bank,$form_data->can_curr_experince ,'id="can_curr_experince" class="" onchange="fillbranches()"');?>
		</div>
		</div>

		</td>
	</tr>
	<tr>
		<td valign="top"><label id="current_department_label" class="input-label"> Your current department
		<p class="note">(you can select multiple)</p>
		</label></td>
		<td><?php 
		if(!empty($form_data->str_user_departments))
		echo form_hcheckboxTree("user_departments",$objmasters->departments,$form_data->str_user_departments,"callback");
		else
		echo form_hcheckboxTree("user_departments",$objmasters->departments,'',"callback");
			?>
		
		</td>
	</tr>

	<tr id="ctc_tr">
		<td valign="top"><label class="input-label"> Current CTC</label></td>
		<td>
			<?php 
			
			$form_data->can_ctc = ($form_data->can_ctc == "")?"0.0" :$form_data->can_ctc;
			$arr_ctc =  explode(".", strval($form_data->can_ctc));
			echo form_ctc_lksdropdown("can_ctc_lks",$arr_ctc[0] ,'id="can_ctc_lks" class="input-type-select"') ." Lakhs " . form_dropdown("can_ctc_thousands", $objmasters->ctc_thousands,$arr_ctc[1] ,'id="ctc_thousands" class="input-type-select size4"')." Thousands " ;
			echo form_input("can_ctc_notes","$form_data->can_ctc_notes" ,'id="can_ctc_notes" class="large input-type-text size5" placeholder="additional perks, eg accomodation etc"');?>
		</td>
	</tr>
	<tr>
		<td valign="top"><label id="current_band_label" class="input-label"> Current Internal Band</label></td>
		<td><input name="can_band" type="text" value="<?php echo $form_data->can_band?>"
			class="input-type-text size2" /></td>
	</tr>

	<tr>
		<td valign="top"><label id="current_designation_label" class="input-label"> Current Designation</label></td>
		<td width="290"><?php  echo form_dropdown('can_designation_id',$objmasters->designations ,$form_data->can_designation_id ,'id="ddlCurrentBanks" class="input-type-select size3"');?>
		</td>
	</tr>
	<tr id="loaction_tr">
		<td valign="top" width="120"><label class="input-label"> Current Location </label></td>
		<td width="290"><?php echo form_input("txtCurrLocation",$form_data->can_location_name,' id="txtCurrLocation"  class="large input-type-text size2"');?>
		<input type="hidden" id="can_location_id" name="can_location_id" value='<?php echo $form_data->can_location_id; ?>'>

		</td>
	</tr>
	<tr>
		<td valign="top"><label id="current_branch_label" class="input-label"> Current Bank Branch</label></td>
		<td>
		<div id="DivCurrBankBranch"></div>
		
		<?php 
			  if($form_data->can_current_branch_id != "")
			  {
				echo form_dropdown("can_current_branch_id",$this->objMaster->get_branches($form_data->can_curr_experince,true,true),$form_data->can_current_branch_id,'class="jscurrbankbranch large" onclick="check_current_bank()" id="can_current_branch_id"');
			  }	
			  else
			  {
			  	echo form_dropdown("can_current_branch_id",$this->objMaster->get_branches(0,true,true),$form_data->can_current_branch_id,'class="jscurrbankbranch large" id="can_current_branch_id" onclick="check_current_bank()" ');
			  } 
		?>
		</td>
	</tr>

</table>
<!--end addmore-->
<div style="clear: both;"></div>
</div>
<div class="box-2" id="new_ctc_box" style="display:none;">
<h1 class="title">Current CTC &amp; Location</h1>
<div style="clear: both;"></div>
<br />
<table width="540" border="0" cellspacing="0" cellpadding="5">
	<tr id="new_cuur_ctc">
	</tr>
	<tr id="new_curr_loc"> 
	</tr>
</table>
</div>
<div class="box-2" id="preexperience">
<h1 class="title" id="previous_experience_title">Previous experience</h1>
<div style="clear: both;"></div>
<br />
<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td width="30%" valign="top"><label id="previous_bank_label" class="input-label"> Tick ALL Banks you have worked with in your career except your current
		<p class="note">(you can select multiple)</p>
		</label></td>
		<td><?php 
					echo form_hcheckboxTree("can_prev_experince",$objmasters->hbanks,$form_data->can_prev_experince,"");	
					?>
		</td>
	</tr>
	<tr>
		<td valign="top"><label class="input-label"> Which all departments you have worked earlier
		<p class="note">(you can select multiple)</p>
		</label></td>
		<td>
		
		<?php
		if(!empty($form_data->user_prev_departments))
		 echo form_hcheckboxTree("user_prev_departments",$objmasters->departments,$form_data->user_prev_departments,"callback");
		 else
		  echo form_hcheckboxTree("user_prev_departments",$objmasters->departments,"","callback");
		 	?>

		</td>
	</tr>
	
	<tr>
		<td colspan="2">
		<div>
		<table width="100%" id="tbCurrDepartmentDetails" border="0" cellpadding="5px" cellspacing="0" class="experience-table">
			<tr>
				<th width="30%" valign="top">Department</th>
				<th width="10%" valign="top">Is current</th>
				<th>Are you intreseted in recieving opportunities for this
				department</th>
				<th width="25%" valign="top">Approx relevant exp.in months</th>
			</tr>
			<?php 
			$depts = "";
			if(isset($form_data->user_prev_departments))
			$depts = $form_data->user_prev_departments;
			if(isset($form_data->str_user_departments))
			$depts=$depts.$form_data->str_user_departments;
	if(!empty($depts))
			{
		?>
			<?php 
			
			$upd_array = explode(",",$form_data->user_prev_departments.$form_data->str_user_departments);
			foreach ($upd_array as $upd) {  
			if(!empty($upd)) {
				$prev_dept_details = $form_data->prev_dept_details;
			?>
					
					<tr id="<?php  echo $upd; ?>">
						<td class="table-left-column"> <?php  echo rtrim($prev_dept_details[$upd]['dm_description'],"/"); ?><input type="hidden" name="ud_dm_description[]" value="<?php  echo $prev_dept_details[$upd]['dm_description']; ?>" /></td>
						<td> <input disabled="disabled" type="checkbox" name=""<?php  if($prev_dept_details[$upd]['ud_is_current']) echo 'checked="checked"' ?> value="1"   /> <input type="hidden" name="ud_is_current_temp[]" value="<?php echo $prev_dept_details[$upd]['ud_is_current'] ?>" />
						</td>
						<td><input type="checkbox" name="ud_is_interested_temp[]"<?php  if($prev_dept_details[$upd]['ud_is_interested']) echo 'checked="checked"'; ?> value="1" class="depinterested"  />
						
						</td>
						<td> <input type="text" value="<?php  echo $prev_dept_details[$upd]['ud_relevant_experince']; ?>" name="ud_relevant_experince[]" />
						<input type="hidden" name="ud_department_id[]" class="departmentId" value="<?php echo $upd ?>">
						<input type="hidden" name="ud_is_current[]" value="<?php if($prev_dept_details[$upd]['ud_is_current']) echo 1; else echo 0 ?>">
						<input type="hidden" class="depinterested_f" name="ud_is_interested[]" value="<?php if($prev_dept_details[$upd]['ud_is_interested']) echo 1; else echo 0 ?>">
						</td>
					</tr>
					
			<?php } } ?>
			<?php } ?>
		</table>
		</div>

		</td>
	</tr>
	
</table>
<!--end addmore-->
<div style="clear: both;"></div>
</div>

<!--end box-->
<div
	class="box-2" id="qualification"><!--<img src="images/icon-2.jpg" width="40" alt="" align="left" /> -->
<h1 class="title">Qualification</h1>
<div style="clear: both;"></div>
<br />
<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td width="30%" valign="top"><label class="input-label"> Post Graduation
		<p class="note">(you can select multiple)</p>
		</label></td>
		<td>
		<?php
			$selected_post_grad =  !empty($form_data->can_postgrad_qualifications) ? $form_data->can_postgrad_qualifications: "";
			echo form_hcheckboxTree("can_postgrad_qualifications",$this->objMaster->get_qualifications(Qualifications::POSTGRADUATION,List_Bl::FORMAT_TREE),$selected_post_grad);
		  ?>
		</td>
	</tr>
	<tr>
		<td valign="top"><label class="input-label"> Graduation
		<p class="note">(you can select multiple)</p>
		</label></td>
		<td>
		

		<?php
			$selected_grad =  !empty($form_data->can_grad_qualifications) ? $form_data->can_grad_qualifications: "";
			echo form_hcheckboxTree("can_grad_qualifications",$this->objMaster->get_qualifications(Qualifications::GRADUATION,List_Bl::FORMAT_TREE),$selected_grad);
		?>
		</td>
	</tr>
	<tr>
		<td valign="top"><label class="input-label"> Certification
		<p class="note">(multiple certificates to be seprated by ',')</p>
		</label></td>
		<td><input name="can_certifications" value='<?php echo $form_data->can_certifications;?>'  type="text" class="input-type-text" /></td>
	</tr>
</table>
</div>
<!--end box-->
<div
	class="box-2" id="personal"><!--<img src="images/icon-1.jpg" width="40" alt="" align="left" /> -->
<h1 class="title">Personal</h1>
<div style="clear: both;"></div>
<br />
<table width="100%" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td width="30%" valign="top"><label class="input-label"> Age years</label></td>
		<td><?php echo form_dropdown("can_age",$objmasters->age,$form_data->can_age,"id='can_age', class='input-type-select size6'")?>
		</td>
	</tr>
	<tr>
		<td valign="top"><label class="input-label"> Date of Birth </label></td>
		<td><?php 
			$date_of_birth = date_parse($form_data->can_dob);
			if (checkdate($date_of_birth["month"], $date_of_birth["day"], $date_of_birth["year"]))
			//if($form_data->can_dob != "" || $form_data->can_dob != "//" )
			{
				$dob = explode("-",$form_data->can_dob);
				$dob["months"] = $dob[1];
				$dob["days"] = $dob[2];
				$dob["years"] = $dob[0];
			}
			else
			{
				if($form_data->can_dob != "")
				{
					$dob = explode("-",$form_data->can_dob);
					if(is_array($dob))
					{
						$dob["months"] = $dob[1];
						$dob["days"] = $dob[2];
						$dob["years"] = $dob[0];
					}
					
				}
				else
				{
					$dob["months"] = "";
					$dob["days"] = "";
					$dob["years"] = "";
				}
			}
			echo form_dropdown('dob_day',$objmasters->days ,$dob["days"]  ,'id="ddlDOBDay" class="small-select date input-type-select size6"') ." &nbsp; " .form_monthdropdown('dob_month', $dob["months"], 'id="ddlDOBMonth" class="small-select month"')." &nbsp; " .form_dropdown('dob_year', $objmasters->dob_years,$dob["years"] ,'id="dob_year" class="small-select year input-type-select size6"')."&nbsp;" ;?>
		<div style="clear: both;"></div>
		</td>
	</tr>
	<tr>
		<td valign="top"><label class="input-label"> Gender </label></td>
		<td><input name="can_gender" type="radio" value="M" <?php echo ($form_data->can_gender == "M" ? "checked":"");?> />Male <input
			name="can_gender" type="radio" value="F" <?php echo ($form_data->can_gender == "F" ? "checked":"");?> />Female</td>
	</tr>
	<tr>
		<td valign="top"><label class="input-label"> Marital Status</label></td>
		<td><?php 
		$arrmarstattus = array("" =>" ","Married"=>"Married","Single"=>"Single","Others"=>"Others");
		echo form_dropdown("can_marital_status",$arrmarstattus,$form_data->can_marital_status,"class='small=select input-type-select'");?>
		</td>
	</tr>
	<tr style="overflow:hidden">
		<td valign="top"><label class="input-label"> Attach Resume</label></td>
		<td><input name="flResume" type="file" value="Add File" id="f1resume" class="addfile" /> <input type="hidden" name="f1Resume_h" value="<?php echo $form_data->can_resume_file ?>" id="resume_file" />
			
			<?php 
				
				echo '<span id="resume_file_name"><a href="'.base_url().'uploads/resume/'.$form_data->can_resume_file.'">'.$form_data->can_resume_file.'</a></span>';
			?>
			</td>
	</tr>
	<tr>
		<td valign="top"><label class="input-label"> Upload Photo</label></td>
		<td><input name="flPhotos" id="flPhotos" type="file" value="Add File"
			class="addfile" value="<?php echo $form_data->can_photo ?>" />
			<input type="hidden" name="f1Photos_h" id="photo_file" value="<?php echo $form_data->can_photo ?>" />
			<?php 
				
				echo '<img src="'.base_url().'uploads/profile_photo/'.$form_data->can_photo.'" id="photo_display" width="50" />';
			?>
			</td>
	</tr>
	<tr>
		<td>
			<?php echo form_hidden("user_id",$form_data->user_id)?>
			<?php echo form_hidden("can_user_id",$form_data->can_user_id)?>
		</td>
	</tr>
</table>
</div>
<!--end box-->
                <div class="box-2">
                	<div class="save-button">
                       <input name="submit" type="submit"  value="Save" class="submit" /> &nbsp;
                       <input name="submit" type="submit" value="Verify And Save" class="submit" />
                	</div>
                </div>
                
                <?php echo form_close();?>
				
				<script type="text/javascript">
				<?php $timestamp = time();?>
		$(function() {
			$(".depinterested").click(function () {
				//alert("this");
				if($(this).is(':checked'))
				{
					$(this).closest('tr').find('.depinterested_f').val("1");
				}
				else
				{
					$(this).closest('tr').find('.depinterested_f').val("0");
				}
			});
			<?php if($form_data->can_photo == "") { ?>
			$("#photo_display").hide();
			<?php }
			 if($form_data->can_resume_file == "") { ?>
			$("#resume_file_name").hide();
			<?php } ?>
			$('#f1resume').uploadify({
				'formData'     : {
					'timestamp' : '<?php echo $timestamp;?>',
					'token'     : '<?php echo md5('unique_salt' . $timestamp);?>',
					'type' : 'resume'
				},
				'fileTypeExts' : '*.doc; *.docx; *.pdf',
				'buttonText' : 'BROWSE...',
				'multi'    : false,
				'swf'      : '<?php echo asset_url() ?>images/uploadify.swf',
				'uploader' : '<?php echo base_url() ?>mbc/candidate_upload/upload_files',
				'onUploadSuccess' : function(file, data, response) {
					$("#resume_file").val(data);
					$("#resume_file_name").show();
					$("#resume_file_name").html(data);
					 $('input[type="submit"]').removeAttr('disabled');
				},
				'onSelect' : function() {
					$('input[type="submit"]').attr('disabled','disabled');
				}
			});
			$('#flPhotos').uploadify({
				'formData'     : {
					'timestamp' : '<?php echo $timestamp;?>',
					'token'     : '<?php echo md5('unique_salt' . $timestamp);?>',
					'type' : 'photo'
				},
				'fileTypeExts' : '*.gif; *.jpg; *.png',
				'buttonText' : 'BROWSE...',
				'multi'    : false,
				'swf'      : '<?php echo asset_url() ?>images/uploadify.swf',
				'uploader' : '<?php echo base_url() ?>mbc/candidate_upload/upload_files',
				'onUploadSuccess' : function(file, data, response) {
					 $('input[type="submit"]').removeAttr('disabled');
					$("#photo_file").val(data);
					$("#photo_display").show();
					$("#photo_display").attr('src','<?php echo base_url() ?>uploads/profile_photo/'+data);
				},
				'onSelect' : function() {
					$('input[type="submit"]').attr('disabled','disabled');
				}
			});
		});
	</script>
