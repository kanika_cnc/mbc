
<ul class="tab-menu ">
	<li><a href="<?php echo base_url()?>mbc/settings/departments"
		class="<?php echo $tab_name== "departments"?"active":"" ;?>">Departments</a></li>
	<li><a href="<?php echo base_url()?>mbc/settings/banks""
		class="<?php echo $tab_name== "banks"?"active":"" ;?>">Banks</a></li>
	<li><a href="<?php echo base_url()?>mbc/settings/qualifications"
		class="<?php echo $tab_name== "qualifications"?"active":"" ;?>">Qualification</a></li>
	<!--  <li><a href="<?php echo base_url()?>mbc/settings/ctc"
		class="<?php echo $tab_name== "ctc"?"active":"" ;?>">CTC</a></li>-->
	<li><a href="<?php echo base_url()?>mbc/settings/designations"
		class="<?php echo $tab_name== "designations"?"active":"" ;?>">Designations</a></li>		
	<li><a href="<?php echo base_url()?>mbc/settings/locations"
		class="<?php echo $tab_name== "locations"?"active":"" ;?>">Location</a></li>
	<li><a href="<?php echo base_url()?>mbc/settings/member_banks"
		class="last  <?php echo $tab_name== "member_banks"?"active":"" ;?>">Member Banks</a></li>
	<!--  <li><a href="template.php" class="last"
		class="<?php echo $tab_name== "settings"?"active":"" ;?>">Template</a></li>-->
</ul>
<div class="tab-box"><?php
echo $tab_data;
?></div>
<!--end box-->
<script>
if ($(".cg-dialog-wrapper").css("display") == "none")
{
	$(".tab-menu").show();
}
else if($(".cg-dialog-wrapper").css("display") == "block")
{
	$(".tab-menu").hide();
}
else
{
	$(".tab-menu").show();
}
</script>
