<?php

$objmasters =(object)$masters;
$form_data = (object)$data;
?>
<ul class="tab-menu ">
                	<li><a href="<?php echo base_url(); ?>mbc/search" class="active">Search</a></li>
                    <li><a href="<?php echo base_url(); ?>mbc/retrievals" class="last">Visual</a></li>
                </ul>
                <?php echo form_open("mbc/search"); ?>
            <div class="tab-box">
<div class="box-search">
                	<ul>
                		<li><h5>Search in :</h5></li>
                        <li><input name="search_type" type="radio" checked="checked" value="0" id="rad_boolean" /> Boolean search</li>
                        <li><input name="search_type" type="radio" value="1" id="rad_keyword" /> key word search
                        <!--<select>
                        	<option value="all">All words</option>
                            <option>Any of the words</option>
                            <option>Exact pharase</option>
                        </select>-->
                        </li>
                	</ul>
                    <div class="clear"></div>
                </div><!--end box-search-->
            
       	    <div class="box-search">
            <table width="400" border="0" id="box_keyword" cellspacing="0" cellpadding="5">
  <tr>
    <td width="127"><label><strong>Any of the Keywords :</strong></label></td>
    <td width="253"><input name="anykeywords" type="text" placeholder="e.g. Banker"  class="input"/></td>
  </tr>
  <tr>
    <td><span class="search-option">
      <label><strong>All the Keywords :</strong></label>
    </span></td>
    <td><span class="search-option">
      <input name="allkeywords" type="text" placeholder="e.g. Banker"  class="input"/>
    </span></td>
  </tr>
  <tr>
    <td><span class="search-option">
      <label><strong>Excluding Keywords :</strong></label>
    </span></td>
    <td><span class="search-option">
      <input name="excludekeywords" type="text" placeholder="e.g. Banker"  class="input"/>
    </span></td>
  </tr>
</table>
 <table width="400" border="0" id="box_boolean" cellspacing="0" cellpadding="5">
  <tr>
    <td width="127"><label><strong>Boolean Search :</strong></label></td>
    <td width="253"><input name="boolean_textbx" type="text" placeholder="Boolean Search"  class="input"/></td>
  </tr>

</table>
              </div><!--end box-search-->
              
  
  <table width="550" border="0" cellspacing="0" cellpadding="5">
  <tr>
    <td width="140"><span class="search-option">
      <label><strong>Total Experience :</strong></label>
    </span></td>
    <td width="390">
    <?php
		$exp = (convert_months_to_year_array($form_data->can_total_experience));
		echo form_dropdown("ddl_exp_years", $objmasters->exp_years,$exp["years"] ,'id="ddl_exp_years" class=""') ." Years " . form_dropdown("ddl_exp_months",$objmasters->exp_months,$exp["months"] ,'id="ddl_exp_months" " class=""')."Months" ;?> </td>
  </tr>
  <tr>
    <td><span class="search-option">
      <label><strong>Annual Salary :</strong></label>
    </span></td>
    <td><?php $form_data->can_ctc = ($form_data->can_ctc == "")?"0.0" :$form_data->can_ctc;
			$arr_ctc =  explode(".", strval($form_data->can_ctc));
			echo form_dropdown("can_ctc_lks",$objmasters->ctc_lakhs,$arr_ctc[0] ,'id="can_ctc_lks" class=""') ." Lakhs " . form_dropdown("can_ctc_thousands", $objmasters->ctc_thousands,$arr_ctc[1] ,'id="ctc_thousands" class=""')."Thousands" ;?>       
       		<div class="clear"></div></td>
  </tr>
  <tr>
    <td><span class="search-option">
      <label><strong>Candidate Preferred Location :</strong></label>
    </span></td>
    <td><span class="search-option">
      <?php echo form_input("txtbxCurrLocation",$form_data->can_location_name,' id="txtCurrLocation"  class="large"');?>
		<input type="hidden" id="can_location_id" name="can_location_id" value='<?php echo $form_data->can_location_id; ?>'>
    </span></td>
  </tr>
</table>
            


                
<div class="border"></div>
                
                
                <h2 class="heading">Employment Details</h2>
                
                
                
    <table width="600" border="0" cellspacing="0" cellpadding="5">
  <tr>
    <td width="140"><strong>Department :</strong></td>
    <td width="440"><?php 
		
		echo form_hcheckboxTree("user_departments",$objmasters->departments,$form_data->str_user_departments,"callback");
		
			?></td>
  </tr>
  <tr>
    <td><span class="search-option">
      <label><strong>Employers :</strong></label>
    </span></td>
    <td><input name="bank" type="text" placeholder="Add Qualification"  class="input medium"/>
                    <select class="medium-select">
                    	<option>-Current Employer-</option>
                    </select>  
                    <select class="medium-select">
                    	<option>-All Words-</option>
                    </select>              
       		<div class="clear"></div></td>
  </tr>
  <tr>
    <td><span class="search-option">
      <label><strong>Exclude Employers :</strong></label>
    </span></td>
    <td><span class="search-option">
      <input name="bank4" type="text" placeholder="Add Qualification"  class="input medium"/>
      <select name="select2" class="medium-select">
        <option>-Current Employer-</option>
      </select>
      <select name="select2" class="medium-select">
        <option>-All Words-</option>
      </select>
    </span></td>
  </tr>
  <tr>
    <td><strong>Designation :</strong></td>
    <td><span class="search-option">
      <input name="bank5" type="text" placeholder="Add Qualification"  class="input medium"/>
      <select name="select3" class="medium-select">
        <option>-Current Designation-</option>
      </select>
      <select name="select3" class="medium-select">
        <option>-All Words-</option>
      </select>
    </span></td>
  </tr>
</table>
            

                
                
          <div class="border"></div>
                
                
                <h2 class="heading">Education Details</h2>
                
                <table width="400" border="0" cellspacing="0" cellpadding="5">
  <tr>
    <td width="140"><strong>UG Qualification :</strong></td>
    <td width="240"><span class="search-option">
      <select name="select4" class="large-select">
        <option>-Select UG Qualification-</option>
      </select>
    </span></td>
  </tr>
  <tr>
  	<td colspan="2" align="center">
    <a href="searchresult.html"><input name="Submit" type="button" value="Search" class="submit" /></a></td>
  </tr>
  
</table>

                
                                    
                   
                    
                    <!--<p class="search-option">
                	<label><strong>Institute Name :</strong></label>
                    <input name="bank" type="text" placeholder="Add Qualification"  class="input medium"/>
                    <select class="medium-select">
                    	<option>-Current Employer-</option>
                    </select>   -->
            
             		<div class="clear"></div>
                    </p>
                
            </div><!--end box-->
<script type="text/javascript">
$(document).ready(function(){
	$("#box_keyword").hide();
	$("#rad_boolean").click(function(){
		$("#box_keyword").hide();
		$("#box_boolean").show();
	});
	$("#rad_keyword").click(function(){
		$("#box_keyword").show();
		$("#box_boolean").hide();
	});

});
$(function()
{  $( "#txtCurrLocation" ).autocomplete({
      source: function( request, response ) {
        $.ajax({
          url: "<?php echo base_url()?>base/master_list/locations/json/" + request.term,
          dataType: "json",
          data: {
            style: "full",
            maxRows: 12
          },
          success: function(data) {
            response( $.map(data.options,function( item ) {
              return {
                label: item.name,
                value: item.name,
                option: item.id
              };
            }));
          }
        });
      },
      minLength: 2,
      select: function( event, ui ) {
          $("#can_location_id").val(ui.item.option);
      },
      open: function() {
        $( this ).removeClass( "ui-corner-all" ).addClass( "ui-corner-top" );
      },
      close: function() {
        $( this ).removeClass( "ui-corner-top" ).addClass( "ui-corner-all" );
      }
    });
});
</script>