<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<link href="<?php echo asset_url();?>/styles/style.css" rel="stylesheet" type="text/css" />
<link rel="stylesheet" type="text/css" href="/css/result-light.css">
<script type='text/javascript' src='http://code.jquery.com/jquery-1.9.1.min.js'></script>
<script type='text/javascript' src='<?php echo asset_url();?>/scripts/jquery.validate.min.js'></script>
<script type='text/javascript' src='<?php echo asset_url();?>/scripts/jquery.ui.autocomplete.js'></script>

<title>Mybankconnect | <mp:Title/></title>
</head>

<body>
    
<div id="wrapper">
	<div id="header">
		<?php $this->load->view( "controls/header" );?>
	</div><!--end header-->	
    
	<div id="main">	
		<mp:Content/>
    </div><!--end main-->

	<div id="footer">
		<?php $this->load->view( "controls/footer" );?>
	</div><!--end footer-->

</div><!--end wrapper-->

</body>
</html>
