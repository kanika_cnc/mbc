<?php $attributes = array('id' => 'loginForm');
      echo form_open('Auth/login', $attributes); ?>
    <input name="login" type="submit" value="Login" tabindex="3" class="button">
    <input name="password" type="password" placeholder="Password" tabindex="2" value="" class="input">
    <input name="email" type="text" value="" class="input" tabindex="1" placeholder="Email">

<script>
$(document).ready(function() { 
    $("#loginForm").validate({
       debug: true,
       rules: {
            email: {
              required: true,
              email: true
            },
            password: {
              required: true
            }
        },
       messages: {
            email: {
                required: "Please enter a email adress",
                email: "Please enter a valid email address"
            },
            password:"Please enter password"
        }
    });
    });
</script>	