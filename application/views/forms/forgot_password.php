<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Login Form</title>
<link href="<?php echo asset_url();?>/styles/login.css" rel="stylesheet" type="text/css" />
<script type='text/javascript' src='<?php echo asset_url();?>/scripts/jquery-1.9.1.js'></script>
<script type='text/javascript' src='<?php echo asset_url();?>/scripts/jquery.validate.min.js'></script>
<script type='text/javascript' src='<?php echo asset_url();?>/scripts/login.js'></script>

</head>
<body>
<?php echo $javascript;?>
<div id="logo">
<img src="<?php echo asset_url();?>/images/logo.png" width="500" alt=" " />
</div><!--end logo-->
<!--WRAPPER-->

<div id="wrapper">
	<!--SLIDE-IN ICONS-->
    <div class="user-icon"></div>
    <div class="pass-icon"></div>
    <!--END SLIDE-IN ICONS-->
	<!--LOGIN FORM-->
	<?php $attributes = array('id' => 'forgotForm','class'=>'login-form');
      echo form_open('auth/acl/forgot_password', $attributes); ?>
	<!--HEADER-->
    <div class="header">
    <span style="color:red"><?php echo validation_errors(); ?>
	<?php if($email_sent == 1)
		{
			echo "An email has been sent to ". $email . " with your password.";
		}
	?>
	<?php if(!empty($error)) { ?>
	<?php echo $error ?>
	<?php } ?>
	</span>
    <!--TITLE--><h1>Forgot Password</h1><!--END TITLE-->
	    </div>
    <!--END HEADER-->
	
	<!--CONTENT-->
    <div class="content">
	<!--USERNAME-->
		<?php echo form_input("email",$email,"id = 'email' class='input username' placeholder='Email'");?>
		<!--END USERNAME-->
    </div>
    <!--END CONTENT-->
    
    <!--FOOTER-->
    <div class="footer">
    <!--LOGIN BUTTON--><input type="submit" name="submit" value="Send Email" class="button" /><!--END LOGIN BUTTON-->
    </div>
    <?php echo form_close();?>
    <!--END FOOTER-->

<!--END LOGIN FORM-->

</div>
<!--END WRAPPER-->

<!--GRADIENT--><div class="gradient"></div><!--END GRADIENT-->

</body>
</html>