<?php
echo form_open("mbc/retrievals"); ?>
<div class="col-left" id="departments">
		<h4>Departments (<?php echo count($form_data->candidates);?>)</h4>
                <?php	echo form_hcheckboxTree("departments",$objmasters->departments,$form_data->departments,"",true,3); ?>
                </div><!--end col-left-->
                
              <div class="col-right">
                <div class="box-search">
                        <ul>
                		<li><h5>Banks</h5></li>
                        <li>
                        	<?php  echo form_dropdown('banks',  $objmasters->bank,$form_data->banks,'id="banks"');?>
                        </li>
                        <li><h5>Designations</h5></li>
                        <li>
                        	<?php  echo form_dropdown('designations',  $objmasters->designations,$form_data->designations,'id="designations"');?>
                        </li>
						<li> 
						
						<?php 
						
						/// Problem in Departments tree structure for using ajax. Hence the submit button for now.
						echo form_submit('search', 'Search'); ?> </li>						
                        
                	</ul>                        
                    <div class="clear"></div>
                  </div><!--end box-search-->
                      <?php echo form_close();?>
                    
            <table width="98%" border="0" cellspacing="0" cellpadding="5" align="center">
			<?php
			if(count($form_data->candidates)>0)
			{ 
				$i=0;
				foreach($form_data->candidates as $candidate) { ?>
             	 <tr class="<?php echo  $i%2==0?"orange":"blue" ?>">
					<td width="23"><img src="<?php echo asset_url(); ?>images/small_user.png" width="22" alt=" " /></td>
					<td width="127"><strong><?php echo $candidate->user_first_name ?> <?php echo $candidate->user_last_name ?></strong></td>
					<td width="127"><strong><?php echo $candidate->can_curr_experince?></strong></td>
					<td width="172"><?php echo $candidate->can_location_name ?></td>
					<td width="139"><?php echo $candidate->can_designation_name ?></td>
					<td width="33" align="center">
						<a target='_blank' href="<?php echo base_url()?>mbc/candidate_upload/candidate/<?php echo $candidate->user_id;?>" class="submit">View</a></td>
				  </tr>
			  <?php
			  		$i++;
			   }
			 } ?>
             
            </table>                    
                    
                </div><!--end col-right-->
       <div class="clear"></div>
            
       	    
            
                