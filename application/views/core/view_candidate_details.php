<?php
//$objmasters =(object)$masters;
$form_data = (object)$form_data;
//debug($form_data);
?>

<div class="box-2" id="Registration">
<h1 class="title">Registration</h1>
<div style="clear: both;"></div>
<br />
<table width="540" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td width="120"><?php echo form_label("Name<span style='color:red'>*</span>","user_first_name")?>
		</td>
		<td width="290"><?php echo $form_data->user_first_name . " " . $form_data->user_last_name;?>
		</td>
	</tr>
	<tr>
		<td><?php echo form_label("Email<span style='color:red'>*</span>","user_email")?>
		</td>
		<td><?php 
		echo $form_data->user_email;
		?></td>
	</tr>
	<tr>
		<td><?php echo form_label("Contact Number<span style='color:red'>*</span>","user_contact_mobile")?>
		</td>
		<td><?php 
		echo "0-".$form_data->user_contact_mobile?>
		</td>
	</tr>
	<tr>
		<td><label> Total Years of Experience</label></td>
		<td><?php
		
		$exp = (convert_months_to_year_array($form_data->can_total_experience));
		echo $exp["years"] . " Years" . "  ".$exp["months"] . " Months";
		//echo form_dropdown("ddl_exp_years", $objmasters->exp_years,$exp["years"] ,'id="ddl_exp_years" class=""') ." Years " . form_dropdown("ddl_exp_months",$objmasters->exp_months,$exp["months"] ,'id="ddl_exp_months" " class=""')."Months" ;?>
		</td>
	</tr>
	<tr>
		<td>What best suites your status?</td>
		<td>
		<?php
			switch($form_data->can_type_id)
			{
				case 5 : 
					echo "I am corporate banker";
					break; 
				case 6 : 
					echo "My client is a corporate bank in India";
					break;
				case 7: echo "I work for credit rating agencies in India";
				break;
				case 8: echo "I earlier worked for a corporate bank in India";
				break;
				case 9 : echo "Fresher";
				break;
			}
			//echo form_dropdown("can_type_id",$arrcantype,$form_data->can_type_id,'id ="can_type_id" onchange="ShowToolTip(this.id)"')
		?>
		<br />
		<label id="lblToolTip" for="ddlCandidateType" style="display: none">Tip:
		<i>Added with this option includes operations, Administration, Central
		Processing Unit & support functions for Corporate Branch in India</i></label>

		</td>
	</tr>
</table>
<!--end addmore-->
<div style="clear: both;"></div>
</div>
<div
	class="box-2" id="experience"><!--<img src="images/icon-3.jpg" width="40" alt="" align="left" /> -->
<h1 class="title">Experience</h1>
<div style="clear: both;"></div>
<br />
<table width="540" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td width="120"><label id="current_bank_label"> Currently which bank are you working for?</label>
		</td>
		<td width="290"><?php  
		debug($form_data->can_curr_experince);
		//echo form_dropdown('can_curr_experince',  $objmasters->bank,$form_data->can_curr_experince ,'id="can_curr_experince" class="" onchange="fillbranches()"');?>
		</div>
		</div>

		</td>
	</tr>
	<tr id="ctc_tr">
		<td><label> Current CTC</label></td>
		<td>
			<?php 
			$form_data->can_ctc = ($form_data->can_ctc == "")?"0.0" :$form_data->can_ctc;
			echo $form_data->can_ctc ."lakhs";
			//$arr_ctc =  explode(".", strval($form_data->can_ctc));
			//echo form_ctc_lksdropdown("can_ctc_lks",$arr_ctc[0] ,'id="can_ctc_lks" class=""') ." Lakhs " . form_dropdown("can_ctc_thousands", $objmasters->ctc_thousands,$arr_ctc[1] ,'id="ctc_thousands" class=""')."Thousands" ;
			//echo form_input("can_ctc_notes","$form_data->can_ctc_notes" ,'id="can_ctc_notes" class="large" placeholder="additional perks, eg accomodation etc"');?>
		</td>
	</tr>
	<tr>
		<td><label id="current_band_label"> Current Internal Band</label></td>
		<td> <?php echo $form_data->can_band?>
		</td>
	</tr>

	<tr>
		<td><label id="current_designation_label"> Current Designation</label></td>
		<td width="290"><?php
		echo  $form_data->can_designation_id; 
		//echo form_dropdown('can_designation_id',$objmasters->designations ,$form_data->can_designation_id ,'id="ddlCurrentBanks" class=""');
		?>
		</td>
	</tr>
	<tr id="loaction_tr">
		<td width="120"><label> Current Location </label></td>
		<td width="290"><?php
		echo $form_data->can_location_name; 
		//echo form_input("txtCurrLocation",$form_data->can_location_name,' id="txtCurrLocation"  class="large"');?>
		</td>
	</tr>
	<tr>
		<td><label id="current_branch_label"> Current Bank Branch</label></td>
		<td>
		<div id="DivCurrBankBranch"></div>
		
		<?php 
		echo $form_data->can_current_branch_id;
			  if($form_data->can_current_branch_id != "")
			  {
				//echo form_dropdown("can_current_branch_id",$this->objMaster->get_branches($form_data->can_curr_experince,true,true),$form_data->can_current_branch_id,'class="jscurrbankbranch large" onclick="check_current_bank()" id="can_current_branch_id"');
			  }	
			  else
			  {
			  	//echo form_dropdown("can_current_branch_id",$this->objMaster->get_branches(0,true,true),$form_data->can_current_branch_id,'class="jscurrbankbranch large" id="can_current_branch_id" onclick="check_current_bank()" ');
			  } 
		?>
		</td>
	</tr>

</table>
<!--end addmore-->
<div style="clear: both;"></div>
</div>
<div class="box-2" id="new_ctc_box" style="display:none;">
<h1 class="title">Current CTC &amp; Location</h1>
<div style="clear: both;"></div>
<br />
<table width="540" border="0" cellspacing="0" cellpadding="5">
	<tr id="new_cuur_ctc">
	</tr>
	<tr id="new_curr_loc"> 
	</tr>
</table>
</div>
<div class="box-2" id="preexperience">
<h1 class="title" id="previous_experience_title">Previous experience</h1>
<div style="clear: both;"></div>
<br />
<table width="540" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td width="120"><label id="previous_bank_label"> Which all banks you have worked earlier or would like to work
		<p class="note">(you can select multiple)</p>
		</label></td>
		<td width="290"><?php debug($form_data->can_prev_experince);
					//echo form_hcheckboxTree("can_prev_experince",$objmasters->hbanks,$form_data->can_prev_experince,"");	
					?>
		</td>
	</tr>
	
	<tr>
		<td colspan="2">
		<div>
		<table id="tbCurrDepartmentDetails" border="1" cellpadding="5px"
			cellspacing="0">
			<tr>
				<th>Department</th>
				<th>Is current</th>
				<th>Are you intreseted in recieving opportunities for this
				department</th>
				<th>Approx relevant exp.in months</th>
			</tr>
			<?php 
			$depts = "";
			//if(isset($form_data->user_prev_departments))
			$depts = $form_data->user_departments;
			//if(isset($form_data->str_user_departments))
			//$depts=$depts.$form_data->str_user_departments;
			if(!empty($depts))
			{
		?>
			<?php 
					foreach ($depts as $upd) {  
			?>
					   <tr>
						<td> <?php  echo trim($upd->dm_path); ?></td>
						<td>
							<?php echo $upd->ud_is_current ==1 ? 'yes':'no';?> 
						 </td>
						<td>
							<?php echo $upd->ud_is_interested ==1 ?  'yes':'no';?> 
						 </td>
						<td>
						<?php echo $upd->ud_relevant_experince . " approx. months"?> 
						 </td>
					</tr>
					
			<?php } } ?>
		</table>
		</div>

		</td>
	</tr>
	
</table>
<!--end addmore-->
<div style="clear: both;"></div>
</div>

<!--end box-->
<div
	class="box-2" id="qualification"><!--<img src="images/icon-2.jpg" width="40" alt="" align="left" /> -->
<h1 class="title">Qualification</h1>
<div style="clear: both;"></div>
<br />
<table width="480" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td width="120"><label> Post Graduation
		<p class="note">(you can select multiple)</p>
		</label></td>
		<td width="290">
		<?php
			echo $selected_post_grad =  !empty($form_data->can_postgrad_qualifications) ? $form_data->can_postgrad_qualifications: "";
			//echo form_hcheckboxTree("can_postgrad_qualifications",$this->objMaster->get_qualifications(Qualifications::POSTGRADUATION,List_Bl::FORMAT_TREE),$selected_post_grad);
		  ?>
		</td>
	</tr>
	<tr>
		<td><label> Graduation
		<p class="note">(you can select multiple)</p>
		</label></td>
		<td>
		

		<?php
			echo $selected_grad =  !empty($form_data->can_grad_qualifications) ? $form_data->can_grad_qualifications: "";
			//echo form_hcheckboxTree("can_grad_qualifications",$this->objMaster->get_qualifications(Qualifications::GRADUATION,List_Bl::FORMAT_TREE),$selected_grad);
		?>
		</td>
	</tr>
	<tr>
		<td><label> Certification
		<p class="note">(multiple certificates to be seprated by ',')</p>
		</label></td>
		<td><?php echo $form_data->can_certifications;?></td>
	</tr>
</table>
</div>
<!--end box-->
<div
	class="box-2" id="personal"><!--<img src="images/icon-1.jpg" width="40" alt="" align="left" /> -->
<h1 class="title">Personal</h1>
<div style="clear: both;"></div>
<br />
<table width="480" border="0" cellspacing="0" cellpadding="5">
	<tr>
		<td><label> Age years</label></td>
		<td><?php echo $form_data->can_age;?>
		</td>
	</tr>
	<tr>
		<td width="120"><label> Date of Birth </label></td>
		<td width="290"><?php 
			echo ($form_data->can_dob);
			
			//echo form_dropdown('dob_day',$objmasters->days ,$dob["days"]  ,'id="ddlDOBDay" class="small-select date"') ." &nbsp; " .form_monthdropdown('dob_month', $dob["months"], 'id="ddlDOBMonth" class="small-select month"')."&nbsp;" .form_dropdown('dob_year', $objmasters->dob_years,$dob["years"] ,'id="dob_year" class="small-select year"')."&nbsp;" ;?>
		<div style="clear: both;"></div>
		</td>
	</tr>
	<tr>
		<td><label> Gender </label></td>
		<td> <?php echo $form_data->can_gender?></td>
	</tr>
	<tr>
		<td><label> Marital Status</label></td>
		<td><?php 
		echo $form_data->can_marital_status;
		//$arrmarstattus = array("" =>" ","Married"=>"Married","Single"=>"Single","Others"=>"Others");
		//echo form_dropdown("can_marital_status",$arrmarstattus,$form_data->can_marital_status,"class='small=select'");?>
		</td>
	</tr>
	<tr style="overflow:hidden">
		<td valign="top"><label>Resume</label></td>
		<td>
			<?php 
				$resume_url = base_url().'uploads/resume/'.$form_data->can_resume_file;
				echo '<iframe width="100%" src="http://docs.google.com/gview?url='.$resume_url.'"></iframe>';
			?>
			</td>
	</tr>
	<tr>
		<td valign="top"><label>Photo</label></td>
		<td>
			<?php 
				
				echo '<img src="'.base_url().'uploads/profile_photo/'.$form_data->can_photo.'" id="photo_display" width="50" />';
			?>
			</td>
	</tr>
</table>
</div>
<!--end box-->
        
